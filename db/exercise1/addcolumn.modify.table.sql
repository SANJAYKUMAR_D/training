2.Alter Table with Add new Column and Modify\Rename\Drop column

ALTER TABLE `college management`.`student` 
  ADD COLUMN `phoneno` INT NOT NULL AFTER `dob`;

ALTER TABLE `college management`.`student` 
CHANGE COLUMN `phoneno` `phoneno` INT NOT NULL AFTER `batch`;

ALTER TABLE `college management`.`student` 
CHANGE COLUMN `phoneno` `mobileno` INT NOT NULL ;

ALTER TABLE `college management`.`student` 
DROP COLUMN `mobileno`;