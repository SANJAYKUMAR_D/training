9.USE Where to filter records using AND,OR,NOT,LIKE,IN,ANY, wildcards ( % _ )

SELECT *
  FROM `college management`.student
 WHERE name="anbu" 
   AND age="30"

SELECT *
  FROM `college management`.student
 WHERE name="anbu" 
    OR age="30"

SELECT *
  FROM `college management`.student
 WHERE NOT age="33"

SELECT *
  FROM `college management`.student
 WHERE name 
  LIKE "r%"

SELECT *
  FROM `college management`.student
 WHERE name 
    IN ('arun','selva')

SELECT name 
  FROM `college management`.student
 WHERE rollno = ANY (SELECT rollno FROM `college management`.student WHERE age = "33" );

SELECT * 
  FROM `college management`.student
 where name 
  LIKE "se___"