5.Insert Records into Table

INSERT INTO `college management`.`student` (`rollno`
                                           ,`name`
                                           ,`batch`
                                           ,`dob`)
VALUES ('1'
       ,'anbu'
       ,'2020'
       ,'8/11/2000');
INSERT INTO `college management`.`student` (`rollno`
                                           ,`name`
                                           ,`batch`
                                           ,`dob`)
VALUES ('2'
       ,'arun'
       ,'2020'
       ,'7/8/2001');
INSERT INTO `college management`.`student` (`rollno`
                                           ,`name`
                                           ,`batch`
                                           ,`dob`)
VALUES ('3'
       ,'raja'
       ,'2020'
       ,'22/5/2000');
INSERT INTO `college management`.`student` (`rollno`
                                           ,`name`
                                           ,`batch`
                                           ,`dob`)
VALUES ('4'
       ,'krishna'
       ,'2020'
       ,'12/12/2001');
INSERT INTO `college management`.`department` (`deptid`
                                              ,`departmentname`
                                              ,`rollno`)
VALUES ('01'
       ,'cse'
       ,'1');
INSERT INTO `college management`.`department` (`deptid`
                                              ,`departmentname`
                                              ,`rollno`)
VALUES ('02'
       ,'eee'
       ,'2');
INSERT INTO `college management`.`department` (`deptid`
                                              ,`departmentname`
                                              ,`rollno`)
VALUES ('03'
       ,'ece'
       ,'3');
INSERT INTO `college management`.`department` (`deptid`
                                              ,`departmentname`
                                              ,`rollno`)
VALUES ('04'
       ,'mech'
       ,'4');
