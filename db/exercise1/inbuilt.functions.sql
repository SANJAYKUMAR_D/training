10.USE inbuilt Functions - now,MAX, MIN, AVG, COUNT, FIRST, LAST, SUM

SELECT MIN(age)
  FROM `college management`.student

SELECT MAX(age)
  FROM `college management`.student

SELECT AVG(age)
  FROM `college management`.student

SELECT COUNT(deptid)
  FROM `college management`.department

SELECT *
  FROM `college management`.student
 ORDER BY rollno ASC LIMIT 1

SELECT *
  FROM `college management`.student
 ORDER BY rollno DESC LIMIT 1

SELECT SUM(age)
  FROM `college management`.student
