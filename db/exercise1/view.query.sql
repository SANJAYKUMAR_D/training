14.Create VIEW for your Join Query and select values from VIEW

CREATE VIEW `[All_Details]` AS
SELECT name 
      ,departmentname 
      ,staffname
  FROM `college management`.student
  JOIN `college management`.department ON department.rollno = student.rollno
  JOIN `college management`.staff ON staff.deptid = department.deptid

CREATE VIEW `[selectstudents]` AS
SELECT rollno
      ,name
      ,dob
  FROM `college management`.student
 WHERE age = "20"

DROP VIEW `[selectstudents]`;
