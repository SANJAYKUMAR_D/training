3.Use Constraints NOT NULL, DEFAULT, CHECK, PRIMARY KEY, FOREIGN KEY, KEY, INDEX

ALTER TABLE `college management`.`student` 
CHANGE COLUMN `batch` `batch` INT NOT NULL ;

ALTER TABLE `college management`.`student` 
CHANGE COLUMN `batch` `batch` INT NOT NULL DEFAULT 2020 ;

ALTER TABLE `college management`.department
  ADD CHECK (deptid <= 15);

ALTER TABLE `college management`.`student` 
  ADD PRIMARY KEY (`rollno`, `batch`);

ALTER TABLE `college management`.department
FOREIGN KEY (rollno) 
        REFERENCES student(rollno);

ALTER TABLE `college management`.`student` 
  ADD UNIQUE (dob);

CREATE INDEX indxstudname
    ON `college management`.student(name);
