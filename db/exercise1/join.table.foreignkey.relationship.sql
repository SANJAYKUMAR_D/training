12.Read Records by Join using tables FOREIGN KEY relationships (CROSS, INNER, LEFT, RIGHT)

SELECT * 
  FROM `college management`.student
 CROSS JOIN  `college management`.department
         
SELECT *
  FROM `college management`.student
 INNER JOIN  `college management`.department
    ON student.rollno = department.rollno;

SELECT *
  FROM `college management`.student
  LEFT JOIN  `college management`.department
    ON student.rollno = department.rollno;

SELECT *
  FROM `college management`.student
 RIGHT JOIN  `college management`.department
    ON student.rollno = department.rollno;
