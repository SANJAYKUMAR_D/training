13.At-least use 3 tables for Join
Union 2 Different table with same Column name (use Distinct,ALL)

CREATE TABLE `college management`.`staff` (
    `staffid` INT NOT NULL
   ,`staffname` VARCHAR(45) NOT NULL
   ,`age` VARCHAR(45) NOT NULL
   ,`deptid` INT NOT NULL
   , PRIMARY KEY (`staffid`));

INSERT INTO `college management`.`staff` (`staffid`
                                         ,`staffname`
                                         ,`age`
                                         ,`deptid`)
VALUES ('001'
       ,'keerthana'
       ,'30'
       ,'1');
INSERT INTO `college management`.`staff` (`staffid`
                                         ,`staffname`
                                         ,`age`
                                         ,`deptid`)
VALUES ('002'
       ,'priya'
       ,'28'
       ,'2');
INSERT INTO `college management`.`staff` (`staffid`
                                         ,`staffname`
                                         ,`age`
                                         ,`deptid`)
VALUES ('003'
       ,'kamaraj'
       ,'29'
       ,'3');

ALTER TABLE `college management`.staff
  ADD CONSTRAINT FK_Perr
FOREIGN KEY (deptid) 
        REFERENCES department(deptid);

SELECT student.name
      ,department.departmentname
      ,staff.staffname
  FROM college management.student
  JOIN college management.department 
    ON department.rollno = student.rollno
  JOIN college management.staff 
    ON staff.deptid = department.deptid

CREATE TABLE `college management`.`student1` (
    `rollno` INT NOT NULL
   ,`name` VARCHAR(45) NOT NULL
   ,`batch` INT NOT NULL
   ,`dob` VARCHAR(45) NOT NULL
   ,`age` INT NOT NULL
   , PRIMARY KEY (`rollno`));

INSERT INTO `college management`.`student1` (`rollno`
                                            ,`name`
                                            ,`batch`
                                            ,`dob`
                                            ,`age`)
VALUES ('11'
       ,'sunil'
       ,'2020'
       ,'9/12/2000'
       ,'21');
INSERT INTO `college management`.`student1` (`rollno`
                                            ,`name`
                                            ,`batch`
                                            ,`dob`
                                            ,`age`)
VALUES ('12'
       ,'gokul'
       ,'2020'
       ,'4/5/1999'
       ,'20');
INSERT INTO `college management`.`student1` (`rollno`
                                            ,`name`
                                            ,`batch`
                                            ,`dob`
                                            ,`age`)
VALUES ('13'
       ,'praveen'
       ,'2020'
       ,'29/5/2001'
       ,'19');
INSERT INTO `college management`.`student1` (`rollno`
                                            ,`name`
                                            ,`batch`
                                            ,`dob`
                                            ,`age`)
VALUES ('14'
       ,'pavi'
       ,'2020'
       ,'14/2/2000'
       ,'20');

SELECT 'student' 
        rollno
       ,name
       ,batch
       ,dob
       ,age
  FROM  `college management`.student
 UNION
SELECT 'student1'
        rollno
       ,name
       ,batch
       ,dob
       ,age
  FROM  `college management`.student1

SELECT name 
  FROM  `college management`.student
 UNION ALL
SELECT name 
  FROM  `college management`.student1
 ORDER BY name
