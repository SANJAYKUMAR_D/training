1.Create Database\table and Rename\Drop Table

CREATE SCHEMA `college management` ;

CREATE TABLE `college management`.`student` (
    `rollno` INT NOT NULL
   ,`name` VARCHAR(45) NOT NULL
   ,`batch` INT NOT NULL
   ,`dob` DATE NOT NULL
   , PRIMARY KEY (`rollno`));

CREATE TABLE `college management`.`department` (
    `name` VARCHAR(45) NOT NULL
   ,`deptid` INT NOT NULL
   , PRIMARY KEY (`id`));

ALTER TABLE `college management`.department
ADD CONSTRAINT FK_PersonOrder
FOREIGN KEY (rollno) 
        REFERENCES student(rollno);

CREATE TABLE `college management`.`faculty` (
    `facultyid` INT NOT NULL
   ,`facultyname` VARCHAR(45) NOT NULL
   ,`deptid` INT NOT NULL
   , PRIMARY KEY (`facultyid`)
   , FOREIGN KEY (deptid)
             REFERENCES department(deptid));

ALTER TABLE `college management`.`faculty` 
RENAME TO  `college management`.`staff` ;

DROP TABLE `college management`.`staff`;
