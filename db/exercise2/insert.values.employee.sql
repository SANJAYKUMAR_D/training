2. Write a query to insert at least 5 employees for each department

INSERT INTO `employee management`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surename`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annnual_salary`
                                             ,`dept_id`) 
VALUES ('1'
       ,'anbu'
       ,'e'
       ,'1998-02-01'
       ,'2020-02-14'
       ,'85000'
       ,'1');
INSERT INTO `employee management`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surename`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annnual_salary`
                                             ,`dept_id`) 
VALUES ('2'
       ,'alex'
       ,'r'
       ,'1989-12-12'
       ,'2015-04-26'
       ,'90000'
       ,'1');
INSERT INTO `employee management`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surename`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annnual_salary`
                                             ,`dept_id`) 
VALUES ('3'
       ,'arun'
       ,'t'
       ,'2000-02-11'
       ,'2010-02-17'
       ,'100000'
       ,'1');
INSERT INTO `employee management`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surename`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annnual_salary`
                                             ,`dept_id`) 
VALUES ('4'
       ,'akila'
       ,'y'
       ,'1999-06-15'
       ,'2021-06-28'
       ,'150000'
       ,'1');
INSERT INTO `employee management`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surename`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annnual_salary`
                                             ,`dept_id`) 
VALUES ('5'
       ,'aravind'
       ,'u'
       ,'1998-10-10'
       ,'2020-07-18'
       ,'55000'
       ,'1');
INSERT INTO `employee management`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surename`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annnual_salary`
                                             ,`dept_id`) 
VALUES ('6'
       ,'selva'
       ,'i'
       ,'1997-09-30'
       ,'2020-08-29'
       ,'65000'
       ,'2');
INSERT INTO `employee management`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surename`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annnual_salary`
                                             ,`dept_id`)  
VALUES ('7'
       ,'karthi'
       ,'o'
       ,'1995-08-29'
       ,'2020-09-30'
       ,'75000'
       ,'2');
INSERT INTO `employee management`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surename`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annnual_salary`
                                             ,`dept_id`) 
VALUES ('8'
       ,'kavin'
       ,'p'
       ,'1996-07-18'
       ,'2022-02-11'
       ,'99000'
       ,'2');
INSERT INTO `employee management`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surename`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annnual_salary`
                                             ,`dept_id`) 
VALUES ('9'
       ,'praba' 
       ,'l'
       ,'1994-06-28'
       ,'2015-10-10'
       ,'98000'
       ,'2');
INSERT INTO `employee management`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surename`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annnual_salary`
                                             ,`dept_id`) 
VALUES ('10'
       ,'yuki'
       ,'k'
       ,'1995-02-17'
       ,'2019-02-11'
       ,'79000'
       ,'2');
INSERT INTO `employee management`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surename`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annnual_salary`
                                             ,`dept_id`) 
VALUES ('11'
       ,'yogesh'
       ,'j'
       ,'1992-04-26'
       ,'2020-12-12' 
       ,'110000'
       ,'3');
INSERT INTO `employee management`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surename`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annnual_salary`
                                             ,`dept_id`) 
VALUES ('12'
       ,'vignesh'
       ,'h'
       ,'1991-02-14'
       ,'2021-02-01'
       ,'115000'
       ,'3');
INSERT INTO `employee management`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surename`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annnual_salary`
                                             ,`dept_id`) 
VALUES ('13'
       ,'robert'
       ,'g'
       ,'1989-09-22'
       ,'2020-07-08'
       ,'87000'
       ,'3');
INSERT INTO `employee management`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surename`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annnual_salary`
                                             ,`dept_id`) 
VALUES ('14'
       ,'jhonydepp'
       ,'f'
       ,'1989-12-12'
       ,'2014-11-05'
       ,'200000'  
       ,'3');
INSERT INTO `employee management`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surename`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annnual_salary`
                                             ,`dept_id`) 
VALUES ('15'
       ,'galgadot'
       ,'d'
       ,'2000-11-11'
       ,'2015-11-08'
       ,'135000'
       ,'3');
INSERT INTO `employee management`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surename`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annnual_salary`
                                             ,`dept_id`) 
VALUES ('16'
       ,'arnold'
       ,'s'
       ,'2001-10-10'
       ,'2016-09-25'
       ,'95000'
       ,'4');
INSERT INTO `employee management`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surename`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annnual_salary`
                                             ,`dept_id`) 
VALUES ('17'
       ,'jacky'
       ,'a'
       ,'1999-09-09'
       ,'2017-04-22'
       ,'88000'
       ,'4');
INSERT INTO `employee management`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surename`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annnual_salary`
                                             ,`dept_id`) 
VALUES ('18'
       ,'albert'
       ,'c'
       ,'1989-05-05'
       ,'2018-06-07'
       ,'77000'
       ,'4');
INSERT INTO `employee management`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surename`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annnual_salary`
                                             ,`dept_id`)  
VALUES ('19'
       ,'trump'
       ,'v'
       ,'1985-06-07'
       ,'2019-05-05'
       ,'66000'
        ,'4');
INSERT INTO `employee management`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surename`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annnual_salary`
                                             ,`dept_id`) 
VALUES ('20'
       ,'ronaldo'
       ,'b'
       ,'1985-04-22'
       ,'2020-09-09'
       ,'86000'
       ,'4');
INSERT INTO `employee management`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surename`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annnual_salary`
                                             ,`dept_id`) 
VALUES ('21'
       ,'dhoni'
       ,'n'
       ,'1995-09-25'
       ,'2020-10-10'
       ,'150000' 
       ,'5');
INSERT INTO `employee management`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surename`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annnual_salary`
                                             ,`dept_id`) 
VALUES ('22'
       ,'priya'
       ,'m'
       ,'2000-11-08'
       ,'2021-11-11' 
       ,'100000'
       ,'5');
INSERT INTO `employee management`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surename`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annnual_salary`
                                             ,`dept_id`)  
VALUES ('23'
       ,'swetha'
       ,'n'
       ,'1997-11-05'
       ,'2020-12-12'
       ,'91000'
       ,'5');
INSERT INTO `employee management`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surename`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annnual_salary`
                                             ,`dept_id`) 
VALUES ('24'
       ,'santhiya'
       ,'s'
       ,'1992-07-08'
       ,'2019-09-22'
       ,'93000'
       ,'5');
INSERT INTO `employee management`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surename`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annnual_salary`
                                             ,`dept_id`) 
VALUES ('25'
       ,'nisha'
       ,'a'
       ,'2001-07-12' 
       ,'2018-03-03'
       ,'89000'
       ,'5');
INSERT INTO `employee management`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surename`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annnual_salary`
                                             ,`dept_id`) 
VALUES ('26'
       ,'sofia'
       ,'d'
       ,'1981-11-16'
       ,'2010-02-02' 
       ,'100000'
       ,'6');
INSERT INTO `employee management`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surename`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annnual_salary`
                                             ,`dept_id`) 
VALUES ('27'
       ,'ramya'
       ,'f'
       ,'1999-12-31'
       ,'2015-01-01'
       ,'110000'
       ,'6');
INSERT INTO `employee management`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surename`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annnual_salary`
                                             ,`dept_id`) 
VALUES ('28' 
       ,'nandhini'
       ,'h'
       ,'2000-01-01'
       ,'2016-12-31'
       ,'115000'
       ,'6');
INSERT INTO `employee management`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surename`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annnual_salary`
                                             ,`dept_id`) 
VALUES ('29'
       ,'anu'
       ,'n'
       ,'1997-02-02'
       ,'2013-11-16'
       ,'125000'
       ,'6');
INSERT INTO `employee management`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surename`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annnual_salary`
                                             ,`dept_id`) 
VALUES ('30'
       ,'keerthi'
       ,'i'
       ,'1992-03-03'
       ,'2017-07-12'
       ,'96000'
       ,'6');
