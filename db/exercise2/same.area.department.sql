8. Write a query to list out employees from the same area, and from the same department

ALTER TABLE `employee`
  ADD COLUMN `city` VARCHAR(25) NOT NULL AFTER surename;

UPDATE `employee`
   SET city = 'Kolkata'
 WHERE `emp_id` IN (1,5,6,21,23);
UPDATE `employee`
   SET city = 'Chennai'
 WHERE `emp_id` IN (2,4,14,8,11);
UPDATE `employee`
   SET city = 'Hydrabad'
 WHERE `emp_id` IN (15,17,20,22,29);
UPDATE `employee`
   SET city = 'Mumbai'
 WHERE `emp_id` IN (3,7,12,13,24);
UPDATE `employee`
   SET city = 'Delhi'
 WHERE `emp_id` IN (9,10,16,18,28,30);
UPDATE `employee`
   SET city = 'Punjab'
 WHERE `emp_id` IN (19,25,26,27);
 
SELECT city
      ,first_name
  FROM `employee`
 ORDER BY city;

SELECT `department`.dept_name
       ,first_name
  FROM `employee`
  JOIN `department` 
    ON (employee.dept_id = department.dept_id);

SELECT first_name 
      ,city
      ,dept_name
  FROM employee 
      ,department 
 WHERE employee.dept_id = department.dept_id 
   AND dept_name = 'ITDesk';

SELECT first_name
      ,city
      ,dept_name
  FROM employee 
      ,department 
 WHERE employee.dept_id = department.dept_id 
   AND dept_name = 'Finance';

SELECT first_name
      ,city
      ,dept_name
  FROM employee 
      ,department 
 WHERE employee.dept_id = department.dept_id 
   AND dept_name = 'Engineering';

SELECT first_name
      ,city
      ,dept_name
  FROM employee 
      ,department 
 WHERE employee.dept_id = department.dept_id 
   AND dept_name = 'HR';

SELECT first_name
      ,city
      ,dept_name
  FROM employee 
      ,department 
 WHERE employee.dept_id = department.dept_id 
   AND dept_name = 'Recruitment';

SELECT first_name
      ,city
      ,dept_name
  FROM employee 
      ,department 
 WHERE employee.dept_id = department.dept_id 
   AND dept_name = 'Facility';

SELECT city,department.dept_name, first_name
  FROM `employee`
  JOIN `department` 
    ON (employee.dept_id = department.dept_id)
 ORDER BY dept_name
        , city;
 