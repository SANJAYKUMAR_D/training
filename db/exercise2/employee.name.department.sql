11. Write a query to get employee names and their respective department name

SELECT `employee`.first_name AS Employee_Name
     , `department`.dept_name AS Department_Name
  FROM employee
  JOIN department 
    ON (employee.dept_id = department.dept_id);