9. Write a query to find out employee birthday falls on that day

SELECT first_name AS 'Name'
     , dob AS 'Date of birth'
  FROM employee
 WHERE DATE_FORMAT(dob,'%m-%d') = DATE_FORMAT(NOW(),'%m-%d');