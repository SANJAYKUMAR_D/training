7. Prepare an example for self-join

ALTER TABLE `employee management`.`employee` 
  ADD COLUMN `superviser_id` INT NULL AFTER `dept_id`;

UPDATE `employee` 
   SET `superviser_id` = '1' 
 WHERE (`emp_id` = '2');
UPDATE `employee` 
   SET `superviser_id` = '1' 
 WHERE (`emp_id` = '3');
UPDATE `employee` 
   SET `superviser_id` = '3' 
 WHERE (`emp_id` = '4');
UPDATE `employee` 
   SET `superviser_id` = '3' 
 WHERE (`emp_id` = '5');
UPDATE `employee` 
   SET `superviser_id` = '3' 
 WHERE (`emp_id` = '6');

SELECT emp.emp_id AS 'Employee_ID'
      ,emp.first_name AS 'Employee_name'
      ,supv.emp_id AS 'Superviser_ID'
      ,supv.first_name AS 'Superviser_name'
  FROM employee AS emp
      ,employee AS supv
 WHERE emp.`superviser_id` = supv.`emp_id`;

SELF JOIN:

A self join is a join in which a table is joined with itself
the table has a FOREIGN KEY which references its own PRIMARY KEY
ex:
  alex supervisor is anbu
  arun supervisor is anbu
  akila,aravind and selva are under supervison of arun.
