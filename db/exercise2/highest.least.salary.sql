5. Find out the highest and least paid in all the department

SELECT dept_id
     , MAX(annual_salary) AS 'Highest Salary' 
     , MIN(annual_salary) AS 'Least Salary'
  FROM employee
 GROUP BY(dept_id);
