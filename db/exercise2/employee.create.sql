1. Create tables to hold employee(emp_id, first_name, surname, dob, date_of_joining, annual_salary), possible depts: ITDesk, Finance, Engineering, HR, Recruitment, Facility

    employee number must be the primary key in employee table
    department number must be the primary key in department table
    department number is the foreign key in the employee table

CREATE SCHEMA `employee management` ;

CREATE TABLE `employee management`.`employee` (
   `emp_id` INT NOT NULL
  ,`first_name` VARCHAR(45) NOT NULL
  ,`surename` VARCHAR(45) NOT NULL
  ,`dob` DATE NOT NULL
  ,`date_of_joining` DATE NOT NULL
  ,`annnual_income` INT NOT NULL
  ,`dept_id` INT NOT NULL
  , PRIMARY KEY (`emp_id`))
;

CREATE TABLE `employee management`.`department` (
   `dept_id` INT NOT NULL
  ,`dept_name` VARCHAR(45) NOT NULL
  , PRIMARY KEY (`dept_id`))
;

ALTER TABLE `employee management`.employee
  ADD CONSTRAINT FK_PersonOrder
FOREIGN KEY (dept_id) 
        REFERENCES department(dept_id)
;

INSERT INTO `employee management`.`department` (`dept_id`
                                               ,`dept_name`) 
VALUES ('01'
       ,'ITDesk');
INSERT INTO `employee management`.`department` (`dept_id`
                                               ,`dept_name`) 
VALUES ('02'
       ,'Finance');
INSERT INTO `employee management`.`department` (`dept_id`
                                               ,`dept_name`) 
VALUES ('03'
       ,'Engineering');
INSERT INTO `employee management`.`department` (`dept_id`
                                               ,`dept_name`) 
VALUES ('04'
       ,'HR');
INSERT INTO `employee management`.`department` (`dept_id`
                                               ,`dept_name`) 
VALUES ('05'
       ,'Recruitment');
INSERT INTO `employee management`.`department` (`dept_id`
                                               ,`dept_name`) 
VALUES ('06'
       ,'Facility');