10. Prepare a query to find out fresher(no department allocated employee) in the employee, where no matching records in the department table.

ALTER TABLE `employee management`.`employee` 
CHANGE COLUMN `annual_salary` `annual_salary` INT NULL ,
CHANGE COLUMN `dept_id` `dept_id` INT NULL ;

INSERT INTO `employee management`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surename`
                                             ,`city`
                                             ,`dob`
                                             ,`date_of_joining`) 
VALUES ('31'
       ,'prakash'
       ,'a'
       ,'Delhi'
       ,'2000-11-11'
       ,'2021-11-11');
INSERT INTO `employee management`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surename`
                                             ,`city`
                                             ,`dob`
                                             ,`date_of_joining`) 
VALUES ('32'
       ,'sasmitha'
       ,'s'
       ,'Punjab'
       ,'2000-06-09'
       ,'2021-11-11');
INSERT INTO `employee management`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surename`
                                             ,`city`
                                             ,`dob`
                                             ,`date_of_joining`) 
VALUES ('33'
       ,'divya'
       ,'d'
       ,'Chennai'
       ,'1999-09-22'
       ,'2021-11-11');
INSERT INTO `employee management`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surename`
                                             ,`city`
                                             ,`dob`
                                             ,`date_of_joining`) 
VALUES ('34'
       ,'anjali'
       ,'n'
       ,'Chennai'
       ,'2001-10-30'
       ,'2021-12-11');
INSERT INTO `employee management`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surename`
                                             ,`city`
                                             ,`dob`
                                             ,`date_of_joining`) 
VALUES ('35'
       ,'senthil'
       ,'m'
       ,'Mumbai'
       ,'2001-11-08'
       ,'2021-12-22');


SELECT emp_id
     , first_name
     , city
     , dob
     , date_of_joining
  FROM employee
 WHERE dept_id IS NULL;