8. Find employee vacancy position in all the departments from all the 
colleges and across the universities. Result should be populated with 
following information.. Designation, rank, college, department and 
university details. 

ALTER TABLE `university management`.`employee` 
CHANGE COLUMN `name` `name` VARCHAR(100) NULL ,
CHANGE COLUMN `dob` `dob` DATE NULL ,
CHANGE COLUMN `email` `email` VARCHAR(50) NULL ,
CHANGE COLUMN `phone` `phone` BIGINT NULL ;

INSERT INTO `university management`.`employee` (`id`
                                               ,`college_id`
                                               ,`cdept_id`
                                               ,`desig_id`) 
VALUES ('15'
       ,'5'
       ,'5'
       ,'3');
INSERT INTO `university management`.`employee` (`id`
                                               ,`college_id`
                                               ,`cdept_id`
                                               ,`desig_id`) 
VALUES ('16'
       ,'6'
       ,'6'
       ,'3');

SELECT designation.name AS 'designation'
      ,designation.rank
      ,college.name AS 'college_name'
      ,department.dept_name
      ,university.university_name
  FROM employee
 INNER JOIN college
    ON employee.college_id = college.id
 INNER JOIN designation
    ON employee.desig_id = designation.id
 INNER JOIN college_department
    ON employee.cdept_id = college_department.cdept_id
 INNER JOIN department
    ON college_department.udept_code = department.dept_code
 INNER JOIN university 
    ON college.univ_code = university.univ_code 

 WHERE employee.name is NULL