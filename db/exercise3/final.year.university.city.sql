2. Select final year students(Assume that universities has Engineering 
Depts only) details who are studying under a particular university and 
selected cities alone. 
        ROLL_NUMBER, NAME, GENDER, DOB, EMAIL, PHONE, ADDRESS, 
        COLLEGE_NAME, DEPARTMENT_NAME 

SELECT student.`roll_number`
      ,student.`name`
      ,student.`gender`
      ,student.`dob`
      ,student.`email`
      ,student.`phone`
      ,student.`address`
      ,college.`name` AS 'college_name'
      ,department.`dept_name` AS 'department_name'
  FROM student
 INNER JOIN college_department
    ON student.cdept_id = college_department.cdept_id
 INNER JOIN department
    ON college_department.udept_code = department.dept_code
 INNER JOIN college
    ON student.college_id = college.id
 INNER JOIN university
    ON college.univ_code = university.univ_code
   
 WHERE university.university_name = 'oxford'
   AND college.city = 'Coimbatore'
   AND student.academic_year = '2020'