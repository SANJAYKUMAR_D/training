4. List Employees details from a particular university along with their 
college and department details. Details should be sorted by rank and 
college name 

SELECT employee.id AS 'employee_id'
      ,employee.name AS 'employee_name'
      ,employee.dob
      ,employee.email
      ,employee.phone
      ,designation.name
      ,designation.rank
      ,college.name AS 'college_name'
      ,university.univ_code
      ,university.university_name
  FROM college
 INNER JOIN employee
    ON college.id = employee.college_id
 INNER JOIN university
    ON college.univ_code = university.univ_code
 INNER JOIN designation
    ON employee.desig_id = designation.id 
 
 WHERE university.university_name = 'oxford'
 ORDER BY college.name
         ,designation.rank