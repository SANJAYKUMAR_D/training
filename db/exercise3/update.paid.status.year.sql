7. Update PAID_STATUS and PAID_YEAR in SEMESTER_FEE table who 
has done the payment. Entries should be updated based on student 
ROLL_NUMBER.  
   
a) Single Update - Update one student entry 

UPDATE semester_fee 
   SET paid_year = (2020) 
      ,paid_status = "paid" 
 WHERE "12359" = (SELECT roll_number
                    FROM student
                   WHERE semester_fee.stud_id = student.id
);

b) Bulk Update - Update multiple students entries 

UPDATE semester_fee 
   SET paid_year = (2020) 
      ,paid_status = "Paid" 
 WHERE (SELECT roll_number
          FROM student 
         WHERE semester_fee.stud_id = student.id) 
            IN ("12347" , "12354", "12355");

c) PAID_STATUS value as ‘Paid’ 
d) PAID_YEAR value as ‘year format’ 