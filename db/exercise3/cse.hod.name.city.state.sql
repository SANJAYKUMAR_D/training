1. Select College details which are having IT / CSC departments across all 
the universities. Result should have below details;(Assume that one of 
the Designation name is ‘HOD’ in Designation table) 
          CODE, COLLEGE_NAME, UNIVERSITY_NAME, CITY, STATE, 
          YEAR_OPENED, DEPTARTMENT_NAME, HOD_NAME 

SELECT  college.code
       ,college.name AS college_name
       ,university.university_name 
       ,college.city
       ,college.state
       ,college.year_opened
       ,department.dept_name AS department_name
       ,employee.name AS hod_name
  FROM college
 INNER JOIN university
    ON college.univ_code = university.univ_code 
 INNER JOIN employee
    ON college.id = employee.college_id
 INNER JOIN designation
    ON designation.id = employee.desig_id
 INNER JOIN college_department
    ON college_department.cdept_id = employee.cdept_id
 INNER JOIN department
    ON college_department.udept_code = department.dept_code

 WHERE designation.name = "hod"
HAVING department.dept_name = "cse" OR "it"