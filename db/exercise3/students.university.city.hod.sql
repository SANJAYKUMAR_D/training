3. Select students details who are studying under a particular university 
and selected cities alone. Fetch 20 records for every run. 
        ROLL_NUMBER, NAME, GENDER, DOB, EMAIL, PHONE, ADDRESS, 
        COLLEGE_NAME, DEPARTMENT_NAME, HOD_NAME  


SELECT student.`roll_number`
      ,student.`name`
      ,student.`gender`
      ,student.`dob`
      ,student.`email`
      ,student.`phone`
      ,student.`address`
      ,college.`name` AS 'college_name'
      ,department.`dept_name` AS 'department_name'
      ,employee.`name` AS 'hod_name'
  FROM college
 INNER JOIN university
    ON college.univ_code = university.univ_code
 INNER JOIN employee
    ON college.id = employee.college_id 
 INNER JOIN designation
    ON employee.desig_id = designation.id
 INNER JOIN college_department
    ON employee.cdept_id = college_department.cdept_id
 INNER JOIN department
    ON college_department.udept_code = department.dept_code
 INNER JOIN student
    ON college_department.cdept_id = student.cdept_id

 WHERE employee.desig_id = '1'
   AND university.university_name = 'oxford'
   AND college.city = 'coimbatore'