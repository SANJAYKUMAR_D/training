9. Show consolidated result for the following scenarios; 
a)  collected and uncollected semester fees amount per semester 
    for each college under an university. Result should be filtered 
    based on given year. 

ALTER TABLE `university management`.`semester_fee` 
  ADD COLUMN `balance_amount` DOUBLE NOT NULL AFTER `paid_status`;

SELECT university.`university_name`
      ,college.`name`
      ,semester_fee.`semester`
      ,SUM(amount) AS 'collected_fees' 
      ,SUM(balance_amount) AS 'uncollected_fees'
      ,semester_fee.paid_year
  FROM college
 INNER JOIN university
    ON college.univ_code = university.univ_code
 INNER JOIN student
    ON college.id = student.college_id
 INNER JOIN semester_fee
    ON student.id = semester_fee.stud_id

 WHERE paid_year = '2020'
 GROUP BY university_name
         ,semester


b) Collected semester fees amount for each university for the given 
    year  

SELECT university.`university_name`
      ,SUM(amount) AS 'collected_fees' 
      ,semester_fee.paid_year
  FROM semester_fee
 INNER JOIN student
    ON semester_fee.stud_id = student.id
 INNER JOIN college
    ON student.college_id = college.id
 INNER JOIN university
    ON college.univ_code = university.univ_code

 WHERE paid_status = 'paid'
   AND paid_year = '2020'
 GROUP BY university_name
