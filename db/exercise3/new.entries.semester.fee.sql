6. Create new entries in SEMESTER_FEE table for each student from all 
the colleges and across all the universities. These entries should be 
created whenever new semester starts.. Each entry should have below 
default values; 
     a) AMOUNT - Semester fees 
     b) PAID_YEAR - Null 
     c) PAID_STATUS - Unpaid 

ALTER TABLE `university management`.`semester_fee` 
CHANGE COLUMN `amount` `amount` DOUBLE(18,2) NULL DEFAULT 85000 ,
CHANGE COLUMN `paid_year` `paid_year` YEAR NULL ,
CHANGE COLUMN `paid_status` `paid_status` VARCHAR(10) NOT NULL DEFAULT 'un_paid' ;

INSERT INTO `university management`.`semester_fee` (`bill_no`
                                                   ,`cdept_id`
                                                   ,`stud_id`
                                                   ,`semester`)
VALUES ('16'
       ,'1'
       ,'16'
       ,'6');
INSERT INTO `university management`.`semester_fee` (`bill_no`
                                                   ,`cdept_id`
                                                   ,`stud_id`
                                                   ,`semester`) 
VALUES ('17'
       ,'2'
       ,'17'
       ,'6');
INSERT INTO `university management`.`semester_fee` (`bill_no`
                                                   ,`cdept_id`
                                                   ,`stud_id`
                                                   ,`semester`) 
VALUES ('18'
       ,'6'
       ,'18'
       ,'6');
INSERT INTO `university management`.`semester_fee` (`bill_no`
                                                   ,`cdept_id`
                                                   ,`stud_id`
                                                   ,`semester`) 
VALUES ('19'
       ,'4'
       ,'19'
       ,'6');
INSERT INTO `university management`.`semester_fee` (`bill_no`
                                                   ,`cdept_id`
                                                   ,`stud_id`
                                                   ,`semester`) 
VALUES ('20'
       ,'5'
       ,'20'
       ,'6');