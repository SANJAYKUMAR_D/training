10. Display below result in one SQL run 
   a) Shows students details who scored above 8 GPA for the given 
         semester 
   b) Shows students details who scored above 5 GPA for the given 
         semester 
 
SELECT student.`id` AS 'student_id'
      ,student.`roll_number`
      ,student.`name` AS 'student_name'
      ,student.`gender`
      ,college.`code` AS 'college_code'
      ,college.`name` AS 'college_name'
      ,semester_result.`semester`
      ,semester_result.`grade`
      ,semester_result.`gpa`
  FROM student
 INNER JOIN college
    ON student.`college_id` = college.`id`
 INNER JOIN university
    ON college.`univ_code` = university.`univ_code`
 INNER JOIN semester_result
    ON student.`id` = semester_result.`stud_id` 
   
 WHERE semester_result.`gpa`>8
   AND semester_result.semester = '3';

SELECT student.`id` AS 'student_id'
      ,student.`roll_number`
      ,student.`name` AS 'student_name'
      ,student.`gender`
      ,college.`code` AS 'college_code'
      ,college.`name` AS 'college_name'
      ,semester_result.`semester`
      ,semester_result.`grade`
      ,semester_result.`gpa`
  FROM student
 INNER JOIN college
    ON student.`college_id` = college.`id`
 INNER JOIN university
    ON college.`univ_code` = university.`univ_code`
 INNER JOIN semester_result
    ON student.`id` = semester_result.`stud_id`

 WHERE semester_result.`gpa`>5
   AND semester_result.semester = '5';
