package factorial;

class Factorial {
    
    static int factorial(int n) {
        
        if (n == 0)// if conditional statement
            return 1;//return statement
        else
            return(n * factorial(n-1));
    }
    
    public static void main(String args[]) {
        
        int i;
        int fact = 1;
        int number = 6;//It is the number to calculate factorial
        fact = factorial(number);// method call
        System.out.println("Factorial of "+number+" is: "+fact);
        
    }
    
}