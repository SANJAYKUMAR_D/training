package withPackages;

import com.hatchmind.training.cse.Animal;
import com.hatchmind.training.cse.Dog;

public class Main{
    
    public static void main (String[] args) {
        
        String breed;// property declaration
        Dog d = new Dog();
        Animal a = new Animal();
        d.name = "Ram";
        d.printName();
        //a.printDetail(); printDetail() has protected access in Animal
        //System.out.println(a.breed); private access specifier not accessed out side the class
        
    }
    
}