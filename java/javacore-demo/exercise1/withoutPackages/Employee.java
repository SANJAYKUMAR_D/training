package withoutPackages;

//without packages
public class Employee {//class definition

    public String employeeName;//property declaration
    private String employeeId;
    
    public Employee() { // Constructor 
    
    }
    
    protected void printInfo() { //Method Declaration
        System.out.println("EmployeeName: " + employeeName); //EmployeeName:"null"
    }
    
}

class programmer extends Employee{ //Inheritance

    int salary = 10000;
    
    public static void main(String[] args){
        
        programmer obj = new programmer();//Creating pect for child class
        System.out.println("programmer salary is:" + obj.salary); //print salary: 10000
        obj.printInfo();//Method calling
        //System.out.println("employeeId: " + employeeId); private access specifier not accessed out side the class
        
    }
    
}