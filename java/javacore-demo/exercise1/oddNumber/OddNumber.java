package oddNumber;

public class OddNumber {
    
    public static void main(String[] args) {
        
        int[] numbers = {10, 21, 30, 44, 53};// using array
        
        for(int x : numbers ) {
            if( x % 2 == 0 ) {
                continue;// continue statement
            }
            System.out.print(x);
            System.out.print("\n");
        }
        
    }
    
}