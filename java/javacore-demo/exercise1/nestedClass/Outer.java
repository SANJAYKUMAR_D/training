package nestedClass;

public class Outer {
    
    public void outerMethod() {
        
        int x = 98;
        System.out.println("outerMethod");
        System.out.println("running successfully");
        
        class Inner {//nested class
        
            public void innerMethod() {
                System.out.println("innerMethod");
                System.out.println("running successfully");
                System.out.println("x= "+x);
            }
            
        }
        
        Inner y = new Inner();
        y.innerMethod();//inner class method call
    
    }
}

class Main {
    
    public static void main(String[] args) {
        Outer x = new Outer();
        x.outerMethod();//outer class method call
    }
    
}