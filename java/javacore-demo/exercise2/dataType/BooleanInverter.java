/*
Data type exercise:
==================
+ To invert the value of a boolean, which operator would you use?

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    which operator is used to invert the value of a boolean
2.Entity :
    BooleanInverter
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    i)Create a class BooleanInverter.
    ii)Initialise a value to the variables flag, res.
    iii)check res == "pass" ,print flag value
    iv)if it fails, flag = !flag and then print the flag value.

*/ 

package dataType;

public class BooleanInverter {

    public static void main(String[] args) {

        //To Invert the Value of the Boolean//
        boolean flag = false;
        String res = "Fail";

        if(res == "Pass") {
            System.out.println(flag);
        } else {
            flag = !flag;
            System.out.println(flag);
        }

    }

}