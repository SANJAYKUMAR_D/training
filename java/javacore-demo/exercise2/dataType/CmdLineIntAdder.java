/*
Data type exercise:
==================
+ Create a program that reads an unspecified number of integer arguments from the command line and adds them together.
    For example, suppose that you enter the following: java Adder 1 3 2 10
    The program should display 16 and then exit. The program should display an error message if the user enters only one argument.

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    To program that reads an unspecified number of integer arguments from the command line and 
adds them together.
2.Entity :
    CmdLineIntAdder
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    i)Create a class called CmdLineIntAdder.
    ii)Initialise numArgs variable by arguments length
    iii)Check the number of arguments is less than two otherwise else statement initialise value.
    iv)To calculate the arguments using Forloop and print the sum value.

*/

package dataType;

public class CmdLineIntAdder {

    public static void main(String[] args) {

        int numArgs = args.length;
        
        //this program requires at least two arguments on the command line
        if (numArgs < 2) {
            System.out.println("This program requires two command-line arguments.");
        } else {
             int sum = 0;
        
             for (int i = 0; i < numArgs; i++) {
                 sum += Integer.valueOf(args[i]).intValue();
             }
        
             //print the sum
             System.out.println(sum);
        }

    }

}