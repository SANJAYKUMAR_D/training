/*
Data type exercise:
==================
+ print the classname of all the primitive data types (Note: not the wrapper types)

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    the classname of all the primitive data types
2.Entity :
    PrimitiveClassName
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    i)create a class primitiveClassName
    ii)Getting the ClassName of a Primitive type by using the .class and getName method.
    iii)Storing the int class name into a String called intClassName and getting the className of the Respective 
datatype with .class and className method and then print classname.
    iv)Storing the char class name into a String called charClassName and getting the className of the Respective 
datatype with .class and className method and then print classname.
    v)Storing the double class name into a String called doubleClassName and getting the className of the Respective 
datatype with .class and className method and then print classname.
    vi)Storing the float class name into a String called floatClassName and getting the className of the Respective 
datatype with .class and className method and then print classname.

*/

package dataType;

public class PrimitiveClassName {

    public static void main(String[] args) {

        //ClassNames of Primitive Datatypes//
        
        String intClassName = int.class.getName();
        System.out.println("ClassName of Int : "+intClassName);
        
        String charClassName = char.class.getName();
        System.out.println("ClassName of Char : "+charClassName);
        
        String doubleClassName = double.class.getName();
        System.out.println("ClassName of double : "+doubleClassName);
        
        String floatClassName = float.class.getName();
        System.out.println("ClassName of float : "+floatClassName);

    }

}