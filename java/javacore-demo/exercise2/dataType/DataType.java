/*
Data type exercise:
==================
+ print the type of the result value of following expressions
  - 100 / 24
  - 100.10 / 10
  - 'Z' / 2
  - 10.5 / 0.5
  - 12.4 % 5.5
  - 100 % 56

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    print the type of the result value of following expressions
2.Entity :
    DataType
3.Function Declaration :
    public void type(int i)
    public void type(double i)
    public static void main(String[] args)
4.Jobs to be done :
    i)Create a class DataType
    ii)Create a method definition with different property(method overloading)
    iii)Program starts from main methods
    iv)Create an object t for the class DataType
    v)In method call,given expression is given as argument
    vi)According to the resultant value the method get overloaded.
    vii)The type of the resultant value get printed.
*/

package dataType;
    
class DataType {
    
    public void type(int i) {
        System.out.println(i + ": Type int");
    }
    
    public void type(double i) {
        System.out.println(i + ": Type float");
    }
    
    public static void main(String[] args) {
        
        DataType t = new DataType();
        t.type(100 / 24);
        t.type(100.10 / 10);
        t.type('Z' / 2);
        t.type(10.5 / 0.5);
        t.type(12.4 % 5.5);
        t.type(100 % 56);
        
    }
    
}
/*
output :

4: Type int
10.01: Type float
45: Type int
21.0: Type float
1.4000000000000004: Type float
44: Type int
*/