package dataType;

/*
Data type exercise:
==================
+ What is the value of the following expression, and why?
     Integer.valueOf(1).equals(Long.valueOf(1))


Answer: False. 
        The two objects (the Integer and the Long) have different types.

*/