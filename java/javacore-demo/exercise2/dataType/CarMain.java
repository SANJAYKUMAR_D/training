/*
Data type exercise:
==================
+ demonstrate overloading with Wrapper types

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    demonstrate overloading with Wrapper types
2.Entity :
    Car
    CarMain
3.Function Declaration :
    public void display(Object o)
    public void display(String s)
    public static void main(String[] args)
4.Jobs to be done :
    i)create a class Car
    ii)create a function defenition with same name and with different parameter
    iii)create a another class called CarMain
    iv)create a object audi for a class Car
    v)by calling a method (overloading takes place).

*/

package dataType;

class Car {
    
    public void display(Object o) {
       System.out.println("Object called :");
    }

    public void display(String s) {
       System.out.println("String called :"+s);
    }
    
}

public class CarMain {

    public static void main(String[] args) {

        // TODO Auto-generated method stub
        Car audi = new Car();
        audi.display(null);
        
    }
    
}