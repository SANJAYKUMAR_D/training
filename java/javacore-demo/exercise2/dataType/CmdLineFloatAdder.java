/*
Data type exercise:
==================
+ Create a program that is similar to the previous one but instead of reading integer arguments, it reads floating-point arguments.
    It displays the sum of the arguments, using exactly two digits to the right of the decimal point.
    For example, suppose that you enter the following: java FPAdder 1 1e2 3.0 4.754
    The program would display 108.75. Depending on your locale, the decimal point might be a comma (,) instead of a period (.).

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    To program that reads an unspecified number of floating point arguments from the command line and 
adds them together.
2.Entity :
    CmdLineFloatAdder
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    i)Create a class called CmdLineFloatAdder.
    ii)Initialise numArgs variable by arguments length
    iii)Check the number of arguments is less than two otherwise else statement initialise value.
    iv)To calculate the arguments using Forloop
    v)creating the obj for the class DecimalFormat. 
    vi)myFormatter.format(sum) method is used to change result format.
    vi)print the formatted value.

*/

package dataType;
import java.text.DecimalFormat;

public class CmdLineFloatAdder {

    public static void main(String[] args) {

        int numArgs = args.length;
    
        //this program requires at least two arguments on the command line
        if (numArgs < 2) {
            System.out.println("This program requires two command-line arguments.");
        } else {
            double sum = 0.0;
    
            for (int i = 0; i < numArgs; i++) {
                sum += Double.valueOf(args[i]).doubleValue();
            }
    
            //format the sum
            DecimalFormat myFormatter = new DecimalFormat("###,###.##");
            String output = myFormatter.format(sum);
    
            //print the sum
            System.out.println(output);
        }
        
    }
    
}