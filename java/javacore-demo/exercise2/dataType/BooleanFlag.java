/*
Data type exercise:
==================
+ program to store and retrieve four boolean flags using a single int

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    To store and retrieve four boolean flags using a single int
2.Entity :
    BooleanFlag
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    i)Create a class BooleanFlag.
    ii)Initialise a value to the variables knife, pistol, sword, stick and shotgun 
    iii)Print the variables initialised and create a myWeaponBag integer variable for containing five
variables
    iv)Initialise myWeaponBag variable with different variables in or condition and in the print 
statement every five variables check and condition greater than zero if condition is true prints
found with variable name,if false print that variable name not found.
*/ 

package dataType;

public class BooleanFlag {

    public static void main(String[] args) {

        // define weapons
        int knife = 1;
        int pistol = 2;
        int sword = 4;
        int stick = 8;
        int shotgun = 16;
        System.out.println("Weapons defined: knife, pistol, sword, stick, shotgun");
        
        // create weapon bag
        int myWeaponBag;
        System.out.println("Initiating weapon bag...");
        
        // store weapons in bag
        System.out.println("Storing a few weapons in bag (knife, pistol, stick)...");
        myWeaponBag = knife | pistol | stick;
        
        // check for weapons
        System.out.println("Looking for weapons...");
        System.out.println((myWeaponBag & knife) > 0 ? "--> Found knife" : "NO knife");
        System.out.println((myWeaponBag & pistol) > 0 ? "--> Found pistol" : "NO pistol");
        System.out.println((myWeaponBag & sword) > 0 ? "--> Found sword" : "NO sword");
        System.out.println((myWeaponBag & stick) > 0 ? "--> Found stick" : "NO stick");
        System.out.println((myWeaponBag & shotgun) > 0 ? "--> Found shotgun" : "NO shotgun");

    }

}