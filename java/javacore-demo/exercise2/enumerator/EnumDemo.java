/*
Enum exercise:
==============
+ compare the enum values using equal method and == operator

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    compare the enum values using equal method and == operator
2.Entity :
    EnumDemo
    Month
    Year
3.Function Declaration :
    public static void main(String[] args) 
4.Jobs to be done :
    i)create a class EnumDemo
    ii)create a enum classes.
    iii)The values compared by equals method, it will produce the result as true or false.
    iv)The values compared by == operator, it will produce error 'incomparable types' if that values
not match.
*/

package enumerator;

public class EnumDemo {

    public enum Month {
        MAR,
        APR,
        MAY,
        JUN,
        JUL,
        AUG
    }

    public enum Year {
        Y2018,
        Y2019,
        Y2020,
        Y2021,
        Y2022,
        Y2023
    }

    public static void main(String[] args) {

        Year year = Year.Y2020;
        Month aug = Month.AUG;

        if(year == Year.Y2020) {
            System.out.println("Year: " + year);
        }

        System.out.println(year.equals(aug)); // false
        //System.out.println(year == aug);  incomparable types: Year and Month
    
    }
    
}
