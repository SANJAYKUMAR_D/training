/*
java.lang class exercise:
=========================
+ demonstrate object equality using Object.equals() vs ==, using String objects

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    object equality using Object.equals() vs ==, using String objects
2.Entity :
    EqualityDemo
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    i)create a class EqualityDemo
    ii)declare a variable string1 and string2
    iii)in main method create a object for the class EqualityDemo and then initialize the variables
    iv)create object for the class String and passing value as argument
    v)print the result,that string1 == string2(it check the memory location and the value)
    vi)print the result,that string1.equals(string3) (it check the values alone)

*/

package javalang;

class EqualityDemo {
    
    public String string1;
    public String string2;

    public static void main(String[] args) {
        
        EqualityDemo obj = new EqualityDemo();
        obj.string1 = "Hello World";
        obj.string2 = "Hello World";
        
        String string3 = new String("Hello World");
        
         //check memory location
        System.out.println("Operator result: " + (obj.string1 == obj.string2));//true
        System.out.println("Operator result: " + (obj.string1 == string3)); //false
         //check values
        System.out.println("Method result: " + obj.string1.equals(string3));//true

    }

}

/*
Output :

Operator result: true
Operator result: false
Method result: true
*/