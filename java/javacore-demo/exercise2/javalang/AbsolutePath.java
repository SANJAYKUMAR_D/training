/*
java.lang class exercise:
=========================
+ print the absolute path of the .class file of the current class

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    The absolute path of the .class file of the current class
2.Entity :
    AbsolutePath 
3.Function Declaration :
    public AbsolutePath()
    public static void main( String[] args ) 
4.Jobs to be done :
    i)import a package .net
    ii)create a class AbsolutePath with constructor
    iii)inside the constructor,getResource method of URL class gets the class file which name passed as parameter.
    iv)At last,print the file path.

*/

package javalang;

import java.net.*;

class AbsolutePath {

    public AbsolutePath() {

       ClassLoader loader = this.getClass().getClassLoader();
       URL path = loader.getResource("AbsolutePath.class");
       System.out.println(path);

    }

    public static void main( String[] args ) {
        new AbsolutePath();
    }

}

/*
Output :

file:/C:/1dev/training/java/exercise2/java.lang/AbsolutePath.class
*/