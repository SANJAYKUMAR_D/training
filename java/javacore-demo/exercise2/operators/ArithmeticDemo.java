/*
Operators exercise:
===================
+ Change the following program to use compound assignments:
       class ArithmeticDemo {

            public static void main (String[] args){

                 int number = 1 + 2; // number is now 3
                 System.out.println(number);

                 number = number - 1; // number is now 2
                 System.out.println(number);

                 number = number * 2; // number is now 4
                 System.out.println(number);

                 number = number / 2; // number is now 2
                 System.out.println(number);

                 number = number + 8; // number is now 10
                 number = number % 7; // number is now 3
                 System.out.println(number);
            }
       }

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Change the following program to use compound assignments
2.Entity :
    ArithmeticDemo
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    All ordinary assignments are changed into compound assignments
*/

package operators;

class ArithmeticDemo {

    public static void main (String[] args){

        int number = 3;
        System.out.println(number);

        number -= 1; // number is now 2
        System.out.println(number);

        number *= 2; // number is now 4
        System.out.println(number);

        number /= 2; // number is now 2
        System.out.println(number);

        number += 8; // number is now 10
        number %= 7; // number is now 3
        System.out.println(number);

    }
    
}