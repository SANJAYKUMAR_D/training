/*
Control flow exercise:
======================
+ print fibonacci using for loop, while loop and recursion

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    fibonacci using for loop, while loop and recursion
2.Entity :
    FibonacciSeries
3.Function Declaration :
    public void fibonacciFor(int range) 
    public void fibonacciWhile(int range)
    public void fibonacciRecursion(int range) 
    public static void main(String[] args) 
4.Jobs to be done :
    i)Create a class FibonacciSeries,and declare a variable firstNumber,secondNumber,and nextNumber.
    ii)In the fibonacciFor method initialise the value to the variable. In for loop, check the i less than 
or equal to range variable and increment the i value.Print the firstNumber and adding firstNumber 
and secondNumber stored in nextNumber then swapping three variables.for loop continues untill the condition fail
    iii)In the fibonacciWhile method initialise the value to the variable. In for loop, check the i less than 
or equal to range.Print the firstNumber and adding firstNumber and secondNumber stored in nextNumber 
then swapping three variables.i value get incremented.while loop continues untill the condition fail
    iv)In the fibonacciRecursion method,check the condition if i less than or equal to range.Print the firstNumber 
and adding firstNumber and secondNumber stored in nextNumber then swapping three variables.calling the method fibonacciRecursion
recursion takes place untill the if condition fail
    v)In the main method create an object and passing arguments using object invoke the methods.
    vi)By calling methods, the process will execute and fibonacci series will printed.

*/

package controlFlow;

class FibonacciSeries {
    
    public int firstNumber;
    public int secondNumber;
    public int nextNumber;
    

    public void fibonacciFor(int range) {
        
        firstNumber = 0;
        secondNumber = 1;

        for (int i = 1; i <= range; i++)
        {
            System.out.print(" " + firstNumber + " ");
            int nextNumber = firstNumber + secondNumber;
            firstNumber = secondNumber;
            secondNumber = nextNumber;
        }
        
        System.out.print("\n");
    }
    
    public void fibonacciWhile(int range) {
        
        firstNumber = 0;
        secondNumber = 1;
        int i = 1;

        while(i <= range)
        {
            System.out.print(" "+ firstNumber +" ");
            int nextNumber = firstNumber + secondNumber;
            firstNumber = secondNumber;
            secondNumber = nextNumber;
            i++;
        }
        System.out.print("\n");
    }

    public void fibonacciRecursion(int range,int firstNumber,int secondNumber) {
        
        if(range > 0) {

            System.out.print(" "+ firstNumber +" ");
            int nextNumber = firstNumber + secondNumber;
            firstNumber = secondNumber;
            secondNumber = nextNumber;
            fibonacciRecursion(range - 1,firstNumber,secondNumber);

        }

    }

    public static void main(String[] args) {

        FibonacciSeries Fibonacci = new FibonacciSeries();
        Fibonacci.fibonacciFor(10);
        Fibonacci.fibonacciWhile(10);
        Fibonacci.fibonacciRecursion(10,0,1);

    }

}

/*
Output :
 0  1  1  2  3  5  8  13  21  34
 0  1  1  2  3  5  8  13  21  34
 0  1  1  2  3  5  8  13  21  34
*/