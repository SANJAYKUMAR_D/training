/*
Control flow exercise:
======================
+ Consider the following code snippet.

        if (aNumber >= 0)
            if (aNumber == 0)
                System.out.println("first string");
        else System.out.println("second string");
        System.out.println("third string");

        - What output do you think the code will produce if aNumber is 3?

        Write a test program containing the previous code snippet
        - and make aNumber 3. What is the output of the program?
        - Using only spaces and line breaks, reformat the code snippet to make the control flow easier to understand.
        - Use braces, { and }, to further clarify the code.


Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Output of the program
    Use only spaces and line breaks, reformat the code snippet to make the control flow easier to understand.
    Use braces, { and }, to further clarify the code.
2.Entity :
    NumberDemo
3.Function Declaration :
    public static void main(String[] args) 
4.Jobs to be done :
    i)Create a class NumberDemo
    ii)Initialize the variable aNumber as 3
    iii)Reformat the code by adding braces, spaces and line breaks.
*/

package controlFlow;

class NumberDemo {

    public static void main(String[] args) {

        int aNumber = 3;

        if (aNumber >= 0) {

            if (aNumber == 0) {
                System.out.println("first string");
            }

        } else {
            System.out.println("second string");
        }

        System.out.println("third string");
    }
    
}

/*
Output :

third string
*/

