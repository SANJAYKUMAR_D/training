/*
String exercise: 
================
+ What is the initial capacity of the following string builder?
    StringBuilder sb = new StringBuilder("Able was I ere I saw Elba.");

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Initial capacity of the following string builder
2.Entity :
    Capacity
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    i)create a class StringBuilder
    ii)create an object of StringBuilder and print default capacity of StringBuilder
    iii)create an object of StringBuilder and print the capacity by passing a string
*/

package string;

public class Capacity{
    
    public static void main(String[] args) {
        
        // default stringBuilder has a capacity of 16 characters.
        StringBuilder sbr1 = new StringBuilder();
        System.out.println(sbr1.capacity() + " Characters"); // 16 Characters
        
        // passing a string makes the length added to the default capacity 
        StringBuilder sbr2 = new StringBuilder("Able was I ere I saw Elba.");
        System.out.println(sbr2.capacity() + " Characters"); // 42 Characters
        
    }
    
}

/*
Output :

16 Characters
42 Characters
*/