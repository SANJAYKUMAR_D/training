/*
String exercise: 
================
+ Consider the following string:
    String hannah = "Did Hannah see bees? Hannah did.";
    - What is the value displayed by the expression hannah.length()?
    - What is the value returned by the method call hannah.charAt(12)?
    - Write an expression that refers to the letter b in the string referred to by hannah.

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    The value displayed by the expression hannah.length()?
    The value returned by the method call hannah.charAt(12)?
    An expression that refers to the letter b in the string referred to by hannah.
2.Entity :
    HannahDemo
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    i)create a class HannahDemo
    ii)inside main method ,initialize the variable hannah
    iii)print length of the string stored in the variable hannah 
    iv)print character at index 12 in hannah
    v)print the character in a string to get the word with the character 'b'.

*/

package string;

class HannahDemo {

    public static String hannah;

    public static void main(String[] args) {

        hannah = "Did Hannah see bees? Hannah did.";
        System.out.println(hannah.length()); //32
        System.out.println(hannah.charAt(12)); //e
        String[] words = hannah.split(" ");

        for(String word : words) {

            for(int i = 0; i < word.length(); i++) {

                if(word.charAt(i) == 'b') {
                    System.out.println(word); //bees?
                }

            }

        }

    }

}

/*
Output :
32
e
bees?
*/