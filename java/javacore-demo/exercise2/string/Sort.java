/*
String exercise: 
================
+ sort and print following String[] alphabetically ignoring case. Also convert and print even indexed Strings into uppercase
       { "Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem" }

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    sort and print following String[] alphabetically ignoring case
    convert and print even indexed Strings into uppercase
2.Entity :
    Sort
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    i)create a class Sort
    ii)initialize the array with String district
    iii)sorting had done with sort method
    iv)Next sort the even indexed district in the array.

*/

package string;

import java.util.Arrays;

class Sort {
    
    public static void main(String[] args) {
        
        String[] districts = { "Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem" };
        Arrays.sort( districts, 0, districts.length );
        
        for (String district : districts) {
            System.out.print(district + " ,");
        }
        
        System.out.println("\nEven indexed districts:");
        
        for(int i = 0; i < districts.length; i++) {
            if(i % 2 == 0) {
                String dist = districts[i].toUpperCase();
                System.out.print(dist +" ,");
            }
        }
        
    }
    
}

/*
Output :
Erode ,Karur ,Madurai ,Salem ,TRICHY ,Thanjavur ,trichy ,
Even indexed districts:
ERODE ,MADURAI ,TRICHY ,TRICHY ,
*/