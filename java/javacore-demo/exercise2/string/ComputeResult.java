/*
String exercise: 
================
+ In the following program, called ComputeResult, what is the value of result after each numbered line executes?
    public class ComputeResult {
        public static void main(String[] args) {
            String original = "software";
            StringBuilder result = new StringBuilder("hi");
            int index = original.indexOf('a');

    /*1*   result.setCharAt(0, original.charAt(0));
    /*2*   result.setCharAt(1, original.charAt(original.length()-1));
    /*3*   result.insert(1, original.charAt(4));
    /*4*   result.append(original.substring(1,4));
    /*5*   result.insert(3, (original.substring(index, index+2) + " "));

            System.out.println(result);
        }
    }

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    the value of result after each numbered line executes.
2.Entity :
    ComputeResult
3.Function Declaration :
    public static void main(String[] args) 
4.Jobs to be done :
    i)After initialization and object creation,following methods used.
    ii)setCharAt method is used to set a character value that changes the actual one.
    iii)insert method is used to insert a character or substring at a particular location/index.
    iv)append method is used to add string at the end. 

*/

package string;

public class ComputeResult {

    public static void main(String[] args) {

        String original = "software";
        StringBuilder result = new StringBuilder("hi");
        int index = original.indexOf('a');

        result.setCharAt(0, original.charAt(0)); // si
        result.setCharAt(1, original.charAt(original.length()-1)); //se
        result.insert(1, original.charAt(4)); //swe
        result.append(original.substring(1,4)); //sweoft
        result.insert(3, (original.substring(index, index+2) + " ")); //swear oft

        System.out.println(result);
    }
    
}

