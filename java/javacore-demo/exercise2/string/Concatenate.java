/*
String exercise: 
================
+ Show two ways to concatenate the following two strings together to get the string "Hi, mom.":
    String hi = "Hi, ";
    String mom = "mom.";

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Show two ways to concatenate the two strings
2.Entity :
    Concatenate
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    i)Create a class Concatenate
    ii)Initialize the given words as variable name firstWord and secondword
    iii)Concatenate is done by using '+' symbol
    iv)In another way Concatenate is done by using concat() method
*/

package string;

class Concatenate {
    
    public static void main(String[] args) {
        
        String firstWord = "Hi, ";
        String secondword = "mom.";
        String result = (firstWord + secondword);
        System.out.println(result);
        System.out.println(result = firstWord.concat(secondword));
        
    }
    
}

/*
Output :

Hi, mom.
Hi, mom.
*/
