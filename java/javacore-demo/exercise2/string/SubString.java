/*
String exercise:
================
+ How long is the string returned by the following expression? What is the string?
"Was it a car or a cat I saw?".substring(9, 12)

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    How long is the string returned by the following expression
    What is the string
2.Entity :
    SubString
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    i)create a class SubString
    ii)initialize the string str with given expression
    iii)To get part of the string,simply can use the substring method with index value.
    iv)Using the length method to find the length of the string.
    v)print the result

*/

package string;

class SubString {
    
    public static void main(String[] args) {
        
        String str = "Was it a car or a cat I saw?";
        System.out.println(str);
        System.out.println("String length: " + str.length());
        String substr = str.substring(9, 12);
        System.out.println(substr);
        System.out.println("String length: " + substr.length());
    
    }

}

/*
Output :

Was it a car or a cat I saw?
String length: 28
car
String length: 3
*/