/*
String exercise: 
================
+ Write a program that computes your initials from your full name and displays them.

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    program that computes your initials from your full name and displays them.
2.Entity :
    Initials
3.Function Declaration :
    static void printInitials(String name)
    public static void main(String args[]) 
4.Jobs to be done :
    i)created a class Initials
    ii)created a method printInitials to compute and display the initials from full name
    iii)get input from the user
    iv)print the first char from the name and then search for empty single space
    v)IF space find,print the next char from the space 
    vi)step v) repeats untill the condition in for loop exists 
*/

package string;

import java.util.Scanner;

public class Initials {
    
    static void printInitials(String name) {
        
        // Since touuper() returns int,
        // we do typecasting
        System.out.print(Character.toUpperCase(name.charAt(0)));
    
        // print the characters after spaces.
        for (int i = 1; i < name.length() - 1; i++)
            if (name.charAt(i) == ' ')
                System.out.print(" " + Character.toUpperCase( name.charAt(i + 1)));
    }
    
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
        String name = sc.nextLine();
        printInitials(name);
        
    }
    
}

/*
Output :
sanjay kumar
S K
*/