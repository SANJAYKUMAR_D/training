/*
Class and object exercise:
==========================
+ Given the following class, called NumberHolder,
    write some code that creates an instance of the class and initializes its two member variables with provided values,
    and then displays the value of each member variable.

    public class NumberHolder {
        public int anInt;
        public float aFloat;
    }
Work Breakdown Structure(WBS):
==============================
1.Requirement :
    create a code that creates an instance of the class and initializes its two member variables with provided values,
    and then displays the value of each member variable.
2.Entity :
    NumberHolder
3.Function Declaration :
    public void printInfo(int anInt,float aFloat)
    public static void main(String args[])
4.Jobs to be done :
    i)Create method definition printInfo with two parameters
    ii)Create main method.inside that,
    iii)Create object numHold for the class NumberHolder
    iv)Initializes the two member variables with provided values
    v)Call the method printInfo to print the values stored in the member variable anInt and aFloat
*/

package classObject;

public class NumberHolder {
    
    public int anInt;
    public float aFloat;
    
    public void printInfo(int anInt,float aFloat) {
        System.out.println("anInt = " + anInt +" "+ "aFloat = " + aFloat);
    }
    
    public static void main(String[] args) {
        
        NumberHolder numHold = new NumberHolder();
        numHold.anInt = 100;
        numHold.aFloat = 0.06f;
        numHold.printInfo(numHold.anInt,numHold.aFloat);
        
    }
    
}
