/*
Class and object exercise:
==========================
+ What's wrong with the following program? And fix it.

    public class SomethingIsWrong {
        public static void main(String[] args) {
            Rectangle myRect;
            myRect.width = 40;
            myRect.height = 50;
            System.out.println("myRect's area is " + myRect.area());
        }
    }
*/

package classObject;

public class Rectangle {
    
    public int width;
    public int height;

    public int area(int width,int height) {
        return width*height;
    }

    public static void main(String[] args) {
        
        Rectangle myRect = new Rectangle();
        myRect.width = 40;
        myRect.height = 50;
        System.out.println("myRect's area is " + myRect.area(myRect.width,myRect.height));
        
    }
    
}