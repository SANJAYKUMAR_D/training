/*
Inheritance:
===========
+ demonstrate inheritance, overloading, overriding using Animal, Dog, Cat and Snake class of objects

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    demonstrate inheritance, overloading, overriding using Animal, Dog, Cat and Snake class of objects
2.Entity :
    Animal
    Dog
    Cat
    Snake
    Inheritance
3.Function Declaration :
    public void eatfood()
    public void eatfood(int count)  
    public void printHeight(int height)
    public void sound()
    public static void main(String[] args)
4.Jobs to be done :
    i)create a parent class Animal
    ii)create a child class as Dog, Cat, Snake by extends Animal
    iii)in Snake class method name is same,for getting method overloading
    iv)create a object for child class and access parent class method(method overriding)
    v)by calling method with different argument,overloading and overriding occurs
      
*/

package inheritance;

class Animal {
    
    public void eatfood() {
        System.out.println("Eating food...");
    }
    
    public void printHeight(int height) {
        System.out.println(String.format("Animal height is:", + height));
    }
    
}
class Dog extends Animal {
    
    public void sound() {
        System.out.println("Woof");
    }
    
    public void printHeight(int height) {
        System.out.println(String.format("Dog height is:", + height));
    }
    
}
class Cat extends Animal {
    
    public void sound() {
        System.out.println("Meow");
    }
    
    public void printHeight(int height) {
        System.out.println(String.format("Cat height is:", + height));
    }
    
}
class Snake extends Animal {
    
    public void eatfood() {
        System.out.println("Eating a frog...");
    }
    
   public void eatfood(int count) {
        System.out.println("Eating a frog..." + count);
    }
    
}

public class Inheritance {
    
    public static void main(String[] args) {
        
        //method overloading
        Snake cobra = new Snake();
        cobra.eatfood();
        cobra.eatfood(4);
        Cat c = new Cat();
        c.sound();
        //method overriding
        Animal pug = new Dog();
        pug.eatfood();
        
    }
    
}
