/*
VarArgs exercise:
================
+ demonstrate overloading with varArgs

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    demonstrate overloading with varArgs
2.Entity :
    VarArgs
3.Function Declaration :
    static void printInfo(int ... a) 
    static void printInfo(boolean ... a)
    public static void main(String[] args)
4.Jobs to be done :
    i)create a class VarArgs
    ii)create a method definition with different parameter but both has a
common name 'printInfo'.
    iii)calling a overloaded method() with different parameter
*/

package varargs;

public class VarArgs {
    
    public void printInfo(int ... a) {// A method that takes varargs
    
        System.out.print("(int ...): " + "No of arguments: " + a.length + 
                " Contents: ");
        
        for(int x : a)
            System.out.print(x + " ");
        
        System.out.println();
    }
    
    public void printInfo(boolean ... a) {// A method that takes varargs
    
        System.out.print("(boolean ...) " + "No of arguments: " + a.length +
                " Contents: ");
        
        for(boolean x : a)
            System.out.print(x + " ");
        
        System.out.println();
    }
    
    public static void main(String[] args) {
    
        VarArgs obj = new VarArgs();
        // Calling overloaded methods()with different parameter 
        obj.printInfo(1, 2, 3); //OK 
        obj.printInfo(true, false); //OK 
        //obj.printInfo();  Error: Ambiguous
        
    }
    
}