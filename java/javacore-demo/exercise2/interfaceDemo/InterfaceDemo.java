package interfaceDemo;
/*
Interfaces:
===========
+ What is wrong with the following interface? and fix it.
    public interface SomethingIsWrong {
        void aMethod(int aValue){
            System.out.println("Hi Mom");
        }
    }

Answer:
=======
    In this interface, has a method implementation in it.Only default 
and static methods have implementations.
    By adding default keyword in method definition, the method changed as default
    Now the method get implemented.


public interface InterfaceDemo {

    default void aMethod(int aValue) { //define aMethod as a default method
        System.out.println("Hi Mom");
    }

}

*/