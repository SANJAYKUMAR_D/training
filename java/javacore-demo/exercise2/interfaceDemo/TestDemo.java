package interfaceDemo;
/*
Interfaces:
===========
+ Write a class that implements the CharSequence interface found in the java.lang package.
  Your implementation should return the string backwards. Write a small main method to test your class; make sure to call all four methods.

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Write a class that implements the CharSequence interface
    implementation should return the string backwards.
    test your class; make sure to call all four methods.
2.Entity :
    TestDemo
3.Function Declaration :
    public TestDemo(String string)
    public char charAt(int index) 
    public int length()
    public String subSequence(int start,int end)
    public String toString()
    public static void main(String[] args)
4.Jobs to be done :
    i)Create a class TestDemo that implements the CharSequence interface.
    ii)Creating a constructor that stores the string to an instance variable string.
    iii)charAt method returns the character at the particular index.
    iv)length method returns the length of the string.
    v)subSequence method returns the part of a string by specifing start and end index.
    vi)toString method that overrides the default toString method in java, this reversing the
string and storing on object in StringBuilder class .
    vii)In main method create a object to the class TestDemo
    viii)Print the return values by calling the method 
*/

class TestDemo implements CharSequence {
    
    String string;
    
    public TestDemo(String string) {
        this.string = string;
    }
    
    public char charAt(int index) {
        return string.charAt(index);
    }
    
    public int length() {
        return string.length();
    }
    
    public String subSequence(int start,int end) {
        return string.substring(start, end);
    }
    
    public String toString() {
        
        int insertIndex = 0;
        StringBuilder reversed = new StringBuilder();
        
        for(int index = string.length() - 1; index >= 0; index--) {
            reversed.insert(insertIndex, string.charAt(index));
            insertIndex ++;
        }
        
        return reversed.toString();
    }
    
    public static void main(String[] args) {
        
        TestDemo obj = new TestDemo("HATCHMIND");
        System.out.println(obj.charAt(5));
        System.out.println(obj.length());
        System.out.println(obj.subSequence(2,5));
        System.out.println(obj.toString());
        
    }
    
}

/*
Output :

M
9
TCH
DNIMHCTAH
*/