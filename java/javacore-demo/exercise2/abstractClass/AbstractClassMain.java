/*
Abstract class:
===============
+ Demonstrate abstract classes using Shape class.
    - Shape class should have methods to calculate area and perimeter
    - Define two more class Circle and Square by extending Shape class and implement the calculation for each class respectively

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Demonstrate abstract classes using Shape class.
    Shape class should have methods to calculate area and perimeter
    two more class Circle and Square by extending Shape class and implement the calculation for each class respectively
2.Entity :
    Shape
    Circle
    Square
    AbstractClassMain
3.Function Declaration :
    public abstract void findArea()
    public abstract void findPerimeter()
    public Circle(float radius)
    public Square(float side)
    public static void main(String[] args)
4.Jobs to be done :
    i)Create a abstract class,inside that declare a properties and methods.
    ii)Create a class Circle and Rectangle that extends from abstract class Shape
    iii)In class Circle and Rectangle, definition for findArea and findPerimeter method created
    iv)Creating object for the classes
    v)By calling methods, value calculated from the methods printed.

*/

package abstractClass;

abstract class Shape {
    
    public float area;
    public float perimeter;
    public abstract void findArea();
    public abstract void findPerimeter();
    
}
class Circle extends Shape {
    
    public float radius;
    public final float pi = 3.141592f;
    
    public Circle(float radius) {
        this.radius = radius;
    }
    
    public void findArea() {
        area = pi * radius * radius;
        System.out.println(String.format("Area of circle: %.2f", area));
    }
    
    public void findPerimeter() {
        perimeter = 2 * pi * radius;
        System.out.println(String.format("Perimeter of circle: %.2f", perimeter));
    }
    
}

class Square extends Shape {
    
    public float side;
    
    public Square(float side) {
        this.side = side;
    }
    
    public void findArea() {
        area = side * side;
        System.out.format("Area of square: %.2f\n", area);
    }
    
    public void findPerimeter() {
        perimeter = 4 * side;
        System.out.format("Perimeter of square: %.2f\n", perimeter);
    }
    
}

public class AbstractClassMain {

    public static void main(String[] args) {
        
        Shape coin = new Circle(4.2f);
        coin.findArea();
        coin.findPerimeter();
        
        Shape carrom = new Square(6.0f);
        carrom.findArea();
        carrom.findPerimeter();

    }

}

/*
Output :

Area of circle: 55.42
Perimeter of circle: 26.39
Area of square: 36.00
Perimeter of square: 24.00
*/