/*
java doc exercise:
==================
+ Use the Java API documentation for the Box class (in the javax.swing package) to help you answer the following questions.
    - What static nested class does Box define?
    - What inner class does Box define?
    - What is the superclass of Box's inner class?
    - Which of Box's nested classes can you use from any class?
    - How do you create an instance of Box's Filler class?

Answer:
=======
What static nested class does Box define?
    Box.Filler
What inner class does Box define?
    Box.AccessibleBox
What is the superclass of Box's inner class?
    [java.awt.]Container.AccessibleAWTContainer
Which of Box's nested classes can you use from any class?
    Box.Filler
How do you create an instance of Box's Filler class?
    Box.Filler obj = new Box.Filler(minDimension, prefDimension, maxDimension);

*/