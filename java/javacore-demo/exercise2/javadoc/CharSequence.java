/*
java doc exercise:
==================
+ What methods would a class that implements the java.lang.CharSequence interface have to implement?

Answer:
=======
Methods in java.lang.CharSequence interface :
1.charAt(index)
2.length()   
3.subSequence(startValue, endValue)  
4.toString()

*/
