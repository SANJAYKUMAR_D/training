/*
java doc exercise:
==================
+ What Integer method would you use to convert a string expressed in base 5 into the equivalent int?
  For example, how would you convert the string "230" into the integer value 65? Show the code you would use to accomplish this task.

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    What Integer method used to convert a string expressed in base 5 into the equivalent int
2.Entity :
    IntConversion
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    i)create a class IntConversion
    ii)get a string value and base value from the user,by scanner class
    iii)print the integer by using Integer.valueOf(value,base) method.

*/

package javadoc;

import java.util.Scanner;

public class IntConversion {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the number: ");
        String value = sc.nextLine();
        System.out.print("Enter the base value: ");
        int base = sc.nextInt();
        System.out.println(Integer.valueOf(value, base));

    }

}