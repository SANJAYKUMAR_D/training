/*
java doc exercise:
==================
+ What Integer method can you use to convert an int into a string that expresses the number in hexadecimal?
  For example, what method converts the integer 65 into the string "41"?

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    What Integer method used to convert an int into a string that expresses the number in hexadecimal
2.Entity :
    IntConversion
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    i)Create a class IntToStringConversion
    ii)Initialize the integer to the variable
    iii)Print the hexadecimal number by using Integer.toHexString(number) method.

*/

package javadoc;

public class IntToStringConversion {

    public static void main(String[] args) {

        int number1 = 10;
        int number2 = 65;
        
        System.out.println(Integer.toHexString(number1)); // a
        System.out.println(Integer.toHexString(number2)); // 41

    }

}

/*
Output :
a
41
*/