/*
java doc exercise:
==================
+ What Double method can you use to detect whether a floating-point number 
  has the special value Not a Number (NaN)?

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Double method can you use to detect whether a floating-point number 
  has the special value Not a Number (NaN)
2.Entity :
    NotANumber
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    i)create a class NotANumber
    ii)print whether a floating-point number has the special value Not a Number
by using Double.isNaN(value) method
*/

package javadoc;

class NotANumber {

    public static void main(String[] args) {

        System.out.println(Double.isNaN(7.10));
        System.out.println(Double.isNaN(12.0/0));

    }

}