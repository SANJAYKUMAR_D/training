/*
1.Entity:
    1.Address
2.Jobs to be Done:
    1.Create a class with Address variables.
    2.Create constructor to Address class for assign vlaues.
        2.1)Address.id variable is auto generated.
    3.Create getter and setter methods for private variables.
3.Pseudo Code:
public class Address {
	public static long id;
	public static String street;
	public static String city;
    public static int postal_code;
	
	public Address(long id) {
		this.id = id;
	}

	public Address(String street, String city, int postalCode) {
		this.street = street;
		this.city = city;
		this.postalCode = postalCode;
	}

	public Address(long id, String street, String city, int postalCode) {
		this.id = id;
		this.street = street;
		this.city = city;
		this.postalCode = postalCode;
	}
	
	//Getter & Setter
	  
	//toString method 
}
*/

package com.kpr.training.jdbc.model;

public class Address {

	public long id;
	private String street;
	private String city;
	private int postalCode;

	public Address(long id) {
		this.id = id;
	}

	public Address(String street, String city, int postalCode) {
		this.street = street;
		this.city = city;
		this.postalCode = postalCode;
	}

	public Address(long id, String street, String city, int postalCode) {
		this.id = id;
		this.street = street;
		this.city = city;
		this.postalCode = postalCode;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(int postalCode) {
		this.postalCode = postalCode;
	}

	@Override
	public String toString() {
		return new StringBuilder("Address [id = ").append(id).append(", street = ").append(street).append(", city = ")
				.append(city).append(", postalCode = ").append(postalCode).append("]").toString();
	}
	
}
