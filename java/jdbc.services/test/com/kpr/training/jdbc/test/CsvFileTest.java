package com.kpr.training.jdbc.test;

import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.kpr.training.jdbc.model.Person;
import com.kpr.training.jdbc.service.ConnectionService;
import com.kpr.training.jdbc.service.PersonService;

public class CsvFileTest {

	PersonService personService = new PersonService();
	ArrayList<Person> personsLength = new ArrayList<Person>();
	CountDownLatch latch = new CountDownLatch(1);
	
	@Test(priority = 1, description = "Create Persons Using CSV file ")
	public void InsertPersonUsingCsvFile() throws Exception {
			
		
		String csvFilePath = "C:/Users/SanjayKumar/eclipse-workspace/jdbc.services/resources/PersonInformation.csv";
		personsLength = personService.csvPersonInsertion(csvFilePath);
		
		Assert.assertEquals(14, personsLength.size());
		
		if (23 == personsLength.size()) {
			ConnectionService.commitRollback(true);
		} else {
			ConnectionService.commitRollback(false);
		}
	}
	
}
