/*
Problem Statement:
    1.Perform CRUD Operations for Address Service

Work Breakdown Structure(WBS):
==============================
1.Requirement:
    Perform CRUD Operations for Address Service.
2.Entity:
    1.Address
    2.AddressService
    3.AppException
    4.ErrorCode
    5.ConnectionService
3.Method Signature:
    1.public static long insertAddress(Address address)
    2.public static void readAddress(long id)
    3.public static void readAllAddress()
    4.public int updateAddress(Address address, long id)
    5.public Boolean deleteAddress(long id)
4.Jobs to be Done:
    1.Create a class with static variables.
    2.Establish the connection using getConnection method of DriverManager.
    
    //To insert address:
    3.Check whether the postal code is empty
        3.1)if is empty, Throw an AppException with ErrorCode.
    4.Invoke a PreparedStatement for query to insert the address.
    5.Get the address_id from RETURN_GENERATED_KEYS and return the id.
    6.Set the values in database using setString() method.
    
    //To read a address:
    7.Invoke a PreparedStatement for query to select particular address
    8.Execute and get the ResultSet using executeQuery method of PreparedStatement
    9.Display the address.
    
    //To read all the addresses:
    10.Invoke a PreparedStatement for query to select all the addresses.
    11.Get address from ResultSet using next() method in while loop
    12.Display the address

    /To update a address:
    13.Check whether the postal code is empty
        13.1)if is empty, Throw an AppException with ErrorCode.
    14.Get the address_id and the address to be updated
    15.Invoke a PreparedStatement for query to update the address
    16.Execute the PreparedStatement using executeUpdate() method
    17.Check whether the rows affected or not
        17.1)if not affected Throw an AppException with ErrorCode

    //To delete a address:
	18)Get the id of address to be deleted.
	19)Invoke a PreparedStatement for query to delete the address
	20)Execute the PreparedStatement using executeUpdated() method
	21)Check whether the rows affected or not
        21.1)if not affected Throw an AppException with ErrorCode
    22)Close the connection after the operation is done.
5.Pseudo code:
public class AddressService {
	
	private static long addressId;
	private static int count;
	private static Connection connection;
	
	public static long insertAddress(Address address) throws SQLException {
		
		try {
			if (address.getPostalCode() == 0) {
				throw new AppException("402");
			}

			connection = Connections.getConnection();

			PreparedStatement statement = connection.prepareStatement(QueryStatement.insertAddressQuery,
					Statement.RETURN_GENERATED_KEYS);

			statement.setString(1, address.getStreet());
			statement.setString(2, address.getCity());
			statement.setLong(3, address.getPostalCode());

			addressId = statement.executeUpdate();

			if (addressId == 0) {
				throw new AppException("403");
			}

		} catch (AppException e) {
			e.printStackTrace();
		} finally {
            try {
				if (connection != null) {
					connection.close(); 
				}
            } catch (SQLException sql) {
                System.out.println(sql.getMessage());
            }
        }
		return addressId;
	}

	public static void readAddress(long id) throws SQLException {
		
		try {
			connection = Connections.getConnection();
			PreparedStatement ps = connection.prepareStatement(QueryStatement.readAddressQuery);
			ps.setLong(1, id);

			ResultSet result = ps.executeQuery();
			if (result.next()) {
				String street = result.getString("street");
				String city = result.getString("city");
				String postal_code = result.getString("postal_code");
				System.out.println(city + " " + street + " " + postal_code);
			}
		} catch (SQLException sql) {
			System.out.println(sql.getMessage());
		} finally {
			try {
				if (connection != null) {
					connection.close(); 
				}
			} catch (SQLException sql) {
				System.out.println(sql.getMessage());
			}
		}
		
	}

	public static void readAllAddress() throws SQLException {
		
		try {
			connection = Connections.getConnection();
			PreparedStatement ps = connection.prepareStatement(QueryStatement.readAllAddressQuery);
			ResultSet result = ps.executeQuery();
			while (result.next()) {
				String street = result.getString("street");
				String city = result.getString("city");
				String postal_code = result.getString("postal_code");
				System.out.println(city + " " + street + " " + postal_code);
			}
		} catch (SQLException sql) {
			System.out.println(sql.getMessage());
		} finally {
			try {
				if (connection != null) {
					connection.close(); 
				}
			} catch (SQLException sql) {
				System.out.println(sql.getMessage());
			}
		}
		
	}

	public static int updateAddress(Address address, long id) throws SQLException {
		
		try {
			if (address.getPostalCode() == 0) {
				throw new AppException("402");
			}
			
			Connection connection = Connections.getConnection();

			PreparedStatement statement = connection.prepareStatement(QueryStatement.updateAddressQuery);

			statement.setString(1, address.getStreet());
			statement.setString(2, address.getCity());
			statement.setLong(3, address.getPostalCode());
			statement.setLong(4, id);

			count = statement.executeUpdate();

			if (count != 0) {
				throw new AppException("403");
			}
		} catch (AppException e) {
			e.printStackTrace();
		} finally {	
            try {
				if (connection != null) {
					connection.close(); 
				}
            } catch (SQLException sql) {
                System.out.println(sql.getMessage());
            }     
        }
		return count;
	}

	public static Boolean deleteAddress(long id) throws SQLException {
		
		boolean delete = false;
		try {
			Connection connection = Connections.getConnection();
			PreparedStatement ps = connection.prepareStatement(QueryStatement.deleteAddressQuery);
			ps.setLong(1, id);

			if (ps.executeUpdate() != 1) {
				throw new AppException("405");
			} else {
				System.out.println("Address Deleted Successfully");
				delete = true;
			}

		} catch (AppException e) {
			System.out.println(e.getMessage());
		} catch (SQLException sql) {
			System.out.println(sql.getMessage());
		} finally {
			try {
				if (connection != null) {
					connection.close(); 
				}
			} catch (SQLException sql) {
				System.out.println(sql.getMessage());
			}
		}
		return delete;
	}

}
    
*/

package com.kpr.training.jdbc.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.kpr.training.jdbc.constant.Constant;
import com.kpr.training.jdbc.constant.QueryStatement;
import com.kpr.training.jdbc.exception.AppException;
import com.kpr.training.jdbc.exception.ErrorCode;
import com.kpr.training.jdbc.model.Address;
import com.kpr.training.jdbc.model.Person;
import com.kpr.training.jdbc.validation.AddressValidator;

public class AddressService {
	
	Address address;
	Person person;
	long addressId = 0;

	public long checkAddressOrCreate(Address address) {
		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.GET_ADDRESS_ID_QUERY)) {

			setPs(ps, address);

			ResultSet resultSet = ps.executeQuery();
			if (resultSet.next()) {
				
				return resultSet.getLong(Constant.ID);
			}
			return create(address);

		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR32, e);
		}
	}

	public long create(Address address) {

		AddressValidator.isPostalCodeEmpty(address);

		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.CREATE_ADDRESS_QUERY,
						Statement.RETURN_GENERATED_KEYS)) {

			setPs(ps, address);
			ResultSet resultSet;

			if (ps.executeUpdate() == 1 && (resultSet = ps.getGeneratedKeys()).next()) {
				return resultSet.getLong(Constant.GENERATED_ID);
			}
			return 0;

		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR01, e);
		}
	}

	public Address read(long id) {

		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.READ_ADDRESS_QUERY)) {

			ps.setLong(1, id);
			ResultSet resultSet = ps.executeQuery();

			if (!resultSet.next()) {
				return null;
			}
			return getRs(resultSet);

		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR02, e);
		}

	}

	public ArrayList<Address> readAll() {

		ArrayList<Address> addresses = new ArrayList<Address>();
		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.READ_ALL_ADDRESS_QUERY)) {

			ResultSet resultSet = ps.executeQuery();

			while (resultSet.next()) {
				addresses.add(getRs(resultSet));
			}
			return addresses;

		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR03, e);
		}

	}

	public void update(Address address, long id) {

		AddressValidator.isPostalCodeEmpty(address);
		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.UPDATE_ADDRESS_QUERY)) {

			setPs(ps, address);
			ps.setLong(4, id);

			if (ps.executeUpdate() != 1) {
				throw new AppException(ErrorCode.ERR34);
			}

		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR04, e);
		}
	}

	public void delete(long id) {

		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.DELETE_ADDRESS_QUERY)) {
			ps.setLong(1, id);

			if (ps.executeUpdate() != 1) {
				throw new AppException(ErrorCode.ERR34);
			}

		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR05, e);
		}
	}

	public void deleteNotUsedAddress() {

		try (PreparedStatement ps = ConnectionService.get()
				.prepareStatement(QueryStatement.DELETE_NOT_USED_ADDRESS_QUERY)) {

			ResultSet resultSet = ps.executeQuery();

			if (resultSet.next()) {
				delete(resultSet.getLong(Constant.ID));
			}
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR36, e);
		}
	}

	public List<Address> search(Address address) {

		List<Address> addresses = new ArrayList<>();

		if (address.getStreet() == null && address.getCity() == null && address.getPostalCode() == 0) {
			throw new AppException(ErrorCode.ERR37);
		}

		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.ADDRESS_SEARCH_QUERY)) {

			setPs(ps, address);
			ResultSet resultSet = ps.executeQuery();

			if (address.getStreet() != null || address.getCity() != null || address.getPostalCode() != 0) {
				while (resultSet.next()) {
					addresses.add(getRs(resultSet));
				}
			}
			return addresses;
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR12);
		}
	}

	public Address getRs(ResultSet resultSet) throws SQLException {
		return new Address(resultSet.getLong(Constant.ID), resultSet.getString(Constant.STREET),
				resultSet.getString(Constant.CITY), resultSet.getInt(Constant.POSTAL_CODE));
	}

	public void setPs(PreparedStatement ps, Address address) throws SQLException {
		ps.setString(1, address.getStreet());
		ps.setString(2, address.getCity());
		ps.setInt(3, address.getPostalCode());
	}

	public static void main(String[] args) throws Exception {
		AddressService addressService = new AddressService();
		Connection connection = ConnectionService.get(); 
		long id = addressService.create(new Address("T Nagar", "Chennai", 600028));
		System.out.println(id);
		
		if(id > 0) {
			connection.commit();
		} else {
			connection.rollback();
		}
	}

}