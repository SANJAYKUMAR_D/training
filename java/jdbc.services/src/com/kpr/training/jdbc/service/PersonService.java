/*
Problem Statement:
    1.Perform CRUD Operations for Person Service

Work Breakdown Structure(WBS):
==============================
1.Requirement:
    Perform CRUD Operations for Person Service.
2.Entity:
    1.Person
    2.PersonService
    3.AppException
    4.ErrorCode
3.Method Signature:
    1.public static long insertPerson(Person person)
    2.public static void updatePerson(Person person, long id)
    3.public static void readAllPerson()
    4.public static void readPerson(long id)
    5.public static Boolean deletePerson(long id)
4.Jobs to be Done:
    1.Create a class with static variables.
    2.Establish the connection using getConnection method of DriverManager.
    
    //To insert person details:
    3.Check whether the email valid or not
        3.1)if is invalid, Throw an AppException with ErrorCode.
    4.Invoke a PreparedStatement for query to insert the person details.
    5.Get the persor_id from RETURN_GENERATED_KEYS and return the id.
    6.Set the values in database using setString() method.
    
    //To read the person details:
    7.Invoke a PreparedStatement for query to select particular person details
    8.Execute and get the ResultSet using executeQuery method of PreparedStatement
    9.Display the person details.
    
    //To read all the persons detail:
    10.Invoke a PreparedStatement for query to select all the persons detail.
    11.Get person details from ResultSet using next() method in while loop
    12.Display the person details

    /To update the person details:
    13.Check whether the email valid or not
        13.1)if is invalid, Throw an AppException with ErrorCode.
    14.Get the person_id and the person details to be updated
    15.Invoke a PreparedStatement for query to update the person details
    16.Execute the PreparedStatement using executeUpdate() method
    17.Check whether the rows affected or not
        17.1)if not affected Throw an AppException with ErrorCode

    //To delete the person details:
	18)Get the id of person to be deleted.
	19)Invoke a PreparedStatement for query to delete the person details
	20)Execute the PreparedStatement using executeUpdated() method
	21)Check whether the rows affected or not
        21.1)if not affected Throw an AppException with ErrorCode
    22)Close the connection after the operation is done.
5.Pseudo code:

public class PersonService {

	private static long personId;
	private static Connection connection;
	public static long insertPerson(Person person) throws SQLException {
		try {  
			   
	        Pattern p = Pattern.compile("^(.+)@(.+)$");
		    Matcher m = p.matcher(person.getEmail());
		    
		    if(!m.find()) {
		        throw new AppException("406"); 
		    }
		    
		    connection = Connections.getConnection();
		   	PreparedStatement statement = connection.prepareStatement(QueryStatement.insertPersonQuery);
			statement.setString(1, person.getName());
			statement.setString(2, person.getEmail());
			statement.setLong(3, person.getAddressId());
			statement.setString(4, person.getBirthDate());
			personId = statement.executeUpdate();
			
			if(personId == 0) {
				throw new AppException("405");
			}
			
		} catch(AppException e) {
			e.printStackTrace();
		} catch (SQLException sql) {
			System.out.println(sql.getMessage());
		} finally {
			try {
				if (connection != null) {
					connection.close(); 
				}
			} catch (SQLException sql) {
				System.out.println(sql.getMessage());
			}
		}
		return personId;
	}
	
	public static void updatePerson(Person person, long id) throws SQLException {
        try {Pattern p = Pattern.compile("^(.+)@(.+)$");
	        Matcher m = p.matcher(person.getEmail()); 
	        
	        if(!m.find()) {
	    	    throw new AppException("406"); 
	        }
	        
			connection = Connections.getConnection();
			PreparedStatement statement = connection.prepareStatement(QueryStatement.updateAddressQuery);
			statement.setString(1, person.getName());
			statement.setString(2, person.getEmail());
			statement.setLong(3, person.getAddressId());
			statement.setString(4, person.getBirthDate());
			
			int count = statement.executeUpdate();
			
			if(count == 0) {
				throw new AppException("405");
			}
		} catch(AppException e) {
			e.printStackTrace();
		} catch (SQLException sql) {
			System.out.println(sql.getMessage());
		} finally {
			try {
				if (connection != null) {
					connection.close(); 
				}
			} catch (SQLException sql) {
				System.out.println(sql.getMessage());
			}
		}
	}
	
	public static void readAllPerson() throws SQLException {
		try {
			connection = Connections.getConnection();
			PreparedStatement statement = connection.prepareStatement(QueryStatement.readAllPersonQuery);
			ResultSet result = statement.executeQuery();
			
			while (result.next()) {
				String name = result.getString("name");
				String email = result.getString("email");
				Date birth_date = result.getDate("birth_date");
				String street = result.getString("street");
				String city = result.getString("city");
				String postal_code = result.getString("postal_code");
				String created_date = result.getString("created_date");
				System.out.println(" Name:" + name + " Email:" + email + " Street:" + street + " City:" + city
						+ " Postal code:" + postal_code + " Birth Day:" + birth_date + " Created Date:" + created_date);
			}
			
		} catch (SQLException sql) {
			System.out.println(sql.getMessage());
		} finally {
			try {
				if (connection != null) {
					connection.close(); 
				}
			} catch (SQLException sql) {
				System.out.println(sql.getMessage());
			}
		}
	}
	
	public static void readPerson(long id) throws SQLException {
		try {
			connection = Connections.getConnection();
			PreparedStatement statement = connection.prepareStatement(QueryStatement.readPerson);
			statement.setLong(1, id);
			ResultSet result = statement.executeQuery();
			
			if (result.next()) {
				String name = result.getString("name");
				String email = result.getString("email");
				Date birth_date = result.getDate("birth_date");
				String street = result.getString("street");
				String city = result.getString("city");
				String postal_code = result.getString("postal_code");
				System.out.println(" Name:" + name + " Email:" + email + " Street:" + street + " City:" + city
						+ " Postal code:" + postal_code + " Birth Day:" + birth_date);
			} 
			
		} catch (SQLException sql) {
			System.out.println(sql.getMessage());
		} finally {
			try {
				if (connection != null) {
					connection.close(); 
				}
			} catch (SQLException sql) {
				System.out.println(sql.getMessage());
			}
		}
	}
	
	public static Boolean deletePerson(long id) throws SQLException, AppException {
		boolean delete = false;
		try {
			Connection connection = Connections.getConnection();
			PreparedStatement statement = connection.prepareStatement(QueryStatement.deletePersonQuery);
			statement.setLong(1, id);

			if (statement.executeUpdate() != 1) {
				throw new AppException("301");
			} else {
				delete = true;
				System.out.println("Person deleted Successfully");
			}
		} catch (SQLException sql) {
			System.out.println(sql.getMessage());
		} finally {
			try {
				if (connection != null) {
					connection.close(); 
				}
			} catch (SQLException sql) {
				System.out.println(sql.getMessage());
			}
		}
		return delete;
	}
	
}
*/

package com.kpr.training.jdbc.service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.kpr.training.jdbc.constant.Constant;
import com.kpr.training.jdbc.constant.QueryStatement;
import com.kpr.training.jdbc.exception.AppException;
import com.kpr.training.jdbc.exception.ErrorCode;
import com.kpr.training.jdbc.model.Address;
import com.kpr.training.jdbc.model.Person;
import com.kpr.training.jdbc.validation.PersonValidator;;

public class PersonService {

	Person person;
	Address address;
	long personId = 0;
	long addressId = 0;
	PreparedStatement ps;
	ResultSet rs;
	AddressService addressService = new AddressService();

	/*
	 * Methods for Validate the Condition ------------------------
	 */
	public void isEmailUnique(Person person) throws Exception {

		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.GET_PERSON_ID_QUERY)) {
			ps.setString(1, person.getEmail());
			ps.setLong(2, person.getId());

			if (ps.executeQuery().next()) {
				throw new AppException(ErrorCode.ERR14);
			}
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR20, e);
		}
	}

	public void isNameUnique(Person person) throws Exception {

		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.CHECK_UNIQUE_NAME_QUERY)) {

			ps.setString(1, person.getFirstName());
			ps.setString(2, person.getLastName());
			ps.setLong(3, person.getId());

			if (ps.executeQuery().next()) {
				throw new AppException(ErrorCode.ERR16);
			}

		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR15, e);
		}
	}

	public java.sql.Date convertDate(String stringDate) throws ParseException {

		try {

			java.util.Date date = new SimpleDateFormat("dd-MM-yyyy").parse(stringDate);
			java.sql.Date sqlDate1 = new java.sql.Date(date.getTime());
			return sqlDate1;
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR33, e);
		}
	}

	/*
	 * Cascade Operations: ------------------------
	 */
	public long create(Person person) {

		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.CREATE_PERSON_QUERY,
				Statement.RETURN_GENERATED_KEYS)) {

			PersonValidator.isNameNotNull(person);
			isEmailUnique(person);
			isNameUnique(person);
			setPs(ps, addressService.checkAddressOrCreate(person.getAddress()), person);
			
			if (ps.executeUpdate() == 1 && (rs = ps.getGeneratedKeys()).next()) {
				
				return rs.getLong(Constant.GENERATED_ID);
			}
			
			return 0;
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR21, e);
		}
	}

	public Person read(long id, boolean includeAddress) {

		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.READ_PERSON_QUERY)) {

			ps.setLong(1, id);
			rs = ps.executeQuery();

			if (!rs.next()) {
				return null;
			}
			person = getRs(rs);
			
			if (includeAddress) {
				person.setAddress(addressService.read(person.getAddressId()));
			}
			return person;
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR22, e);
		}
	}

	public List<Person> readAll() {

		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.READ_ALL_PERSONS_QUERY)) {

			List<Person> persons = new ArrayList<>();

			rs = ps.executeQuery();

			while (rs.next()) {
				person = getRs(rs);
				person.setAddress(addressService.read(person.getAddressId()));
				persons.add(person);
			}
			return persons;

		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR23, e);
		}
	}

	public void update(Person person) {

		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.UPDATE_PERSON_QUERY)) {

			PersonValidator.isNameNotNull(person);
			isNameUnique(person);
			isEmailUnique(person);
			setPs(ps, addressService.checkAddressOrCreate(person.getAddress()), person);
			ps.setLong(6, person.getId());

			System.out.println(ps.executeUpdate());
			if (ps.executeUpdate() != 1) {
				throw new AppException(ErrorCode.ERR34);
			}
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR09, e);
		}
	}

	public void delete(long id) {

		int count = 0;
		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.GET_ADDRESS_ID_COUNT)) {
			ps.setLong(1, id);

			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				count = rs.getInt(Constant.ID_COUNT);
				addressId = rs.getInt(Constant.ADDRESS_ID);
			}

			PreparedStatement preparedStatement = ConnectionService.get()
					.prepareStatement(QueryStatement.DELETE_PERSON_QUERY);
			preparedStatement.setLong(1, id);
			
			if (preparedStatement.executeUpdate() != 1) {
				throw new AppException(ErrorCode.ERR34);
			}
			if (count == 1) {
				addressService.delete(addressId);
			}

		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR24, e);
		}
	}

	/*
	 * CSV file -------------------------
	 */
	public ArrayList<Person> csvPersonInsertion(String csvFilePath) {

		ArrayList<Person> persons = new ArrayList<Person>();
		try (BufferedReader lineReader = new BufferedReader(new FileReader(csvFilePath))) {

			CSVParser records = CSVParser.parse(lineReader,
					CSVFormat.EXCEL.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());

			for (CSVRecord record : records) {

				Address address = new Address(record.get(5), record.get(6), Integer.parseInt(record.get(7)));
				Person person = new Person(record.get(0), record.get(1), record.get(2), address,
						convertDate(record.get(4)));

				create(person);
				persons.add(person);
			}
			return persons;

		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR31, e);
		}
	}

	public static Person getRs(ResultSet rs) throws SQLException {
		return new Person(rs.getLong(Constant.ID), rs.getString(Constant.FIRST_NAME), rs.getString(Constant.LAST_NAME),
				rs.getString(Constant.EMAIL), new Address(rs.getLong(Constant.ADDRESS_ID)),
				rs.getDate(Constant.BIRTH_DATE));
	}

	public static void setPs(PreparedStatement ps, long addressId, Person person) throws SQLException {
		ps.setString(1, person.getFirstName());
		ps.setString(2, person.getLastName());
		ps.setString(3, person.getEmail());
		ps.setLong(4, addressId);
		ps.setDate(5, person.getBirthDate());
	}

	public static void main(String[] args) {
		PersonService personService = new PersonService();
		try {
			Address address = new Address("KumaraNagar", "Tirupur", 641602);
			Person person = new Person("Murugan", "Vel", "murugan@gmail.com", address,
					personService.convertDate("21-09-2000"));
			long personId = personService.create(person);

			if (personId > 0) {
				ConnectionService.commitRollback(true);
			} else {
				ConnectionService.commitRollback(false);
			}

			ConnectionService.release();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
