import java.io.Serializable;

public class StudentDemo implements Serializable {

	private static final long serialVersionUID = 1L;
	public String name;
    public int studentId;
    public String address;
    public long phoneNumber;

    public void contactStudent() {
        System.out.println("Contact details: ");
        System.out.println("Name: " + name);
        System.out.println("Phone number: " + phoneNumber);
        System.out.println("Address: " + address);
    }

}
