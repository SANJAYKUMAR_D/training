/*
Stream Tokenizer:
=================
1) What is stream tokenizer?

Answer:
=======
    The StreamTokenizer class takes an input stream and parses it into "tokens",
    allowing the tokens to be read one at a time. 

    The stream tokenizer can recognize identifiers, numbers, quoted strings,
    and various comments

2) Why do we use stream tokenizer?  

Answer:
=======
    StreamTokenizer is a class parses input stream into tokens.
    It allows to read one token at a time. 

3) Mention any five methods of stream tokenizer?

Answer:
=======
i)commentChar(int character)
    Specified that the character argument starts at a single-line comment.

ii)eolIsSignificant(boolean flag)
    Method determines whether or not ends of line are treated as tokens.

iii)lineno()
    Method returns the current line number.

iv)lowerCaseMode(boolean flag)
    The method determines whether word token are automatically lower cased.

v)nextToken()
    This method parses the next token from the input stream of this tokenizer.

*/
