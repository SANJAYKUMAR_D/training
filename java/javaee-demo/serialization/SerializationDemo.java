/*
Serialization:
==============
1)What is serialization? 

Answer:
=======
    Serialization is the process of converting an object into a stream 
    of bytes to store the object or transmit it to memory, a database, or a file.

    This mechanism is used to persist the object.

2)What is need of serialization?

Answer:
=======
    Its main purpose is to save the state of an object in order to be able
    to recreate it when needed.

3)What will happen if one of the member in the class doesn�t implement  
serializable interface?

Answer:
``````
     When you try to serialize an object which does not implements Serializable 
     interface, in case if the object includes a reference of an non serializable
     object then NotSerializableException will be thrown.  

*/