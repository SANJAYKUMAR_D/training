/*
Serialization:
==============
4) Create a File, get the Following fields as input from the user 
    1.Name
    2.studentId
    3.Address
    4.Phone Number
now store the input in the File and serialize it, and again de serialize the File
and print the content.
      
Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Create a File, get the Following fields as input from the user 
     1.Name
     2.studentId
     3.Address
     4.Phone Number
    now store the input in the File and serialize it, and again de serialize the File
and print the content.
2.Entity :
    SerializeDeserializeDemo 
3.Function Declaration :
    public static void main(String[] args) throws ClassNotFoundException
4.Jobs to be done :
    1)Create a output stream file for specified path
    2)Create a ObjectOutputStream to store stream of object
    3)Write the student object of Student class to ObjectOutputStream, now the object
      serialized.
    4)Close the file.
    5)Create a input stream file for specified path
    6)Create a ObjectInputStream to import object byte stream
    7)Read student object of Student class from ObjectOutputStream, now the object
      de-serialized.
    8)Close the files.
5.Pseudocode :

public class SerializeDeserializeDemo  {

	public static void main(String[] args) throws ClassNotFoundException {
		
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);

        StudentDemo student = new StudentDemo();
        System.out.println("Student name:");
        student.name = scanner.nextLine();
        System.out.println("Student id:");
        student.studentId = scanner.nextInt();
        System.out.println("Address:");
        student.address = scanner.next();
        System.out.println("Phone number:");
        student.phoneNumber = scanner.nextLong();

        //Serializing the object of a Student class
        try {
            FileOutputStream outputFile = new FileOutputStream("C:\\Users\\SanjayKumar\\eclipse-workspace\\javaee-demo\\serialization\\student.ser");
            //Creating a object output stream file and passing serializing file
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputFile);
            //Writing the student object to object stream file
            objectOutputStream.writeObject(student);
            //After work done closing the files
            objectOutputStream.close();
            outputFile.close();
            System.out.println("Object serialized");
        }
        catch (IOException ioe) {
            System.out.println(ioe);
        }

        //Deserializing the object of a Student class
        try {
            FileInputStream inputFile = new FileInputStream("C:\\Users\\SanjayKumar\\eclipse-workspace\\javaee-demo\\serialization\\student.ser");
            ObjectInputStream objectInputStream = new ObjectInputStream(inputFile);

            // reading the student object from object stream file
            StudentDemo studObject = (StudentDemo) objectInputStream.readObject();
            System.out.println("\n" + "Object de-serialized" + "\n");
            // closing the files
            objectInputStream.close();
            inputFile.close();

            studObject.contactStudent();
        }
        catch (IOException ioe) {
            System.out.println(ioe);
        }
		
	}

}

*/

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

public class SerializeDeserializeDemo  {

	public static void main(String[] args) throws ClassNotFoundException {
		
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);

        StudentDemo student = new StudentDemo();
        System.out.println("Student name:");
        student.name = scanner.nextLine();
        System.out.println("Student id:");
        student.studentId = scanner.nextInt();
        System.out.println("Address:");
        student.address = scanner.next();
        System.out.println("Phone number:");
        student.phoneNumber = scanner.nextLong();

        //Serializing the object of a Student class
        try {
            FileOutputStream outputFile = new FileOutputStream("C:\\Users\\SanjayKumar\\eclipse-workspace\\javaee-demo\\serialization\\student.ser");
            //Creating a object output stream file and passing serializing file
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputFile);
            //Writing the student object to object stream file
            objectOutputStream.writeObject(student);
            //After work done closing the files
            objectOutputStream.close();
            outputFile.close();
            System.out.println("Object serialized");
        }
        catch (IOException ioe) {
            System.out.println(ioe);
        }

        //Deserializing the object of a Student class
        try {
            FileInputStream inputFile = new FileInputStream("C:\\Users\\SanjayKumar\\eclipse-workspace\\javaee-demo\\serialization\\student.ser");
            ObjectInputStream objectInputStream = new ObjectInputStream(inputFile);

            // reading the student object from object stream file
            StudentDemo studObject = (StudentDemo) objectInputStream.readObject();
            System.out.println("\n" + "Object de-serialized" + "\n");
            // closing the files
            objectInputStream.close();
            inputFile.close();

            studObject.contactStudent();
        }
        catch (IOException ioe) {
            System.out.println(ioe);
        }
		
	}

}
