/*
Java Map exercises:
================================
2) Write a Java program to test if a map contains a mapping for the specified key?

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Test if a map contains a mapping for the specified key
2.Entity :
    TestMap
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    i)Create a class MapToSpecifiedMap with main method.
    ii)Create a map named colleges and insert a map values by key,value.
    iii)Print the values stored in the map.
    iv)Print the value by checking the key present or not.
5.Pseudocode :

public class TestMap {

	public static void main(String[] args) {
		
		Map<Integer, String> colleges = new HashMap<>();
        colleges.put(2003, "KPR");
        colleges.put(2021, "Sri Krishna");
        colleges.put(2485, "PSG");
        colleges.put(3004, "CIT");
        colleges.put(2345, "ANGEL");
        colleges.put(3016, "KCT");
        System.out.println(colleges);
        System.out.println(colleges.containsKey(2003));
        System.out.println(colleges.containsKey(2001));

	}

}
*/

import java.util.HashMap;
import java.util.Map;

public class TestMap {

	public static void main(String[] args) {
		
		Map<Integer, String> colleges = new HashMap<>();
        colleges.put(2003, "KPR");
        colleges.put(2021, "Sri Krishna");
        colleges.put(2485, "PSG");
        colleges.put(3004, "CIT");
        colleges.put(2345, "ANGEL");
        colleges.put(3016, "KCT");
        System.out.println(colleges);
        System.out.println("Checking key '2003' is present in colleges: " + colleges.containsKey(2003));
        System.out.println("Checking key '2001' is present in colleges: " + colleges.containsKey(2001));

	}

}
