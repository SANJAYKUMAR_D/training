/*
Java Map exercises:
================================
4) Write a Java program to get the portion of a map whose keys range 
from a given key to another key?

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Get the portion of a map whose keys range from a given key to another key
2.Entity :
    MapRange
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    i)Create a class MapRange with main method.
    ii)Create a map named colleges and insert a map values as key,value.
    iii)create a map govtColleges and submap values inserted in that
    iv)Print the given submap govtColleges.
5.Pseudocode :

public class MapRange {

	public static void main(String[] args) {
		
		TreeMap<Integer, String> colleges = new TreeMap<>();
        colleges.put(2003, "KPR");
        colleges.put(2021, "Sri Krishna");
        colleges.put(2485, "PSG");
        colleges.put(3004, "CIT");
        colleges.put(2345, "ANGEL");
        colleges.put(3016, "KCT");
        Map<Integer, String> govtColleges = colleges.subMap(2021, 3016);
        System.out.println(govtColleges);

	}

}
*/

import java.util.Map;
import java.util.TreeMap;

public class MapRange {

	public static void main(String[] args) {
		
		TreeMap<Integer, String> colleges = new TreeMap<>();
        colleges.put(2003, "KPR");
        colleges.put(2021, "Sri Krishna");
        colleges.put(2485, "PSG");
        colleges.put(3004, "CIT");
        colleges.put(2345, "ANGEL");
        colleges.put(3016, "KCT");
        Map<Integer, String> govtColleges = colleges.subMap(2021, 3016);
        System.out.println(govtColleges);

	}

}
