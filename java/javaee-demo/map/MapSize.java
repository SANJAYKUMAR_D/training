/*
Java Map exercises:
================================
3) Count the size of mappings in a map?

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Count the size of mappings in a map
2.Entity :
    MapSize
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    i)Create a class MapSize with main method.
    ii)Create a map named colleges and insert a map values as key,value.
    iii)Print the values size of mappings.
5.Pseudocode :

public class MapSize {

	public static void main(String[] args) {
		
		Map<Integer, String> colleges = new HashMap<>();
        colleges.put(2003, "KPR");
        colleges.put(2021, "Sri Krishna");
        colleges.put(2485, "PSG");
        colleges.put(3004, "CIT");
        colleges.put(2345, "ANGEL");
        colleges.put(3016, "KCT");
        System.out.println(colleges.size());

	}

}

*/

import java.util.HashMap;
import java.util.Map;

public class MapSize {

	public static void main(String[] args) {
		
		Map<Integer, String> colleges = new HashMap<>();
        colleges.put(2003, "KPR");
        colleges.put(2021, "Sri Krishna");
        colleges.put(2485, "PSG");
        colleges.put(3004, "CIT");
        colleges.put(2345, "ANGEL");
        colleges.put(3016, "KCT");
        System.out.println("Map count: " + colleges.size());

	}

}
