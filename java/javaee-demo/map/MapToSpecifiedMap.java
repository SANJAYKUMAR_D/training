/*
Java Map exercises:
================================
1) Write a Java program to copy all of the mappings from the specified map to another map?

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Copy all of the mappings from the specified map to another map
2.Entity :
    MapToSpecifiedMap
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    i)Create a class MapToSpecifiedMap with main method.
    ii)Create a map named colleges and collegesListFinal.
    iii)insert a map values by key,value.
    iv)print the values stored in the map.
5.Pseudocode :

public class MapToSpecifiedMap {

	public static void main(String[] args) {
		
		Map<Integer, String> colleges = new HashMap<>();
        Map<Integer, String> collegesListFinal = new HashMap<>();
        colleges.put(2003, "KPR");
        colleges.put(2021, "Sri Krishna");
        colleges.put(2485, "PSG");
        colleges.put(3004, "CIT");
        colleges.put(2345, "ANGEL");
        colleges.put(3016, "KCT");
        collegesListFinal.putAll(colleges);
        System.out.println(collegesListFinal);
		
	}

}

*/

import java.util.HashMap;
import java.util.Map;

public class MapToSpecifiedMap {

	public static void main(String[] args) {
		
		Map<Integer, String> colleges = new HashMap<>();
        Map<Integer, String> collegesListFinal = new HashMap<>();
        colleges.put(2003, "KPR");
        colleges.put(2021, "Sri Krishna");
        colleges.put(2485, "PSG");
        colleges.put(3004, "CIT");
        colleges.put(2345, "ANGEL");
        colleges.put(3016, "KCT");
        collegesListFinal.putAll(colleges);
        System.out.println(collegesListFinal);
		
	}

}
