/*
Generic-Methods and Class:
==========================
3. Write a generic method to exchange the positions of two different elements in an array.

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Generic method to exchange the positions of two different elements in an array.
2.Entity :
    SwapDemo
3.Function Declaration :
    public static void main(String[] args)
    public static <T> void swap(T[] list, int number1, int number2) 
4.Jobs to be done :
    1)Create a class with main method.
    2)Create a generic method to swap two numbers in a array.
        2.1)use temporary variable to swap two numbers.
        2.2)the swapped array get printed.
    3)Create a array and add elements to it.
    4)By invoking method to exchange the positions of two different elements in an array 
5.Pseudocode :

public class SwapDemo {

	public static <T> void swap(T[] list, int number1, int number2) {
        T temp = list[number1];
        list[number1] = list[number2];
        list[number2] = temp;
        System.out.println("The swapped element is " + Arrays.toString(list) );
    }

	public static void main(String[] args) {
		
		//Declare and adding elements in lists
        Integer[] list = {111, 222, 333, 444, 555, 666};
        swap(list, 2, 4);

	}

}

*/

import java.util.Arrays;

public class SwapDemo {

	public static <T> void swap(T[] list, int number1, int number2) {
        T temp = list[number1];
        list[number1] = list[number2];
        list[number2] = temp;
        System.out.println("The swapped element is " + Arrays.toString(list) );
    }

	public static void main(String[] args) {
		
		//Declare and adding elements in lists
        Integer[] list = {111, 222, 333, 444, 555, 666};
        swap(list, 2, 4);

	}

}
