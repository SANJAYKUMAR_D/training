/*
Generic-Methods and Class:
===========================
2. Will the following class compile? If not, why?
public final class Algorithm {
    public static <T> T max(T x, T y) {
        return x > y ? x : y;
    }
}

Answer :
========
    This code make compile error, because the greater than (>) operator 
applies only for primitive numeric types.
 
*/