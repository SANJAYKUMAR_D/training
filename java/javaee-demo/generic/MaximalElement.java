/*
Generic-Methods and Class:
==========================
4. Write a generic method to find the maximal element in the range [begin, end) of a list.

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Generic method to find the maximal element in the range [begin, end) of a list.
2.Entity :
    MaximalElement
3.Function Declaration :
    public static void main(String[] args)
    public static <T> T largest(List<T> list, int startIndex, int endIndex)
4.Jobs to be done :
    1)Create a class with main method.
    2)Create a list and add elements to it.
    3)By invoking largest menthod 
        3.1)the maximal element in the range get printed.
5.Pseudocode :

public class MaximalElement {

	public static <T> T largest(List<T> list, int startIndex, int endIndex) {
        T max = list.get(startIndex);
        for (int start = startIndex; start <= endIndex; start++) {
            if ((Integer)max < (Integer)list.get(start)) {
                max = list.get(start);
            }
        }
        return max;
    }
	
	public static void main(String[] args) {
		
		List<Integer> list = Arrays.asList(1, 22, 3, 4, 5, 6, 7, 8);
        System.out.println(largest(list, 3, 8));

	}

}

*/

import java.util.Arrays;
import java.util.List;

public class MaximalElement {

	public static <T> T largest(List<T> list, int startIndex, int endIndex) {
        T max = list.get(startIndex);
        for (int start = startIndex; start <= endIndex; start++) {
            if ((Integer)max < (Integer)list.get(start)) {
                max = list.get(start);
            }
        }
        return max;
    }
	
	public static void main(String[] args) {
		
		List<Integer> list = Arrays.asList(1, 22, 3, 4, 5, 6, 7, 8);
        System.out.println(largest(list, 3, 7));

	}

}
