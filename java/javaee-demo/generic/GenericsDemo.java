/*
Generic-Class literals:
=======================
2. Write a program to demonstrate generics - for loop, for list, set and map.

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    program to demonstrate generics - for loop, for list, set and map.
2.Entity :
    GenericsDemo
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Create a class with main method.
    2)Create a generic list and add elements to it.
    3)Create a generic set and add elements to it.
    4)Create a generic map and add elements to it.
    5)Use generic in for loop to print the elements in the map. 
5.Pseudocode :

public class GenericsDemo {

	public static void main(String[] args) {
		
		List<String> iplTeams = Arrays.asList("CSK", "RCB", "KKR", "MI", "SRH");
        Set<Integer> uniqueNumbers = new HashSet<>();
        ADD elements to the set
        Map<Integer, String> players = new HashMap<>();
        ADD elements to the map;
        for (Map.Entry<?, ?> player : players.entrySet()) {
            System.out.println(player.getKey() +" - " +
            		players.get(player.getKey()));
        }


	}

}

*/

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class GenericsDemo {

	public static void main(String[] args) {
		
		@SuppressWarnings("unused")
		List<String> iplTeams = Arrays.asList("CSK", "RCB", "KKR", "MI", "SRH");

        Set<Integer> jessyNumbers = new HashSet<>();
        jessyNumbers.add(7);
        jessyNumbers.add(23);
        jessyNumbers.add(81);

        Map<Integer, String> players = new HashMap<>();
        players.put(7, "Dhoni");
        players.put(1, "Rahul");
        players.put(3, "Raina");
        players.put(8, "Jadeja");

        for (Map.Entry<?, ?> player : players.entrySet()) {
            System.out.println(player.getKey() +" - " +
            		players.get(player.getKey()));
        }


	}

}
