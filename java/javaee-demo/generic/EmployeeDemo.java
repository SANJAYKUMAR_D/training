/*
Generic-Implementing Iterable interface:
========================================
1. Write a program to print employees name list by implementing iterable interface.

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Program to print employees name list by implementing iterable interface.
2.Entity :
    EmployeeDemo
3.Function Declaration :
    public static void main(String[] args)
    public MyIterable(T[] t)
    public Iterator<T> iterator()
4.Jobs to be done :
    1)Create a interface that implements Iterable interface.
    2)Create a class with main method.
    3)Create a array and add elements to it.
    4)By creating object to the interface with array.
    5)Print the elements stored in the array.
5.Pseudocode :

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

class MyIterable<T> implements Iterable<T> {
	public List<T> list;
	public MyIterable(T[] t) {
		list = Arrays.asList(t);
	}
	public Iterator<T> iterator() {
		return list.iterator();
	}
}

public class EmployeeDemo {

	public static void main(String[] args) {
		
		String[] employeeName = {"Dinesh", "Imran", "Raina", "Santner", "Bravo"};
		MyIterable<String> names = new MyIterable<>(employeeName);
		for (String string : names) {
			System.out.println(string);
		}

	}

}

*/


import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

class MyIterable<T> implements Iterable<T> {
	public List<T> list;
	public MyIterable(T[] t) {
		list = Arrays.asList(t);
	}
	public Iterator<T> iterator() {
		return list.iterator();
	}
}

public class EmployeeDemo {

	public static void main(String[] args) {
		
		String[] employeeName = {"Dinesh", "Imran", "Raina", "Santner", "Bravo"};
		MyIterable<String> names = new MyIterable<>(employeeName);
		for (String string : names) {
			System.out.println(string);
		}

	}

}
