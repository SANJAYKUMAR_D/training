/*
Generic-Implementing Iterable interface:
========================================
2. Why except iterator() method in iterable interface, are not neccessary to define 
in the implemented class?

Answer:
=======
    Except iterator() method in iterable interface, are not neccessary to define 
in the implemented class.
    Because, the other two methods have default implementations.

*/