/*
Generic-Methods and Class:
========================
1. Write a generic method to count the number of elements in a collection that have a specific
   property (for example, odd integers, prime numbers, palindromes).
   
Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Generic method to count the number of elements in a collection that have a specific
property (for example, odd integers, prime numbers, palindromes).
2.Entity :
    CountSpecificProperty
3.Function Declaration :
    public static void main(String[] args)
    public static int countDemo(ArrayList<Integer> numbers)
4.Jobs to be done :
    i)Create a class with main method.
    ii)Create a generic method to find number of odd numbers in a list.
    iii)Create a list and add elements to it.
    iv)By invoking method, count an be printed.
    
5.Pseudocode :

public class CountSpecificProperty {

	public static int countDemo(ArrayList<Integer> numbers) {
		int count = 0;
		for(int number : numbers) {
			if(number % 2 != 0) {
				count++;
			}
		}
		return count;
	}
	
	public static void main(String[] args) {
		
		ArrayList<Integer> numbers = new ArrayList<>();
		ADD numbers to the list
		//Print the number of odd numbers in a list.
		System.out.println("Number of odd integers : " + countDemo(numbers));

	}

}
*/

import java.util.ArrayList;

public class CountSpecificProperty {

	public static int countDemo(ArrayList<Integer> numbers) {
		int count = 0;
		for(int number : numbers) {
			if(number % 2 != 0) {
				count++;
			}
		}
		return count;
	}
	
	public static void main(String[] args) {
		
		ArrayList<Integer> numbers = new ArrayList<>();
		numbers.add(50);
		numbers.add(77);
		numbers.add(23);
		numbers.add(82);
		numbers.add(71);
		//Print the number of odd numbers in a list.
		System.out.println("Number of odd integers : " + countDemo(numbers));

	}

}
