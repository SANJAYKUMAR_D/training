/*
Generic-Methods and Class:
==========================
5. What will be the output of the following program?

*/

public class UseGenerics {
	
    public static void main(String args[]) {  
    	
        MyGen<String> m = new MyGen<String>();  
        m.set("merit");
        System.out.println(m.get()); 
        
    }
    
}

class MyGen<T> {
	
    T var;
    void  set(T var) {
        this.var = var;
    }
    T get() {
        return var;
    }
    
}

/*
Output :
======== 
merit
 
*/
