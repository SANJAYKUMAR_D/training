/*
Generic-Class literals:
========================
1. Write a program to demonstrate generics - class objects as type literals.

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Program to demonstrate generics - class objects as type literals.
2.Entity :
    RectangleDemo
    Shape 
3.Function Declaration :
    public static void main(String[] args)
    public static <T> boolean checkInterface(Class<?> theClass)
4.Jobs to be done :
    i)Create a class RectangleDemo that implements Shape interface
    ii)Create literal class and invoke RectangleDemo class checkInterface method. 
    iii)Print type literal class, class name and its type class.
    Class objects can be used as type specifications 
    
5.Pseudocode :

interface Shape {
	public void draw();
}

public class RectangleDemo  implements Shape {

	public static <T> boolean checkInterface(Class<?> theClass) {
        return theClass.isInterface();
    }
	
	public static void main(String[] args) {

        Class<Integer> intClass = int.class;            
        boolean boolean1 = checkInterface(intClass);
        System.out.println(boolean1);                   
        System.out.println(intClass.getClass());        
        System.out.println(intClass.getName());         
 
        boolean boolean2 = checkInterface(RectangleDemo.class);
        System.out.println(boolean2);                   
        System.out.println(RectangleDemo.class.getClass());       
        System.out.println(RectangleDemo.class.getName());        

        boolean boolean3 = checkInterface(Shape.class);
        System.out.println(boolean3);                   
        System.out.println(Shape.class.getClass());    
        System.out.println(Shape.class.getName());     

        try {
            Class<?> errClass = Class.forName("RectangleDemo");
            System.out.println(errClass.getClass());
            System.out.println(errClass.getName());
        } catch (ClassNotFoundException classNotFoundException) {
            System.out.println(classNotFoundException.toString());
        }

	}

}

*/

interface Shape {
	public void draw();
}

public class RectangleDemo {

	//create a generified method
	public static <T> boolean checkInterface(Class<?> theClass) {
        return theClass.isInterface();
    }
	
	public static void main(String[] args) {

		//Class objects can be used as type specifications 
        Class<Integer> intClass = int.class;            
        boolean boolean1 = checkInterface(intClass);
        System.out.println(boolean1);                   
        System.out.println(intClass.getClass());        
        System.out.println(intClass.getName());         
 
        boolean boolean2 = checkInterface(RectangleDemo.class);
        System.out.println(boolean2);                   
        System.out.println(RectangleDemo.class.getClass());       
        System.out.println(RectangleDemo.class.getName());        

        boolean boolean3 = checkInterface(Shape.class);
        System.out.println(boolean3);                   
        System.out.println(Shape.class.getClass());    
        System.out.println(Shape.class.getName());     

        try {
            Class<?> errClass = Class.forName("RectangleDemo");
            System.out.println(errClass.getClass());
            System.out.println(errClass.getName());
        } catch (ClassNotFoundException classNotFoundException) {
            System.out.println(classNotFoundException.toString());
        }

	}

}

/*
Output :
========
false
class java.lang.Class
int
false
class java.lang.Class
RectangleDemo
true
class java.lang.Class
Shape
class java.lang.Class
RectangleDemo

*/