/*
EXCEPTION HANDLING exercises:
=============================
1)Demonstrate the catching multiple exception with example. 

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Catching multiple exception with example.
2.Entity :
    MultipleCatchDemo 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    i)Create a class MultipleCatchDemo.
    ii)Initializing the array with elements.
    iii)Iterate the element by using for loop.
    iv)By dividing first and second element in the array.If exception occurred it may
handled in catch blocks.
    v)The exception message get printed.
5.Pseudocode :

public class MultipleCatchDemo {
    
	public static void main(String[] args) {
		
        int numbers[] = {51, 82, 34, 7, 0, 19, 22, 87, 0, 8};
        
        for (int i = 0; i < numbers.length; i++) {
            try {
                System.out.println((numbers[i] / numbers[i + 1]));
            }
            catch (ArithmeticException arithmeticException) {//multiple catch block
                System.out.println("Cannot divide by zero.");
            }
            catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException) {
                System.out.println("Array went out of its bound.");
            }
         
        }
        
    }
	
}
*/

public class MultipleCatchDemo {
    
	public static void main(String[] args) {
		
        int numbers[] = {51, 82, 34, 7, 0, 19, 22, 0, 8};
        
        for (int i = 0; i < numbers.length; i++) {
            try {
                System.out.println((numbers[i] / numbers[i + 1]));
            }
            catch (ArithmeticException arithmeticException) {
                System.out.println("Cannot divide by zero.");
            }
            catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException) {
                System.out.println("Array went out of its bound.");
            }
         
        }
        
    }
	
}
