/*
EXCEPTION HANDLING exercises:
=============================
5)i)Mutiple catch block -  
       a]Explain with Example
       b]It is possible to have more than one try block? - Reason.
 ii)Difference between catching multiple exceptions and Mutiple catch blocks.

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Mutiple catch block - with Example
    Is possible to have more than one try block? - Reason.
    Difference between catching multiple exceptions and Mutiple catch blocks.
2.Entity :
    MultipleException 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    i)Create a class MultipleException.
    ii)Initialize the number variable with value.
    iii)Inside try block, print the number devided by zero.
    iv)By using multiple catch block different exception is handled. 
5.Pseudocode :

public class MultipleException {

	public static void main(String[] args) {
		
		int number = 500;
        try {
            System.out.println(number / 0);
        }
        catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBound) {//multiple catch block
            System.out.println("Accessing the array out of its boundry.");
        }
        catch (ArithmeticException arithmeticException) {
            System.out.println("Cannot divide by zero.");
        }
        catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
        
	}

}

*/

public class MultipleException {

	public static void main(String[] args) {
		
		int numbers[] = {1,2,3,4,5};
        try {
            int number = numbers[4];
            System.out.println(number / 0);
        }
        catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBound) {
            System.out.println("Accessing the array out of its boundry.");
        }
        catch (ArithmeticException arithmeticException) {
            System.out.println("Cannot divide by zero.");
        }
        catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
        
	}

}

/*
(i)b]It is possible to have more than one try block? - Reason.

No, we cannot use more than one try block.
but we can have nested try catch block or separate try catch blocks.

(ii)Difference between catching multiple exceptions and Multiple catch blocks.

catching multiple exceptions :

If a catch block handles multiple exceptions, you can separate them using a pipe (|)
e.g: catch (NullPointerException | ArithmeticException e)

Multiple catch blocks :

Making more catch blocks for handling the exceptions differently.
e.g:
catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException) {
    //Accessing the array out of its boundry.
}
catch (NullPointerException nullPointerException) {
    //NullPointerException.
}

*/



