/*
EXCEPTION HANDLING exercises:
=============================
9)Difference between throws and throw , Explain with simple example.

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Difference between throws and throw , Explain with simple example.
2.Entity :
    ThrowsAndThrow 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    i)Create an class ThrowsAndThrow 
    ii)Check by Invoking checkAge method with passing age value.
         2.1)If the age is less than 18 throws ArithmeticException error.
         2.2)Else print Eligible statement.

5.Pseudocode :

public class ThrowsAndThrow {

	void checkAge(int age){  
		if(age<18)  
		   throw new ArithmeticException("Not Eligible to vote");  
		else  
		   System.out.println("Eligible to vote");  
	   }  
	
    public static void main(String[] args) {  
    	
    	ThrowsAndThrow candidate = new ThrowsAndThrow();
		candidate.checkAge(12);
		
	   }
    
}

*/

public class ThrowsAndThrow {

	void checkAge(int age){  
		if(age<18)  
		   throw new ArithmeticException("Not Eligible to vote");  
		else  
		   System.out.println("Eligible to vote");  
	   }  
	
    public static void main(String[] args) {  
    	
    	ThrowsAndThrow candidate = new ThrowsAndThrow();
    	candidate.checkAge(12);
		
	   }
    
}

/*
Answer :

throw :
    1)throw keyword is used to explicitly throw an exception.
    2)Checked exception cannot be propagated using throw only.
    3)Throw is followed by an instance.
    4)Throw is used within the method.
    5)You cannot throw multiple exceptions.
    
throws :
    1)throws keyword is used to declare an exception.
    2)Checked exception can be propagated with throws.
    3)Throws is followed by class.
    4)Throws is used with the method signature.
    5)You can declare multiple exceptions e.g.
public void method()throws IOException,SQLException.
  
*/
