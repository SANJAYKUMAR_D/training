/*
EXCEPTION HANDLING exercises:
=============================
6)Handle and give the reason for the exception in the following code: 
        PROGRAM:
             public class Exception {  
             public static void main(String[] args)
             {
  	       int arr[] ={1,2,3,4,5};
	      System.out.println(arr[7]);
            }
          }
//Display the output.

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Handle and give the reason for the exception in the following code
2.Entity :
    ExceptionDemo 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    i)Create a Array with some elements.
    ii)Print the specified index statement in try block for checking exceptions.
    iii)If the specified index does not exist,throw the exception.
5.Pseudocode :

public class ExceptionDemo {

	public static void main(String[] args) {
		
        int arr[] ={1,2,3,4,5};
        try {
            System.out.println(arr[7]);
        }
        catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException) {
            System.out.println("The specified index does not exist in a array");
        }
        
    }
	
}

*/

public class ExceptionDemo {

	public static void main(String[] args) {
		
        int arr[] ={1,2,3,4,5};
        try {
            System.out.println(arr[7]);
        }
        catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException) {
            System.out.println("The specified index does not exist in a array");
        }
        
    }
	
}

/*
 Output :
 
 The specified index does not exist in a array
  
 */
