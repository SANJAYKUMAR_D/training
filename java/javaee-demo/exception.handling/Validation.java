/*
EXCEPTION HANDLING exercises:
=============================
7)a]Why validation is important?Explain with example.
  b]Is the following code can be done as it is?If not correct the code and give the reason for the change of code.
          PROGRAM:
              check if user already exists
              validate user
              validate address

              insert user
              insert address

Answer:
=======
a]   Validation is important because, to process, to store, to avoid collapse, to avoid mismatch and etc.
Validation makes the user to give inputs to the needs.
e.g: "emailId" to be unique to make the user unique on database.
Validation helps the user to make unique emailId. 


b]   There is no need to validate everything.
Insert user if the required data not matched or not exists.

*/
