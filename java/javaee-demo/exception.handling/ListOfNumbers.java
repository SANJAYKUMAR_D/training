/*
EXCEPTION HANDLING exercises:
=============================
2)Write a program ListOfNumbers (using try and catch block).

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Write a program ListOfNumbers (using try and catch block).
2.Entity :
    ListOfNumbers 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    i)Create a list with some elements.
    ii)Iterating the elements, if the element casted to int then there 
won't be any error.
    iii)It throws exception if it cannot casted into int.
5.Pseudocode :

public class ListOfNumbers {

    public static void main(String[] args) {
        List numbers = new ArrayList();
        ADD numbers into the list;
        Iterator iterator = numbers.iterator();
        while (iterator.hasNext()) {
            try {
                System.out.println((int) iterator.next());
            }
            catch (Exception e) {
                System.out.println("Value not a Integer");
            }
        }
    }

}

*/

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ListOfNumbers {

    public static void main(String[] args) {
        List numbers = new ArrayList();
        numbers.add(1);
        numbers.add(2);
        numbers.add('3');
        numbers.add(4);
        numbers.add(5);
        Iterator iterator = numbers.iterator();
        while (iterator.hasNext()) {
            try {
                System.out.println((int) iterator.next());
            }
            catch (Exception e) {
                System.out.println("Value not a Integer");
            }
        }
    }

}
