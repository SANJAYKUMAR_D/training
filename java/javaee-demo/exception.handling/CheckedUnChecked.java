/*
EXCEPTION HANDLING exercises:
=============================
3)Compare the checked and unchecked exception.

Answer:
=======

Checked exceptions :
   
    i)These exceptions are also called compile time exceptions and because, these exceptions will be known during compile time.
    ii)These exceptions are checked at compile time only. 
	iii)java.lang.Exception is checked exception.
Example :
	SQLException, IOException, ClassNotFoundException.

Unchecked exceptions :

    i)Unchecked exceptions are those exceptions which are not at all known to compiler.
	ii)These exceptions occur only at run time. 
	iii)These exceptions are also called as run time exceptions.
	iv)All sub classes of java.lang.RunTimeException and java.lang.Error are unchecked exceptions.
Example :
    NullPointerException, ArrayIndexOutOfBoundsException, ArithmeticException, NumberFormatException.
    
*/
