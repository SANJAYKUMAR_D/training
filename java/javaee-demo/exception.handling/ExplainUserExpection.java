/*
EXCEPTION HANDLING exercises:
=============================
8)Explain the below program.
         try{
          dao.readPerson();
        } catch (SQLException sqlException) {
        throw new MyException("wrong", sqlException);
      }

Answer:
=======
If try block thrown expection, its catched by catch block and it throws to
the constructor of the user defined Exception "MyException".

*/