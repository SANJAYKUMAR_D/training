/*
Networking:
===========
- Create a program using HttpUrlconnection in networking.

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Create a program using HttpUrlconnection in networking.
2.Entity :
    HttpUrlconnection 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Open the url connection using HttpURLConnection.
    2)Get the length of the content using getContentLength method.
    3)Display the connection length.
5.Pseudocode :

public class HttpUrlconnection {

	public static void main(String[] args) {
		
		URL url = null;
        try {
            url = new URL("https://www.gmail.com/inbox");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            System.out.println("Content length: " + connection.getContentLength());
        } catch (IOException e) {
            e.printStackTrace();
        }

	}

}

*/

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpUrlconnection {

	public static void main(String[] args) {
		
		URL url = null;
        try {
            url = new URL("https://www.gmail.com/inbox");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            System.out.println("Content length: " + connection.getContentLength());
        } catch (IOException e) {
            e.printStackTrace();
        }

	}

}
