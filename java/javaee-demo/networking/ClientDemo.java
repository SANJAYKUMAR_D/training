/*
Networking:
===========
- Build a client side program using ServerSocket class for Networking.

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Build a client side program using ServerSocket class for Networking.
2.Entity :
    ClientDemo 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Create a Socket class with localhost.
    2)Pass argument of DataOutputStream class with getOutputStream method.
    3)Write to server using writeUTF method
    4)Close and flush the dataOutput.
5.Pseudocode :

public class ClientDemo {

	public static void main(String[] args) {
		
		try {
			Socket socket = new Socket("localhost", 4560);
			DataOutputStream dataOutput = new DataOutputStream(socket.getOutputStream());
			dataOutput.writeUTF("Client Server");
			dataOutput.flush();
			dataOutput.close();
			socket.close();
		} catch (Exception e) {
			System.out.println(e);
		}

	}

}
*/

import java.io.DataOutputStream;
import java.net.Socket;

public class ClientDemo {

	public static void main(String[] args) {
		
		try {
			Socket socket = new Socket("localhost", 4560);
			DataOutputStream dataOutput = new DataOutputStream(socket.getOutputStream());
			dataOutput.writeUTF("Client Server");
			dataOutput.flush();
			dataOutput.close();
			socket.close();
		} catch (Exception e) {
			System.out.println(e);
		}

	}

}
