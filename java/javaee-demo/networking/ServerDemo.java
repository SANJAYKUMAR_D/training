/*
Networking:
===========
- Build a server side program using Socket class for Networking.

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Build a server side program using Socket class for Networking.
2.Entity :
    ServerDemo 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Create a ServerSocket class with localhost.
    2)Accept serverSocket using accpet mathod.
    3)Pass argument of DataOutputStream class with getInputStream method.
    4)Read to client passed string using readUTF method.
    5)Finally close the dataOutput.
5.Pseudocode :

public class ServerDemo {
	
	public static void main(String[] args) {
		
		try {
			ServerSocket serverSocket = new ServerSocket(2345);
			System.out.println("Waiting for client...");
			Socket socket = serverSocket.accept();
			System.out.println("Client accepted");
			DataInputStream dataInput = new DataInputStream(socket.getInputStream());
			String string = (String) dataInput.readUTF();
			System.out.println(string);
			serverSocket.close();
		} catch (Exception e) {
			System.out.println(e);
		}

	}

}

*/

import java.io.DataInputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerDemo {
	
	public static void main(String[] args) {
		
		try {
			ServerSocket serverSocket = new ServerSocket(2345);
			System.out.println("Waiting for client...");
			Socket socket = serverSocket.accept();
			System.out.println("Client accepted");
			DataInputStream dataInput = new DataInputStream(socket.getInputStream());
			String string = (String) dataInput.readUTF();
			System.out.println(string);
			serverSocket.close();
		} catch (Exception e) {
			System.out.println(e);
		}

	}

}
