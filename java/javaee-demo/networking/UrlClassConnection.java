/*
Networking:
===========
- Create a program  for url class and url connection class in networking.

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Create a program  for url class and url connection class in networking.
2.Entity :
    UrlClassConnection 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Initialize the path by creating object to the class Url.
    2)Display the protocol name using getProtocol() method.
    3)Display the host name using getHost() method.
    4)Display the port number using getPort() method
    5)Display the file name using getFile() method
5.Pseudocode :

public class UrlClassConnection {

	public static void main(String[] args) {
		
		try {
			URL url = new URL("https://www.coursera.ac.in/");

			System.out.println("Protocol: " + url.getProtocol());
			System.out.println("Host: " + url.getHost());
			System.out.println("Port: " + url.getPort());
			System.out.println("File Name: " + url.getFile());

		} catch (Exception e) {
			System.out.println(e);
		}

	}

}

*/

import java.net.URL;

public class UrlClassConnection {

	public static void main(String[] args) {
		
		try {
			URL url = new URL("https://www.coursera.ac.in/");

			System.out.println("Protocol: " + url.getProtocol());
			System.out.println("Host: " + url.getHost());
			System.out.println("Port: " + url.getPort());
			System.out.println("File Name: " + url.getFile());

		} catch (Exception e) {
			System.out.println(e);
		}

	}

}

