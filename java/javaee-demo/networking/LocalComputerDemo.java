/*
Networking:
===========
- Find out the IP Address and host name of your local computer.

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Find out the IP Address and host name of your local computer.
2.Entity :
    LocalComputerDemo 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Get the local host address and host name using InetAddress.
    2)Try to get the address of website or web browser using getByName
      method of InetAddress.
    3)Otherwise throw an exception(Address not found).
5.Pseudocode :

public class LocalComputerDemo {

	public static void main(String[] args) {
		
		try {

            // getting address of local host
            InetAddress address = InetAddress.getLocalHost();
            System.out.println("IP address: " + address.getHostAddress());
            System.out.println("Host name: " + address.getHostName());

            // getting address of a web browser
            InetAddress gmailAddress = InetAddress.getByName("gmail.co.in");
            System.out.println("Gmail address: " + gmailAddress);
        } catch (UnknownHostException e) {
            System.out.println("Address not found: ");
        }

	}

}

*/

import java.net.InetAddress;
import java.net.UnknownHostException;

public class LocalComputerDemo {

	public static void main(String[] args) {
		
		try {

            // getting address of local host
            InetAddress address = InetAddress.getLocalHost();
            System.out.println("IP address: " + address.getHostAddress());
            System.out.println("Host name: " + address.getHostName());

            // getting address of a web browser
            InetAddress gmailAddress = InetAddress.getByName("gmail.co.in");
            System.out.println("Gmail address: " + gmailAddress);
        } catch (UnknownHostException e) {
            System.out.println("Address not found: ");
        }

	}

}
