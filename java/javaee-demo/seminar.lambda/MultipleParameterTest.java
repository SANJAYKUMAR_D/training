//Multiple Parameter

interface Addable {
    int add(int number1,int number2);
}

public class MultipleParameterTest{
    
    public static void main(String[] args) {
        
        // Multiple parameters in lambda expression
        Addable addition1 = (number1,number2) -> (number1 + number2);
        System.out.println(addition1.add(10,20));
        
        // Multiple parameters with data type in lambda expression  
        Addable addition2 = (int number1,int number2) -> (number1 + number2);
        System.out.println(addition2.add(100,200));

    }
    
}

// Parameter type

//(int number1,int number2) -> (number1 + number2);
//(Car car) -> System.out.println("The car is: " + car.getName());
