//Zero Parameter

interface Sayable {
    public String say();
}

public class ZeroParameterTest {
    
    public static void main(String[] args) {
        
        Sayable obj=() -> {
            return "Zero Parameter";
        };
        
        System.out.println(obj.say());
        
    }
    
}