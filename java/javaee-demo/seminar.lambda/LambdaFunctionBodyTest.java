interface Addable {
    void add(int number1,int number2);
}

public class LambdaFunctionBodyTest {
    
    public static void main(String[] args) {
        
        Addable addition1 = (number1,number2) -> {
            System.out.println(number1 + number2);
            System.out.println("get incremented :" + (number1 + number2 + 1));
        };
        
        addition1.add(10,20);
        
    }
    
}