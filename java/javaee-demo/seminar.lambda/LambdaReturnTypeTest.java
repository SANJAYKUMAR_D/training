interface Addable {
    int add(int number1,int number2);
}

public class LambdaReturnTypeTest {
    
    public static void main(String[] args) {
        
        // Lambda expression without return type.
        Addable addition1 = (number1,number2) -> (number1 > number2);
        System.out.println(addition1.add(10,20));
        
        // Lambda expression with return type.
        Addable addition2 = (int number1,int number2) -> {
            return (number1 + number2);
        };
        System.out.println(addition2.add(100,200));
        
    }
}