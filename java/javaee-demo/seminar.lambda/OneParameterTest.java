//One Parameter

import java.util.ArrayList;

public class OneParameterTest {

    public static void main(String[] args) {
  
        ArrayList<Integer> numbers = new ArrayList<Integer>();
        numbers.add(5);
        numbers.add(9);
        numbers.add(8);
        numbers.add(1);
        numbers.forEach((n) -> System.out.println(n));  // instead of using for loop (parallel processing)
        /*
        for(int number : numbers ) {
            System.out.println(number);
        }
        */
    }
  
}