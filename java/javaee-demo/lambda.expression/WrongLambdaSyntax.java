/*
Java Lambda Expression exercises:
================================
+  What is wrong with the following interface? and fix it.
        (int x, y) -> x + y;


Answer:
=======
(int x, y) -> x + y;

This lambda expression throws error while compiling, because in lambda expression 
access specifier is not mandatory. but, if the access specifier is specified for particular 
variable then it will shows error.

correct code:
    (x, y) -> x + y; or
    (int x, int y) -> x + y;
*/