/*
Java Lambda Expression exercises:
================================
+ Code a functional program that can return the sum of elements of varArgs, passed into the 
method of functional interface.

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Return the sum of elements of varArgs, passed into the method of functional interface.
2.Entity :
    VarArgsInLambda
3.Function Declaration :
    public int add(int ... numbers);
    public static void main(String[] args)
4.Jobs to be done :
    i)Create a functional interface with abstract method.
    ii)Create a class VarArgsInLambda with main method.
    iii)By lambda expression,Referencing the functional block of code to the object of interface.
    iv)At last calling the method assigned to the object of interface,elements are passed into
the method of functional interface.
    v)Print the added value.
5.Pseudocode :

interface Addable {//functional interface
    public int add(int ... numbers);
}

public class VarArgsInLambda {
	
	public int sum;
    Addable addable = (int ... numbers) -> {//lambda expression
        for(int number : numbers) {
            sum += number;
        }
        return sum;
    };

	public static void main(String[] args) {
		
		VarArgsInLambda addObj = new VarArgsInLambda();
        System.out.println(addObj.addable.add(10,20,30,40,50));

	}

}

*/

interface Addable {
	
    public int add(int ... numbers);
    
}

public class VarArgsInLambda {
	
	public int sum;
    Addable addable = (int ... numbers) -> {
        for(int number : numbers) {
            sum += number;
        }
        return sum;
    };

	public static void main(String[] args) {
		
		VarArgsInLambda addObj = new VarArgsInLambda();
        System.out.println(addObj.addable.add(10,20,30,40,50));

	}

}
