/*
Java Lambda Expression exercises:
================================
+  which one of these is a valid lambda expression? and why?:
        (int x, int y) -> x+y; or (x, y) -> x + y;


Answer:
=======
(int x, int y) -> x + y; or
(x, y) -> x + y;

-here, both the statements are correct.
-In the lambda expression there is no need to specify access specifiers while 
passing arguments.

*/