/*
Java Lambda Expression exercises:
================================
+  write a Lambda expression program with a single method interface
To concatenate two strings

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Single method interface to concatenate two strings
2.Entity :
    StringConcatenate
3.Function Declaration :
    public String concat(String firstname, String lastname)
    public static void main(String[] args)
4.Jobs to be done :
    i)Create a functional interface with abstract method.
    ii)Create a class Concatenate with main method.
    iii)Referencing the functional block of code to the object of interface.
    iv)At last calling the method assigned to the object of interface.
    v)The concatenated string get printed.
5.Pseudocode :

interface Concatenate {//functional interface
    public String concat(String firstname, String lastname);
}

public class StringConcatenate {

	public static void main(String[] args) {
		
		Concatenate concater = (firstname, lastname) -> firstname + " " + lastname;//lambda expression
        String myName = concater.concat("Tony", "Stark");
        System.out.println(myName);
		
	}

}

*/

interface Concatenate {
	
    public String concat(String firstname, String lastname);
    
}

public class StringConcatenate {

	public static void main(String[] args) {
		
		Concatenate concater = (firstname, lastname) -> firstname + " " + lastname;
        String myName = concater.concat("Tony", "Stark");
        System.out.println(myName);
		
	}

}
