/*
Java Lambda Expression exercises:
================================
+  write a program to print difference of two numbers using lambda expression 
    and the single method interface

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Difference of two numbers using lambda expression with the single method interface
2.Entity :
    NumberDifference
3.Function Declaration :
    public int Difference(int number1, int number2)
    public static void main(String[] args)
4.Jobs to be done :
    i)Create a functional interface with abstract method.
    ii)Create a class NumberDifference with main method.
    iii)Initializing the value to the numbers.
    iv)Referencing the difference of two numbers to the object of interface.
    v)At last calling the method assigned to the object of interface.
    vi)Print the resultant value.
5.Pseudocode :

interface Subtraction {//functional interface
    public int Difference(int number1, int number2);
}

public class NumberDifference {

	public static void main(String[] args) {
		
		Subtraction subtract = (number1, number2) -> number1 - number2;//lambda expression
        int number1 = 25;//initializing variables
        int number2 = 30;
        int differenceValue = subtract.Difference(number1, number2);
        System.out.println(String.format(differenceValue));
		
	}

}

*/

interface Subtraction {
	
    public int Difference(int number1, int number2);
    
}

public class NumberDifference {

	public static void main(String[] args) {
		
		Subtraction subtract = (number1, number2) -> number1 - number2;
        int number1 = 25;
        int number2 = 30;
        int differenceValue = subtract.Difference(number1, number2);
        System.out.println(String.format("Difference of %d and %d is %d"
        		                         ,number1
        		                         ,number2
        		                         ,differenceValue));
		
	}

}
