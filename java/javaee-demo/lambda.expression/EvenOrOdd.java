/*
Java Lambda Expression exercises:
================================
+ Convert the following anonymous class into lambda expression.
interface CheckNumber {
    public boolean isEven(int value);
}

public class EvenOrOdd {
    public static void main(String[] args) {
        CheckNumber number = new CheckNumber() {
            public boolean isEven(int value) {
                if (value % 2 == 0) {
                    return true;
                } else return false;
            }
        };
        System.out.println(number.isEven(11));
    }
}

*/

interface CheckNumber {
	
    public boolean isEven(int number);
    
}

public class EvenOrOdd {

	public static void main(String[] args) {
		
		CheckNumber numberObj = number -> number % 2 == 0 ? true : false;
        System.out.println(numberObj.isEven(27));

	}

}
