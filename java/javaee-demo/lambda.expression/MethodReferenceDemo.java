/*
Java Lambda Expression exercises:
================================
+  What is Method Reference and its types.Write a program for each types 
with suitable comments.

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Method Reference types with example
2.Entity :
    StaticMethodReference 
    InstanceMethodReference 
    ConstructorReference 
    MethodReferenceDemo 
3.Function Declaration :
    public void greet(String yourName)
    public int compare(int number1, int number2)
    public int average()
    public void add(double ... numbers)
    public static void main(String[] args)
4.Jobs to be done :
    i)Create a functional interface with single abstract method for each Method reference.
    ii)Create a class with main method.
    iii)Create a object for the class in main method 
    iv)In static method reference, the static method referred in the class and
is initialized to the object of interface.
    v)In parameter method reference, the method is referred by class and no 
need of specifying parameters. It can refer the interface.
    vi)In instance method reference, the method is referred by the object of the class.
    vii)In constructor method reference, it refers the constructor of a class and it
can compared with interface.
5.Pseudocode :

interface Printer {
    public void greet(String yourName);
}

interface Comparable {
    public int compare(int number1, int number2);
}

interface Average {
    public int average();
}

interface AddableDemo {
    public void add(double ... numbers);
}

class StaticMethodReference {
    public static void show(String name) {
        System.out.println(String.format(name));
    }
}

class InstanceMethodReference {
    public int sum;
    public int number1;
    public int number2;

    public int calculateAverage() {
        sum = number1 + number2;
        int avrg = (sum / 2);
        return avrg;
    }
}

class ConstructorReference {
    ConstructorReference(double ... numbers) {
        int sum = 0;
        for (double number : numbers) {
            sum += number;
        }
        System.out.println(sum);
    }
}

public class MethodReferenceDemo {

    public static void main(String[] args) {

        Printer print = StaticMethodReference::show; //Static Method Reference
        print.greet("Johny Depp");

        Comparable item = Integer::compare; //Parameter Method Reference
        System.out.println("Comparing numbers 22 and 33: " + item.compare(22, 21));

        InstanceMethodReference objDemo = new InstanceMethodReference();
        objDemo.number1 = 15;
        objDemo.number2 = 35;
        Average average = objDemo::calculateAverage; //Instance Method Reference
        int result = average.average();
        System.out.println("Average of two numbers: " + result);

        AddableDemo adder = ConstructorReference::new; //Constructor Reference
        adder.add(10,20,30,40,50);

    }

}

Answer :
========
Method reference is further simplified form of lambda expression.It has four types,
        1.Static Method Reference
        2.Parameter Method Reference
        3.Instance Method Reference
        4.Constructor Reference
*/

interface Printer {
    public void greet(String yourName);
}

interface Comparable {
    public int compare(int number1, int number2);
}

interface Average {
    public int average();
}

interface AddableDemo {
    public void add(double ... numbers);
}

class StaticMethodReference {
    public static void show(String name) {
        System.out.println(String.format("Welcome %s!", name));
    }
}

class InstanceMethodReference {
    public int sum;
    public int number1;
    public int number2;

    public int calculateAverage() {
        sum = number1 + number2;
        int avrg = (sum / 2);
        return avrg;
    }
}

class ConstructorReference {
    ConstructorReference(double ... numbers) {
        int sum = 0;
        for (double number : numbers) {
            sum += number;
        }
        System.out.println("Sum of elements: " + sum);
    }
}

public class MethodReferenceDemo {

    public static void main(String[] args) {

        Printer print = StaticMethodReference::show; //Static Method Reference
        print.greet("Johny Depp");

        Comparable item = Integer::compare; //Parameter Method Reference
        System.out.println("Comparing numbers 22 and 33: " + item.compare(22, 21));

        InstanceMethodReference objDemo = new InstanceMethodReference();
        objDemo.number1 = 15;
        objDemo.number2 = 35;
        Average average = objDemo::calculateAverage; //Instance Method Reference
        int result = average.average();
        System.out.println("Average of two numbers: " + result);

        AddableDemo adder = ConstructorReference::new; //Constructor Reference
        adder.add(10,20,30,40,50);

    }

}
