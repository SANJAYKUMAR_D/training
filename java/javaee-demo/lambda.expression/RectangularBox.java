/*
Java Lambda Expression exercises:
================================
+  Write a program to print the volume of  a Rectangle using lambda expression

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    The volume of  a Rectangle using lambda expression
2.Entity :
    RectangularBox
3.Function Declaration :
    public double volume()
    RectangularBox(double length, double width, double height)
    public static void main(String[] args)
4.Jobs to be done :
    i)Create a functional interface with abstract method.
    ii)Create a class RectangularBox with main method.
    iii)Create a constructor inside a Class.
    iv)by using lambda expression,Referencing the functional block of code to the object of interface.
        functional block contains volume formula length*width*height.
    v)At last call the method assigned to the object of interface.
    vi)print the volume of the rectangle.
5.Pseudocode :

interface Rectangle {//functional interface
    public double volume();
}

public class RectangularBox {
	
    public double length;
    public double width;
    public double height;
    
    Rectangle boxVolume = () -> this.length * this.width * this.height;//lambda expression
    
    RectangularBox(double length, double width, double height) {//constructor
        this.length = length;
        this.width = width;
        this.height = height;
    }

	public static void main(String[] args) {
		
	    RectangularBox box = new RectangularBox(44.5, 12.2, 10);
        double volume = box.boxVolume.volume();
        System.out.println("Volume");
		
	}

}

*/

interface Rectangle {
	
    public double volume();
    
}

public class RectangularBox {
	
    public double length;
    public double width;
    public double height;
    
    Rectangle boxVolume = () -> this.length * this.width * this.height;
    
    RectangularBox(double length, double width, double height) {
        this.length = length;
        this.width = width;
        this.height = height;
    }

	public static void main(String[] args) {
		
	    RectangularBox box = new RectangularBox(44.5, 12.2, 10);
        double volume = box.boxVolume.volume();
        System.out.println("Volume of the box is " + volume +" cm�");
		
	}

}
