/*
Internationalization :
======================
2. To print the following pattern of the Date and Time using SimpleDateFormat.
        "yyyy-MM-dd HH:mm:ssZ"

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Print the following pattern of the Date and Time using SimpleDateFormat.
        "yyyy-MM-dd HH:mm:ssZ"
2.Entity :
    SimpleDateFormat 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Initialize the string with given pattern.
    2)Create a object for SimpleDateFormat.
    3)Format the date using formatter.
    4)Display the formatted Date.
5.Pseudocode :

public class SimpleDateFormatDemo {

	public static void main(String[] args) {
		
		String pattern = "yyyy-MM-dd HH:mm:ss Z";
        SimpleDateFormat formatter = new SimpleDateFormat(pattern);

        //formatting the date object
        String date = formatter.format(new Date());
        System.out.println(date);

	}

}

*/

import java.util.Date;
import java.text.SimpleDateFormat;

public class SimpleDateFormatDemo {

	public static void main(String[] args) {
		
		String pattern = "yyyy-MM-dd HH:mm:ss Z";
        SimpleDateFormat formatter = new SimpleDateFormat(pattern);

        //formatting the date object
        String date = formatter.format(new Date());
        System.out.println(date);

	}

}
