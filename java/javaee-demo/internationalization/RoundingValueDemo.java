/*
Internationalization :
======================
2)Write a code to rounding the value to maximum of 3 digits by setting maximum and 
minimum Fraction digits.

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    code to rounding the value to maximum of 3 digits by setting maximum and minimum 
Fraction digits.
2.Entity :
    RoundingValueDemo 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Create an object by invoking getInstance method of NumberFormat class.
	2)Set minimum and maximum fraction digits on numberFormat.
	3)Display by formatting the number using numberFormat.
5.Pseudocode :

public class RoundingValueDemo {

	public static void main(String[] args) {
		
		NumberFormat numberFormat = NumberFormat.getInstance();
        
        numberFormat.setMinimumFractionDigits(1);
        numberFormat.setMaximumFractionDigits(3);
        System.out.println(numberFormat.format(123.456789));
        System.out.println(numberFormat.format(100));

	}

}
*/

import java.text.NumberFormat;

public class RoundingValueDemo {

	public static void main(String[] args) {
		
		NumberFormat numberFormat = NumberFormat.getInstance();
        
        numberFormat.setMinimumFractionDigits(1);
        numberFormat.setMaximumFractionDigits(3);
        System.out.println(numberFormat.format(123.456789));
        System.out.println(numberFormat.format(100));

	}

}
