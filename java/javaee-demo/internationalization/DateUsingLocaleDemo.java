/*
Internationalization :
======================
1. To print the following Date Formats in Date using locale.
        DEFAULT, MEDIUM, LONG, SHORT, FULL. 

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Print the following Date Formats in Date using locale.
        DEFAULT, MEDIUM, LONG, SHORT, FULL.
2.Entity :
    DateUsingLocaleDemo 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Create an object of Date and print current date.
    2)Create an object of locale using language and region.
    3)Display the date Format using default, medium, long, short, full.
5.Pseudocode :
public class DateUsingLocaleDemo {

	public static void main(String[] args) {
		
		Date date = new Date();
        System.out.println("Current Date: " + date);

        Locale locale = new Locale("en", "CA");
        String defaultTime = DateFormat.getTimeInstance(DateFormat.DEFAULT, locale).format(date);
        System.out.println("DEFAULT: " + defaultTime);

        String mediumDate = DateFormat.getDateInstance(DateFormat.MEDIUM, locale).format(date);
        System.out.println("MEDIUM: " + mediumDate);

        String longDate = DateFormat.getDateInstance(DateFormat.LONG, locale).format(date);
        System.out.println("LONG: " + longDate);

        String shortDate = DateFormat.getDateInstance(DateFormat.SHORT, locale).format(date);
        System.out.println("SHORT: " + shortDate);

        String fullTime = DateFormat.getTimeInstance(DateFormat.FULL, locale).format(date);
        System.out.println("FULL: " + fullTime);

	}

}

*/

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUsingLocaleDemo {

	public static void main(String[] args) {
		
		Date date = new Date();
        System.out.println("Current Date: " + date);

        Locale locale = new Locale("en", "CA");
        String defaultTime = DateFormat.getTimeInstance(DateFormat.DEFAULT, locale).format(date);
        System.out.println("DEFAULT: " + defaultTime);

        String mediumDate = DateFormat.getDateInstance(DateFormat.MEDIUM, locale).format(date);
        System.out.println("MEDIUM: " + mediumDate);

        String longDate = DateFormat.getDateInstance(DateFormat.LONG, locale).format(date);
        System.out.println("LONG: " + longDate);

        String shortDate = DateFormat.getDateInstance(DateFormat.SHORT, locale).format(date);
        System.out.println("SHORT: " + shortDate);

        String fullTime = DateFormat.getTimeInstance(DateFormat.FULL, locale).format(date);
        System.out.println("FULL: " + fullTime);

	}

}
