/*
Internationalization :
======================
1)Write a code to change the number format to Denmark number format .

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Change the number format to Denmark number format
2.Entity :
    DenmarkNumberFormatDemo 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Create an object for Locale class by specifying the language and region of the country.
    2)Create a NumberFormat and set the locale.
    3)Display the number formatting.
5.Pseudocode :

public class DenmarkNumberFormatDemo {

	public static void main(String[] args) {
		
		Locale locale = new Locale("da", "Dk");
        //formating the number based on locale
        NumberFormat numberFormat = NumberFormat.getInstance(locale);
        System.out.println("Denmark Number Format: " + numberFormat.format(11.757));

	}

}

*/

import java.text.NumberFormat;
import java.util.Locale;

public class DenmarkNumberFormatDemo {

	public static void main(String[] args) {
		
		Locale locale = new Locale("da", "Dk");
        //formating the number based on locale
        NumberFormat numberFormat = NumberFormat.getInstance(locale);
        System.out.println("Denmark Number Format: " + numberFormat.format(11.757));

	}

}
