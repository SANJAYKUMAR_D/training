/*
Concurrency:
============
+ Explain in words Concurrency and Parallelism( in two sentences).

Answer:
=======
Concurrency:
    Concurrency is when two or more tasks can start, run, and complete in overlapping time periods. 

Parallelism:
    Parallelism is when tasks literally run at the same time, for example on a multicore processor

*/