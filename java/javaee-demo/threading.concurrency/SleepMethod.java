/* 
Multi Threading
===============
+ What are the arguments passed for the sleep() method?

Answer:
=======

    method can be used to pause the execution of current thread for specified time in milliseconds.
    
The arguments passed for the sleep() method:
    
    Prototype: public static void sleep (long millis) throws InterruptedException.
    Parameters: millis => The duration of time in milliseconds for which the thread sleeps.
                          The argument value for milliseconds can�t be negative
    Return Type: void.
*/