/*
Concurrency:
============
+ Write a program of performing two tasks by two threads that implements Runnable interface.

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Program of performing two tasks by two threads that implements Runnable interface.
2.Entity :
    RunnableDemo 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Create two classes Even and Odd that implements Runnable interface.
    2)In Even class, run method print only even numbers in the range.
    3)In Odd class, run method print only odd numbers in the range.
    4)Creating objects of Even and Odd classes in main method.
    5)Creating Threads by passing objects into constructor.
    6)Invoking the start method of Threads.
5.Pseudocode :

class Even implements Runnable {

    public void run() {
        for (int number = 1; number <= 10; number++) {
            if (number % 2 == 0) {
                System.out.println("Even numbers: " + number);
            }

            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                System.out.println(e.getStackTrace());
            } 
        }
    }

}

class Odd implements Runnable {

    public void run() {
        for (int number = 1; number <= 10; number++) {
            if (number % 2 != 0) {
                System.out.println("Odd numbers: " + number);
            }

            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                System.out.println(e.getStackTrace());
            }
        }
    }

}

public class RunnableDemo {

	public static void main(String[] args) {
		
		//creating object for class
		Even even = new Even();
        Odd odd = new Odd();

        //creating object for thread.
        // Thread(object, thread_name)
        Thread t1 = new Thread(even, "Even Thread");
        Thread t2 = new Thread(odd, "Odd Thread");

        //invoking thread.
        t1.start();
        t2.start();

	}

}

*/

class Even implements Runnable {

    public void run() {
        for (int number = 1; number <= 10; number++) {
            if (number % 2 == 0) {
                System.out.println("Even numbers: " + number);
            }

            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                System.out.println(e.getStackTrace());
            } 
        }
    }

}

class Odd implements Runnable {

    public void run() {
        for (int number = 1; number <= 10; number++) {
            if (number % 2 != 0) {
                System.out.println("Odd numbers: " + number);
            }

            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                System.out.println(e.getStackTrace());
            }
        }
    }

}

public class RunnableDemo {

	public static void main(String[] args) {
		
		//creating object for class
		Even even = new Even();
        Odd odd = new Odd();

        //creating object for thread.
        // Thread(object, thread_name)
        Thread t1 = new Thread(even, "Even Thread");
        Thread t2 = new Thread(odd, "Odd Thread");

        //invoking thread.
        t1.start();
        t2.start();

	}

}
