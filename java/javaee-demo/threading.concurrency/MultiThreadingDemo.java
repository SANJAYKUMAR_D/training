/*
Multi Threading
===============
+ Write a program which implement multithreading using sleep(), setPriority(), getPriorty(), Name(), getId() methods.

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Program which implement multithreading using sleep(), setPriority(), getPriorty(), Name(), getId() methods.
2.Entity :
    MultiThreadingDemo 
3.Function Declaration :
    public static void main(String[] args) throws Exception
    public void run() 
4.Jobs to be done :
    1)Create a class MultiThreadingDemo by extends Thread class.
    2)Create a method run that invokes getname, sleep, getId methods
    3)Print the values with some time using sleep() method.
    4)In main method, setPriority, and getPriority methods are used.
5.Pseudocode :

public class MultiThreadingDemo extends Thread {

	public void run() {
		
	    for (int i = 0; i < 5; i++) {
	    	
	        //Using getName() method to get thread name.
	        System.out.println(Thread.currentThread().getName() + "  " + i);
	        try {
	            // Displaying the running thread
	            System.out.println ("Thread " + 
	                Thread.currentThread().getId() + " is running");
	            // thread to sleep for 1000 milliseconds
	            Thread.sleep(1000);
            } catch (Exception e) {
	            System.out.println(e);
	        }
	        
	    }
	    
	}
	
	public static void main(String[] args) throws Exception {
		
        Thread thread = new Thread(new MultiThreadingDemo());
	    // this will call run() function
	    thread.start();
	    //setPriority() method 
	  	Thread.currentThread().setPriority(5); 
	  	System.out.println("main thread priority : " + 
		    Thread.currentThread().getPriority()); 

	}

}

*/

public class MultiThreadingDemo extends Thread {

	public void run() {
		
	    for (int i = 0; i < 5; i++) {
	    	
	        //Using getName() method to get thread name.
	        System.out.println(Thread.currentThread().getName() + "  " + i);
	        try {
	            // Displaying the running thread
	            System.out.println ("Thread " + 
	                Thread.currentThread().getId() + " is running");
	            // thread to sleep for 1000 milliseconds
	            Thread.sleep(1000);
            } catch (Exception e) {
	            System.out.println(e);
	        }
	        
	    }
	    
	}
	
	public static void main(String[] args) throws Exception {
		
        Thread thread = new Thread(new MultiThreadingDemo());
	    // this will call run() function
	    thread.start();
	    //setPriority() method 
	  	Thread.currentThread().setPriority(5); 
        System.out.println("main thread priority : " + 
		    Thread.currentThread().getPriority()); 

	}

}
