/*
Set
===
+ Create a set
    => Add 10 values
    => Perform addAll() and removeAll() to the set.
    => Iterate the set using 
        - Iterator
        - for-each
+ Explain the working of contains(), isEmpty() and give example.

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Create a set
    Add 10 values
    Perform addAll() and removeAll() to the set.
    Iterate the set using 
     - Iterator
     - for-each
2.Entity :
    SetDemo 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    i)Import the required packages.
    ii)Create a class SetDemo with main method.
    iii)Create a Set and added with 6 values in the Set.
    iv)Create another Set newEmployee and perform addAll() and removeAll() method with it
    v)Printing the elements using Iterator interface and ForEach.
    viii)contains(), isEmpty() methods are used.
5.Pseudocode :

public class SetDemo {

    public static void main(String[] args) {

        //Creating a set and adding 6 elements to it.
        Set<String> employee = new HashSet<>();

        employee.add("Sanjay");
        employee.add("Selva");
        employee.add("Sabari");
        employee.add("Rohit");
        employee.add("Bumra");
        employee.add("Vijay");

        //Creating another set with some elements.
        Set<String> newEmployee = new HashSet<>();
        newEmployee.add("Dhoni");
        newEmployee.add("johncena");
        newEmployee.add("Yuvan");
        newEmployee.add("Senthil");

        //Performing addAll() method
        employee.addAll(newEmployee);
        System.out.println(employee);
        //Performing removeAll() method
        employee.removeAll(newEmployee);
        System.out.println(employee);
        newEmployee.clear();

        //Displaying all the set elements using Iterator interface
        Iterator<String> items = employee.iterator();
        while (items.hasNext()) {
            System.out.println(items.next());
        }

        //Displaying all the set elements using ForEach
        employee.forEach(System.out::println);

        //contains()
        System.out.println(employee.contains("Sanjay"));

        //isEmpty()
        System.out.println(newEmployee.isEmpty());

    }

}
*/

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetDemo {

    public static void main(String[] args) {

        //Creating a set and adding 6 elements to it.
        Set<String> employee = new HashSet<>();

        employee.add("Sanjay");
        employee.add("Selva");
        employee.add("Sabari");
        employee.add("Rohit");
        employee.add("Bumra");
        employee.add("Vijay");

        //Creating another set with some elements.
        Set<String> newEmployee = new HashSet<>();
        newEmployee.add("Dhoni");
        newEmployee.add("johncena");
        newEmployee.add("Yuvan");
        newEmployee.add("Senthil");

        //Performing addAll() method
        employee.addAll(newEmployee);
        System.out.println(employee);
        //Performing removeAll() method
        employee.removeAll(newEmployee);
        System.out.println(employee);
        newEmployee.clear();

        //Displaying all the set elements using Iterator interface
        Iterator<String> items = employee.iterator();
        while (items.hasNext()) {
            System.out.println("Iterator : " + items.next());
        }

        //Displaying all the set elements using ForEach
        System.out.println("Using ForEach:");
        employee.forEach(System.out::println);

        //contains()
        System.out.println("Bag contains Sanjay? " + employee.contains("Sanjay"));

        //isEmpty()
        System.out.println("is NewEmployee empty? " + newEmployee.isEmpty());

    }

}

/*

contains() - It is a method of Set interface used to check an element is
             present or not.
isEmpty() - It is a method of Set interface used to check a list is empty or not.

*/