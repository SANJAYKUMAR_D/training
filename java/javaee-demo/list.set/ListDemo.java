/*
List
====
+ Create a list
    => Add 10 values in the list
    => Create another list and perform addAll() method with it
    => Find the index of some value with indexOf() and lastIndexOf()
    => Print the values in the list using 
        - For loop
        - For Each
        - Iterator
        - Stream API
    => Convert the list to a set
    => Convert the list to a array
+ Explain about contains(), subList(), retainAll() and give example

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Create a list
    Add 10 values in the list
    Create another list and perform addAll() method with it
    Find the index of some value with indexOf() and lastIndexOf()
    Print the values in the list using 
      - For loop
      - For Each
      - Iterator
      - Stream API
    Convert the list to a set
    Convert the list to a array
2.Entity :
    ListDemo 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    i)Import the required packages.
    ii)Create a class ListDemo with main method.
    iii)Create a list and added with 10 values in the list
    iv)Create another list newStudents and perform addAll() method with it
    v)The index of some students with indexOf() and lastIndexOf() is printed.
    vi)Printing the elements of list using For, Enhanced For, Iterator, and
ForEach of StreamAPI.
    vii)Converting the list into Set and Array.
    viii)contains(), subList(), retainAll() methods are used.
5.Pseudocode :

public class ListDemo {

    public static void main(String[] args) {

        //Creating a list and adding 10 elements to it.
        List<String> students = new ArrayList<>();

        students.add("Sanjay");
        students.add("Santheesh");
        students.add("Harry");
        students.add("Watson");
        students.add("Galgadot");
        students.add("Rose");
        students.add("Robert Drowny");
        students.add("Kalam");
        students.add("Smithy");
        students.add("Billgates");

        System.out.println(students);

        //Creating another list with some elements.
        List<String> newStudents = new ArrayList<>();

        newStudents.add("Karthi");
        newStudents.add("Sara");
        newStudents.add("Rock");

        //And perform addAll() method
        students.addAll(newStudents);

        System.out.println(students);

        //Find the index of a value with indexOf() and lastIndexOf()
        System.out.println(students.indexOf("Rock"));
        System.out.println(students.lastIndexOf("Santheesh"));

        //Print the values in the list using
        //For loop
        System.out.println("For loop:");
        for (int index = 0; index < students.size(); index++) {
            System.out.println(students.get(index));
        }

        //For Each
        System.out.println("For Each:");
        for (String student : students) {
            System.out.println(student);
        }

        //Iterator interface
        Iterator<String> studIter = students.iterator();
        while(studIter.hasNext()) {
            System.out.println(studIter.next());
        }

        //Stream API's forEach
        Stream<String> stream = students.stream();
        stream.forEach(System.out::println);

        //Convert the list to a set
        Set<String> studentSet = new HashSet<>(students);

        //Convert the list to a array
        String[] studentArray = new String[students.size()];

        //contains()
        System.out.println(students.contains("Rose"));

        //subList(startIndex, lastIndex)
        List<String> newlyJoinedStudents = students.subList(10, students.size());
        System.out.println(newlyJoinedStudents);

        //retainAll()
        System.out.println(students.retainAll(newlyJoinedStudents) + students);

    }

}

*/

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

public class ListDemo {

    public static void main(String[] args) {

        //Creating a list and adding 10 elements to it.
        List<String> students = new ArrayList<>();

        students.add("Sanjay");
        students.add("Santheesh");
        students.add("Harry");
        students.add("Watson");
        students.add("Galgadot");
        students.add("Rose");
        students.add("Robert Drowny");
        students.add("Kalam");
        students.add("Smithy");
        students.add("Billgates");

        System.out.println("Student list:\n" + students);

        //Creating another list with some elements.
        List<String> newStudents = new ArrayList<>();

        newStudents.add("Karthi");
        newStudents.add("Sara");
        newStudents.add("Rock");

        //And perform addAll() method
        students.addAll(newStudents);

        System.out.println("New student list:\n" + students);

        //Find the index of a value with indexOf() and lastIndexOf()
        System.out.println("Index of student 'Rock' " + students.indexOf("Rock"));
        System.out.println("Last index of student 'Santheesh' " +
        students.lastIndexOf("Santheesh"));

        //Print the values in the list using
        //For loop
        System.out.println("For loop:");
        for (int index = 0; index < students.size(); index++) {
            System.out.println(students.get(index));
        }

        //For Each
        System.out.println("For Each:");
        for (String student : students) {
            System.out.println(student);
        }

        //Iterator interface
        System.out.println("Iterator interface:");
        Iterator<String> studIter = students.iterator();
        while(studIter.hasNext()) {
            System.out.println(studIter.next());
        }

        //Stream API's forEach
        Stream<String> stream = students.stream();
        System.out.println("Iterating values using Stream API:");
        stream.forEach(System.out::println);

        //Convert the list to a set
        Set<String> studentSet = new HashSet<>(students);

        //Convert the list to a array
        String[] studentArray = new String[students.size()];

        //contains()
        System.out.println(students.contains("Rose"));

        //subList(startIndex, lastIndex)
        List<String> newlyJoinedStudents = students.subList(10, students.size());
        System.out.println(newlyJoinedStudents);

        //retainAll()
        System.out.println("After using retainAll method in students list: " + 
        students.retainAll(newlyJoinedStudents) + "\n" + students);

    }

}

/*

contains() - This method is used to check weather the element present in the list. 
             if yes returns 'true' else 'false'.
subList() - This method is used to get a part of list by
            specifying index.
retainAll() - This method is used to retain the elements of list by comparing the 
              list which is passed as a parameter.

*/