/*
IO Stream:
==========
5.Java program to read a file using filter and buffer input and output streams.

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Program to read a file using filter and buffer input and output streams.
2.Entity :
    FilterAndBufferInputStream 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Initialize the file path.
        1.1)If file not exists throw exception
    2)Get the file data as stream using fileInputStream.
    3)Get the buffered data using FilterInputStream.
    4)Convert the data into character and display the converted data
    5)Close the stream file and stream filter.
5.Pseudocode :

public class FilterAndBufferInputStream {

	public static void main(String[] args) throws IOException {
		
		String filePath = "path";
        FileInputStream streamFile = new FileInputStream(filePath);
        FilterInputStream streamFilter = new BufferedInputStream(streamFile);
        while ((data = streamFilter.read()) != -1) {
            System.out.print((char) data);
        }
        streamFile.close();
        streamFilter.close();

	}

}
*/

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FilterInputStream;
import java.io.IOException;

public class FilterAndBufferInputStream {

	public static void main(String[] args) throws IOException {
		
		String filePath = "C:\\Users\\SanjayKumar\\eclipse-workspace\\javaee-demo\\io.stream\\textContent.txt";

        FileInputStream streamFile = new FileInputStream(filePath);
        //getting buffered stream of data
        FilterInputStream streamFilter = new BufferedInputStream(streamFile);

        //reading the data and converting into character
        int data;
        while ((data = streamFilter.read()) != -1) {
            System.out.print((char) data);
        }
        streamFile.close();
        streamFilter.close();

	}

}
