/*
IO Stream:
==========
4.write a program for ByteArrayInputStream class to read byte array as input stream.

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Program for ByteArrayInputStream class to read byte array as input stream.
2.Entity :
    ByteArrayInputStreamDemo 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Initialize the string.
    2)Convert string data into byte array using getBytes method
    3)Get stream of bytes using ByteArrayInputStream
    4)Read the byte from stream of bytes
        4.1)Convert the int to character and display it.
5.Pseudocode :
public class ByteArrayInputStreamDemo {

	public static void main(String[] args) {
		
		String string = "Welcome!";
        byte[] bytes = string.getBytes();

        //getting the stream of bytes in byte array using ByteArrayInputStream
        ByteArrayInputStream byteStream = new ByteArrayInputStream(bytes);

        //reading the byte and converting into character
        int data;
        while ((data = byteStream.read()) != -1) {
            System.out.println((char) data + " : " + data);
        }

	}

}

*/

import java.io.ByteArrayInputStream;

public class ByteArrayInputStreamDemo {

	public static void main(String[] args) {
		
		String string = "Welcome!";
        byte[] bytes = string.getBytes();

        //getting the stream of bytes in byte array using ByteArrayInputStream
        ByteArrayInputStream byteStream = new ByteArrayInputStream(bytes);

        //reading the byte and converting into character
        int data;
        while ((data = byteStream.read()) != -1) {
            System.out.println((char) data + " : " + data);
        }

	}

}
