/*
IO Stream:
==========
5.Java program to write a file using filter and buffer input and output streams.

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Program to write a file using filter and buffer input and output streams.
2.Entity :
    FilterAndBufferOutputStream 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Initialize the file path.
        1.1)If file not exists throw exception
    2)Create an object of BufferedOutputStream to store buffered sream of data.
    3)Convert the data to be stored into bytes.
    4)Write into file using BufferedOutputStream.
5.Pseudocode :

public class FilterAndBufferOutputStream {

	public static void main(String[] args) throws IOException {
		
		String filePath = "path"; 
        FilterOutputStream streamFilter = new BufferedOutputStream(new FileOutputStream(filePath));
        byte[] byteArray = "\nNew Content Added".getBytes();
        for (byte aByte : byteArray) {
            streamFilter.write(aByte);
        }
        streamFilter.close();
        System.out.println("written successfully.");

	}

}
*/

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;

public class FilterAndBufferOutputStream {

	public static void main(String[] args) throws IOException {
		
		String filePath = "C:\\Users\\SanjayKumar\\eclipse-workspace\\javaee-demo\\io.stream\\textContent.txt"; 
        FilterOutputStream streamFilter = new BufferedOutputStream(new FileOutputStream(filePath));

        //Converting into bytes
        byte[] byteArray = "\nNew Content Added".getBytes();

        //Filter output stream converts the byte and write into file
        for (byte aByte : byteArray) {
            streamFilter.write(aByte);
        }

        streamFilter.close();
        System.out.println("written successfully.");

	}

}
