/*
IO Stream:
==========
3.write a Java program reads data from a particular file using FileReader and writes it to another, 
using FileWriter.

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Program reads data from a particular file using FileReader and writes it to another, 
using FileWriter.
2.Entity :
    ReadAndWriteDemo 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Initialize the file path.
    2)Read the file using Reader
    3)Write in new file using Writer
    4)Close the files.
5.Pseudocode :

public class ReadAndWriteDemo {

	@SuppressWarnings("resource")
	public static void main(String[] args) throws IOException {
		
		int character;

        String fileToRead = "C:\\Users\\SanjayKumar\\eclipse-workspace\\javaee-demo\\io.stream\\textContent.txt";
        String fileToWrite = "C:\\Users\\SanjayKumar\\eclipse-workspace\\javaee-demo\\io.stream\\textContent2.txt";

        FileReader reader = new FileReader(fileToRead);
        FileWriter writer = new FileWriter(fileToWrite);

        while ((character = reader.read()) != -1) {
            writer.write((char) character);
        }
        System.out.println("File copied");

	}

}
*/

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ReadAndWriteDemo {

	@SuppressWarnings("resource")
	public static void main(String[] args) throws IOException {
		
		int character;

        String fileToRead = "C:\\Users\\SanjayKumar\\eclipse-workspace\\javaee-demo\\io.stream\\textContent.txt";
        String fileToWrite = "C:\\Users\\SanjayKumar\\eclipse-workspace\\javaee-demo\\io.stream\\textContent2.txt";

        FileReader reader = new FileReader(fileToRead);
        FileWriter writer = new FileWriter(fileToWrite);

        while ((character = reader.read()) != -1) {
            writer.write((char) character);
        }
        System.out.println("File copied");

	}

}
