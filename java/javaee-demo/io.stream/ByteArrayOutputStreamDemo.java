/*
IO Stream:
==========
2.write a program that allows to write data to multiple files together using bytearray outputstream 

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Program that allows to write data to multiple files together using bytearray outputstream
2.Entity :
    ByteArrayOutputStreamDemo 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Initialize the file path.
    2)Initialize the string to stored in multiple files.
	3)Convert the data to array of bytes.
	4)Write the array of bytes to the object of ByteArrayOutputStream.
	5)Write the data back to the files using writeTo method in ByteArrayOutputStream.
    6)Close the open files.
5.Pseudocode :

public class ByteArrayOutputStreamDemo {

	public static void main(String[] args) throws IOException {
		
		FileOutputStream file1 = null;
        FileOutputStream file2 = null;
        ByteArrayOutputStream byteArrOutputStream = null;

        try {
            file1 = new FileOutputStream("C:\\Users\\SanjayKumar\\eclipse-workspace\\javaee-demo\\io.stream\\textFile1.txt");
            file2 = new FileOutputStream("C:\\Users\\SanjayKumar\\eclipse-workspace\\javaee-demo\\io.stream\\textFile2.txt");
            byteArrOutputStream = new ByteArrayOutputStream();

            String data = "\nThis message is commonly writen in both files";
            byte[] bytes = data.getBytes();
            byteArrOutputStream.write(bytes);

            byteArrOutputStream.writeTo(file1);
            byteArrOutputStream.writeTo(file2);

            System.out.println("Data written completed.");
        } catch (IOException ioe){
            System.out.println(ioe.getMessage());
        } finally {
            // closing the files and byte array output stream
            file1.close();
            file2.close();
            byteArrOutputStream.close();
        }

	}

}
*/

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class ByteArrayOutputStreamDemo {

	public static void main(String[] args) throws IOException {
		
		FileOutputStream file1 = null;
        FileOutputStream file2 = null;
        ByteArrayOutputStream byteArrOutputStream = null;

        try {
            file1 = new FileOutputStream("C:\\Users\\SanjayKumar\\eclipse-workspace\\javaee-demo\\io.stream\\textFile1.txt");
            file2 = new FileOutputStream("C:\\Users\\SanjayKumar\\eclipse-workspace\\javaee-demo\\io.stream\\textFile2.txt");
            byteArrOutputStream = new ByteArrayOutputStream();

            String data = "\nThis message is commonly writen in both files";
            byte[] bytes = data.getBytes();
            byteArrOutputStream.write(bytes);

            byteArrOutputStream.writeTo(file1);
            byteArrOutputStream.writeTo(file2);

            System.out.println("Data written completed.");
        } catch (IOException ioe){
            System.out.println(ioe.getMessage());
        } finally {
            // closing the files and byte array output stream
            file1.close();
            file2.close();
            byteArrOutputStream.close();
        }

	}

}
