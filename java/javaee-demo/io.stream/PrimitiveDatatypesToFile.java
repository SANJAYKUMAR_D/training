/*
IO Stream:
==========
6.program to write primitive datatypes to file using by dataoutputstream.

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Program to write primitive datatypes to file using by dataoutputstream.
2.Entity :
    PrimitiveDatatypesToFile 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Initialize the file path.
        1.1)If file not exists throw exception.
    2)Create an object of DataOutputStream to store values of primitive types.
    3)Using writeInt, writeUTF, writeChar, writeDouble, writeBoolean, 
      to write primitive type values.
    4)Close the file.
5.Pseudocode :

public class PrimitiveDatatypesToFile {

	public static void main(String[] args)  throws IOException {
		
            String path = "path";
            DataOutputStream dataOutStream = new DataOutputStream(
                    new FileOutputStream(path));

            //Writing primitive data to the file
            dataOutStream.writeInt(2020);
            dataOutStream.writeUTF("Sanjay");
            dataOutStream.writeChar('D');
            dataOutStream.writeDouble(77.77);
            dataOutStream.writeBoolean(true);

            dataOutStream.close();
            System.out.println("Primitive datatypes written to file successfully");

	}

}
*/

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class PrimitiveDatatypesToFile {

	public static void main(String[] args)  throws IOException {
		
            String path = "C:\\Users\\SanjayKumar\\eclipse-workspace\\javaee-demo\\io.stream\\textContent2.txt";
            DataOutputStream dataOutStream = new DataOutputStream(
                    new FileOutputStream(path));

            //Writing primitive data to the file
            dataOutStream.writeInt(2020);
            dataOutStream.writeUTF("Sanjay");
            dataOutStream.writeChar('D');
            dataOutStream.writeDouble(77.77);
            dataOutStream.writeBoolean(true);

            dataOutStream.close();
            System.out.println("Primitive datatypes written to file successfully");

	}

}
