/*
IO Stream:
==========
1.How to read the file contents  line by line in streams with example

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Read the file contents  line by line in streams with example.
2.Entity :
    ReadFileByStreamDemo 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Initialize the file path.
    2)Get stream of lines in file using lines method in Files class.
    3)Display the lines one by one using Stream's forEach method.
5.Pseudocode :

public class ReadFileByStreamDemo {

	public static void main(String[] args) {
		
        String filePath = "C:\\Users\\SanjayKumar\\eclipse-workspace\\javaee-demo\\io.stream\\textContent.txt";

        try (Stream<String> lines = Files.lines(Paths.get(filePath))) {
            lines.forEach(line -> System.out.println("> " + line));
        } catch (IOException e) {
            e.printStackTrace();
        }

	}

}
*/

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class ReadFileByStreamDemo {

	public static void main(String[] args) {
		
        String filePath = "C:\\Users\\SanjayKumar\\eclipse-workspace\\javaee-demo\\io.stream\\textContent.txt";

        try (Stream<String> lines = Files.lines(Paths.get(filePath))) {
            lines.forEach(line -> System.out.println("> " + line));
        } catch (IOException e) {
            e.printStackTrace();
        }

	}

}
