/*
IO/NIO:
=======
15. Create two paths and test whether they represent same path

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Create two paths and test whether they represent same path.
2.Entity :
    SamePathTestDemo 
3.Function Declaration :
    public static void main(String[] args)
    static void checkPath(Path p1, Path p2) throws IOException 
4.Jobs to be done :
    1)Initialize the path of a file by invoking Path class.
        1.1)throw exception, If file not exists on the specified path.
    2)Check the paths 
        2.1)If its same display as Same paths.
        2.1)Otherwise display as Not same paths.
5.Pseudocode :

public class SamePathTestDemo {
	
	static void checkPath(Path p1, Path p2) throws IOException {
		
        if (Files.isSameFile(p1, p2)) {
            System.out.println("Same paths");
        } else {
        	System.out.println("Not same paths");
        }
        
    }

	public static void main(String[] args) throws IOException {
		
            //getting three paths from system
            Path path1 = Paths.get("C:\\Users\\SanjayKumar\\eclipse-workspace\\javaee-demo\\nio");
            Path path2 = Paths.get("C:\\Users\\SanjayKumar\\eclipse-workspace\\javaee-demo\\nio");
            Path path3 = Paths.get("C:\\Users\\SanjayKumar\\eclipse-workspace\\javaee-demo\\generic");

            //checking the paths are same
            checkPath(path1, path2);
            checkPath(path1, path3);

	}

}
*/

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SamePathTestDemo {
	
	static void checkPath(Path p1, Path p2) throws IOException {
		
        if (Files.isSameFile(p1, p2)) {
            System.out.println("Same paths");
        } else {
        	System.out.println("Not same paths");
        }
        
    }

	public static void main(String[] args) throws IOException {
		
            //getting three paths from system
            Path path1 = Paths.get("C:\\Users\\SanjayKumar\\eclipse-workspace\\javaee-demo\\nio");
            Path path2 = Paths.get("C:\\Users\\SanjayKumar\\eclipse-workspace\\javaee-demo\\nio");
            Path path3 = Paths.get("C:\\Users\\SanjayKumar\\eclipse-workspace\\javaee-demo\\generic");

            //checking the paths are same
            checkPath(path1, path2);
            checkPath(path1, path3);

	}

}
