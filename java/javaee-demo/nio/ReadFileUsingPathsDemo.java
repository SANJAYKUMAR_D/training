/*
IO/NIO:
=======
7. Read a file using java.nio.Files using Paths

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Read a file using java.nio.Files using Paths.
2.Entity :
    ReadFileUsingPathsDemo 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Initialize the path of a file.
        1.1)throw exception, If file not exists on the specified path.
    2)Read the file using readAllLines method of Files and store it into list.
    3)Read the file as bytes using readAllBytes() method and store in into byte array.
    4)Display the list
5.Pseudocode :

public class ReadFileUsingPathsDemo {

	public static void main(String[] args) throws IOException {
		
		Path path = Paths.get("C:\\Users\\SanjayKumar\\eclipse-workspace\\javaee-demo\\nio\\textFileDemo.txt");
        List<String> lines = Files.readAllLines(path);
        byte[] bytes = Files.readAllBytes(path);

        System.out.println(lines);
        System.out.println(new String(bytes));

	}

}
*/

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class ReadFileUsingPathsDemo {

	public static void main(String[] args) throws IOException {
		
		Path path = Paths.get("C:\\Users\\SanjayKumar\\eclipse-workspace\\javaee-demo\\nio\\textFileDemo.txt");
        List<String> lines = Files.readAllLines(path);
        byte[] bytes = Files.readAllBytes(path);

        System.out.println(lines);
        System.out.println(new String(bytes));

	}

}
