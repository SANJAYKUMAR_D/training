/*
IO/NIO:
=======
13. Get the permission allowed for a file

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Get the permission allowed for a file
2.Entity :
    FilePermissionDemo 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Initialize the path of a file.
    2)check the file exists 
        2.1)if exists check file can be readable, writable and executable and display. 
    3)otherwise, Display as file not exists.
5.Pseudocode :

public class FilePermissionDemo {

	public static void main(String[] args) {
		
		File file = new File("C:\\Users\\SanjayKumar\\eclipse-workspace\\javaee-demo\\nio\\textFileDemo.txt");
        boolean exist = file.exists();

        if (exist) {
            //checking the file can be readable, writable and executable
            System.out.println("Executable: " + file.canExecute());
            System.out.println("Writable: " + file.canWrite());
            System.out.println("Readable: " + file.canRead());
        } else {
        	System.out.println("File not exists");
        }

	}

}
*/

import java.io.File;

public class FilePermissionDemo {

	public static void main(String[] args) {
		
		File file = new File("C:\\Users\\SanjayKumar\\eclipse-workspace\\javaee-demo\\nio\\textFileDemo.txt");
        boolean exist = file.exists();

        if (exist) {
            //checking the file can be readable, writable and executable
            System.out.println("Executable: " + file.canExecute());
            System.out.println("Writable: " + file.canWrite());
            System.out.println("Readable: " + file.canRead());
        } else {
        	System.out.println("File not exists");
        }

	}

}
