/*
IO/NIO:
=======
10. InputStream to String and vice versa

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    InputStream to String and vice versa
2.Entity :
    InputStreamToStringDemo 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Initialize the path of a file.
    2)Read the file using the input stream reader. 
    3)Get the buffered data using buffered reader.
    4)Read lines in buffered reader and append to string buffer.
    5)Display Input Stream as String.
5.Pseudocode :

public class InputStreamToStringDemo {

	public static void main(String[] args)  throws IOException {
		
		String path = "Path";
        InputStreamReader reader = new InputStreamReader(new FileInputStream(path));
        @SuppressWarnings("resource")
        BufferedReader bufferReader = new BufferedReader(reader);

        StringBuffer stringBuffer = new StringBuffer();

        String line;
        while ((line = bufferReader.readLine()) != null) {
            stringBuffer.append(line);
            stringBuffer.append("\n");
        }
        String data = stringBuffer.toString();
        System.out.println(data);

        String string = "String to Input Stream";
        InputStream inputStream = new ByteArrayInputStream(string.getBytes());
        System.out.println(inputStream);

	}

}
*/

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class InputStreamToStringDemo {

	public static void main(String[] args)  throws IOException {
		
		String path = "C:\\Users\\SanjayKumar\\eclipse-workspace\\javaee-demo\\nio\\textFileDemo.txt";
        InputStreamReader reader = new InputStreamReader(new FileInputStream(path));
        @SuppressWarnings("resource")
        BufferedReader bufferReader = new BufferedReader(reader);

        StringBuffer stringBuffer = new StringBuffer();

        String line;
        while ((line = bufferReader.readLine()) != null) {
            stringBuffer.append(line);
            stringBuffer.append("\n");
        }
        String data = stringBuffer.toString();
        System.out.println(data);

        String string = "String to Input Stream";
        InputStream inputStream = new ByteArrayInputStream(string.getBytes());
        System.out.println(inputStream);

	}

}
