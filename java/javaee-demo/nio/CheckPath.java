/*
IO/NIO:
=======
9. Given a path, check if path is file or directory

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Given a path, check if path is file or directory.
2.Entity :
    CheckPath 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Initialize the path of a file
    2)Check the file path
        2.1)If the path is a file, display as file
        2.2)Otherwise, display as directory.
5.Pseudocode :

public class CheckPath {

	public static void main(String[] args) {
		
		File filePath = new File("Path");
        if (filePath.isFile()) {
            System.out.println("The given path is file");
        } else {
            System.out.println("The given path is directory");
        }

	}

}
*/

import java.io.File;

public class CheckPath {

	public static void main(String[] args) {
		
		File filePath = new File("C:\\Users\\SanjayKumar\\eclipse-workspace\\javaee-demo\\nio");
        if (filePath.isFile()) {
            System.out.println("The given path is file");
        } else {
            System.out.println("The given path is directory");
        }

	}

}
