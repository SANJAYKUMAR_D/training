/*
IO/NIO:
=======
12. Number of files in a directory and number of directories in a directory

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Number of files in a directory and number of directories in a directory.
2.Entity :
    DirectoryLengthDemo 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Initialize the path of a file.
    2)Use list method to make list of directories.
    3)Display the count of list using length method.
5.Pseudocode :

public class DirectoryLengthDemo {

	public static void main(String[] args) {
		
		File path = new File("Path");
        int directoriesCount = path.list().length;
        System.out.println("Number of directories: " + directoriesCount);

	}

}
*/

import java.io.File;

public class DirectoryLengthDemo {

	public static void main(String[] args) {
		
		File path = new File("C:\\Users\\SanjayKumar\\eclipse-workspace\\javaee-demo\\nio");
        int directoriesCount = path.list().length;
        System.out.println("Number of directories: " + directoriesCount);

	}

}
