/*
IO/NIO:
=======
8. Reading a CSV file using java.nio.Files API as List string with each 
row in CSV as a String

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Reading a CSV file using java.nio.Files API as List string with each 
row in CSV as a String
2.Entity :
    ReadCSVFileDemo 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Initialize the path of a file.
        1.1)throw exception, If file not exists on the specified path.
    2)Read each lines of file using readAllLines method of Files and store it into list.
    3)Display line by line using forEach of Stream.
5.Pseudocode :

public class ReadCSVFileDemo {

	public static void main(String[] args) throws IOException {
		
		Path path = Paths.get("Path");
        List<String> lines = Files.readAllLines(path);
        for (String line : lines) {
            System.out.println(line);
        }

	}

}

*/

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class ReadCSVFileDemo {

	public static void main(String[] args)  throws IOException {
		
		Path path = Paths.get("C:\\Users\\SanjayKumar\\eclipse-workspace\\javaee-demo\\nio\\employeeName.csv");
        List<String> lines = Files.readAllLines(path);
        for (String line : lines) {
            System.out.println(line);
        }

	}

}
