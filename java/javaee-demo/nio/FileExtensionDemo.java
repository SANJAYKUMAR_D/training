/*
IO/NIO:
=======
14. Get the file names of all file with specific extension in a directory

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Get the file names of all file with specific extension in a directory
2.Entity :
    FileExtensionDemo 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Initialize the path of a file.
    2)Filter the file based on the file name using FilenameFilter interface.
    3)Store a filtered file name into a list.
    4)Display the filenames in list.
5.Pseudocode :

public class FileExtensionDemo {

	public static void main(String[] args) {
		
		File path = new File("C:\\Users\\SanjayKumar\\eclipse-workspace\\javaee-demo\\nio");

        // filter based on file name ends with .java and store to list
        String[] files = path.list(new FilenameFilter() {

            @Override
            public boolean accept(File dir, String name) {
                if (name.endsWith(".java")) {
                    return true;
                } else return false;
            }

        });
        for (String file : files) {
            System.out.println(file);
        }

	}

}
*/

import java.io.File;
import java.io.FilenameFilter;

public class FileExtensionDemo {

	public static void main(String[] args) {
		
		File path = new File("C:\\Users\\SanjayKumar\\eclipse-workspace\\javaee-demo\\nio");

        // filter based on file name ends with .java and store to list
        String[] files = path.list(new FilenameFilter() {

            @Override
            public boolean accept(File dir, String name) {
                if (name.endsWith(".java")) {
                    return true;
                } else return false;
            }

        });
        for (String file : files) {
            System.out.println(file);
        }

	}

}
