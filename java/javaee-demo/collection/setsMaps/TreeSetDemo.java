/*
SETS and MAPS:
==============
4.java program to demonstrate insertions and string buffer in tree set

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Program to demonstrate insertions and string buffer in tree set.
2.Entity :
    TreeSetDemo 
3.Function Declaration :
    public static void main(String[] args)
    public int compare(StringBuffer string1, StringBuffer string2)
4.Jobs to be done :
    1)Create a class TreeSetDemo with main method.
    2)Add numbers to a numbers tree set.
    3)Add mobileNames to a mobiles tree set using string builder or string buffer.
    4)If any interruption occurs because of casting string to string buffer.
        4.1)It can be handled by implementing Comparable interface. or
        4.2)Converting StringBuffer to String using method.
5.Pseudocode :

public class TreeSetDemo implements Comparator<StringBuffer> {
	
	public static void main(String[] args) {
		
		TreeSet<Integer> numbers = new TreeSet<>();
        ADD elements to the TreeSet.

        System.out.println(numbers);

        Set<StringBuffer> mobiles = new TreeSet<>(new TreeSetDemo());
        ADD elements to the set.

        System.out.println(mobiles);

	}

}
*/

package setsMaps;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

public class TreeSetDemo implements Comparator<StringBuffer> {

	@Override
    public int compare(StringBuffer string1, StringBuffer string2) {
        return string1.toString().compareTo(string2.toString());
    }
	
	public static void main(String[] args) {
		
		TreeSet<Integer> numbers = new TreeSet<>();
        numbers.add(55);
        numbers.add(22);
        numbers.add(44);
        numbers.add(77);
        numbers.add(66);

        System.out.println(numbers);

        Set<StringBuffer> mobiles = new TreeSet<>(new TreeSetDemo());
        mobiles.add(new StringBuffer("REDMI"));
        mobiles.add(new StringBuffer("OPPO"));
        mobiles.add(new StringBuffer("LENOVA"));
        mobiles.add(new StringBuffer("ONE+"));
        mobiles.add(new StringBuffer("SAMSUNG"));

        System.out.println(mobiles);

	}

}
