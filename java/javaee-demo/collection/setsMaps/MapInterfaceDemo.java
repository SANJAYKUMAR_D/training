/*
SETS and MAPS:
==============
5.demonstrate java program to working of map interface using( put(), remove(), booleanContainsValue(), 
replace() ) . If possible try remaining methods.

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Program to working of map interface using( put(), remove(), booleanContainsValue(), 
replace() ) . If possible try remaining methods.
2.Entity :
    MapInterfaceDemo 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    i)Create a class MapInterfaceDemo with main method.
    ii)Create a map colleges and add entries to it.
    iii)Remove colleges from studentsMarks map.
    iv)Check whether a random college is present in colleges map.
    v)Change the college name of particular college code using replace method.
    vi)Clear all the entries in map
5.Pseudocode :

public class MapInterfaceDemo {

	public static void main(String[] args) {
		
		Map<Integer, String> colleges = new HashMap<>();
        ADD elements to the map.
        System.out.println(colleges);
        // adding key value pair to map 'studentsMarks'

        colleges.remove(2345);
        System.out.println(colleges);
        // removing a entries using key

        boolean check = colleges.containsKey(3016);
        System.out.println("contains: " + check);
        // check the key is present in a map

        if (check) {
        	colleges.replace(3004, "Bannari Amman");
        }
        // replacing the value of the key present in map

        colleges.entrySet().forEach(System.out::println);
        // getting the entry set using entrySet method 

        System.out.println(colleges.hashCode());

        colleges.clear();
        // clear all the entries in map
        System.out.println(colleges);

	}

}

*/

package setsMaps;

import java.util.HashMap;
import java.util.Map;

public class MapInterfaceDemo {

	public static void main(String[] args) {
		
		Map<Integer, String> colleges = new HashMap<>();
        colleges.put(2003, "KPR");
        colleges.put(2021, "Sri Krishna");
        colleges.put(2485, "PSG");
        colleges.put(3004, "CIT");
        colleges.put(2345, "ANGEL");
        colleges.put(3016, "KCT");
        System.out.println(colleges);
        // adding key value pair to map 'studentsMarks'

        colleges.remove(2345);
        System.out.println(colleges);
        // removing a entries using key

        boolean check = colleges.containsKey(3016);
        System.out.println("contains: " + check);
        // check the key is present in a map

        if (check) {
        	colleges.replace(3004, "Bannari Amman");
        }
        // replacing the value of the key present in map

        colleges.entrySet().forEach(System.out::println);
        // getting the entry set using entrySet method 

        System.out.println(colleges.hashCode());

        colleges.clear();
        // clear all the entries in map
        System.out.println(colleges);

	}

}
