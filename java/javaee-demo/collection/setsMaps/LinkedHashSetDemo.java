/*
SETS and MAPS:
==============
2.demonstrate program explaining basic add and traversal operation of linked hash set

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Program explaining basic add and traversal operation of linked hash set
2.Entity :
    LinkedHashSetDemo 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    i)Create a class LinkedHashSetDemo with main method.
    ii)Create a LinkedHashSet and add a elements to it.
    iii)Iterating elements using while loop in set.
5.Pseudocode :

public class LinkedHashSetDemo {

	public static void main(String[] args) {
		
		LinkedHashSet<String> iplTeam = new LinkedHashSet<>(10, 2);
		ADD elements to the set.
        
        //while adding the elements the order of the elements won't change
        //if same element repeats the repeated element not get added into set.
        Iterator<?> name = iplTeam.iterator();
        while(name.hasNext()) {
            System.out.println(name.next());
        }
        // traversing all elements in the set

	}

}

*/

package setsMaps;

import java.util.Iterator;
import java.util.LinkedHashSet;

public class LinkedHashSetDemo {

	public static void main(String[] args) {
		
		LinkedHashSet<String> iplTeam = new LinkedHashSet<>(10, 2);
		iplTeam.add("CSK");
		iplTeam.add("RCB");
		iplTeam.add("SRH");
		iplTeam.add("KKR");
		iplTeam.add("DC");
        
        //while adding the elements the order of the elements won't change
        //if same element repeats the repeated element not get added into set.
        Iterator<?> name = iplTeam.iterator();
        while(name.hasNext()) {
            System.out.println(name.next());
        }
        // traversing all elements in the set

	}

}
