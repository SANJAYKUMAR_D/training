/*
SETS and MAPS:
==============
3.demonstrate linked hash set to array() method in java

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Linked hash set to array() method in java
2.Entity :
    LinkedHashSetToArray 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    i)Create a class LinkedHashSetToArray with main method.
    ii)Create a LinkedHashSet and add elements to it.
    iii)Convert the iplTeams HashSet into array.
5.Pseudocode :

public class LinkedHashSetToArray {

	public static void main(String[] args) {
		
		LinkedHashSet<String> iplTeam = new LinkedHashSet<>();
		ADD elements to the set.

        System.out.println(iplTeam);
        //printing before converting the set into array

        String[] iplName = new String[5];
        iplName = iplTeam.toArray(iplName);
        System.out.println("The arr[] is:"); 
        for (int i = 0; i < iplName.length; i++) 
            System.out.println(iplName[i]);
        //printing after converting the set into array

	}

}
*/

package setsMaps;

import java.util.LinkedHashSet;

public class LinkedHashSetToArray {

	public static void main(String[] args) {
		
		LinkedHashSet<String> iplTeam = new LinkedHashSet<>();
		iplTeam.add("CSK");
		iplTeam.add("RCB");
		iplTeam.add("SRH");
		iplTeam.add("KKR");
		iplTeam.add("DC");

        System.out.println(iplTeam);
        //printing before converting the set into array

        String[] iplName = new String[5];
        iplName = iplTeam.toArray(iplName);
        System.out.println("The arr[] is:"); 
        for (int i = 0; i < iplName.length; i++) 
            System.out.println(iplName[i]);
        //printing after converting the set into array

	}

}