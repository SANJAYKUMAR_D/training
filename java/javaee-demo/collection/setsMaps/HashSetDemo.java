/*
SETS and MAPS:
==============
1.java program to demonstrate adding elements, displaying, removing, and iterating in hash set

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Program to demonstrate adding elements, displaying, removing, and iterating in hash set
2.Entity :
    HashSetDemo 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    i)Create a class HashSetDemo with main method.
    ii)Create a Set and add a elements to it.
    iii)Displaying the set.
    iv)Removing an names in the set.
    v)Iterating elements using foreach loop in set.
5.Pseudocode :

public class HashSetDemo {

	public static void main(String[] args) {
		
		Set<String> employeeName = new HashSet<>();
        // creating a set
		ADD elements to the set

        System.out.println(employeeName);
        // displaying the set

        System.out.println("Item removed: " + employeeName.remove("Dinesh"));
        // removing an element

        for (String name : employeeName) {
            System.out.println(name);
        }
        // iterating elements in set

	}

}

*/

package setsMaps;

import java.util.HashSet;
import java.util.Set;

public class HashSetDemo {

	public static void main(String[] args) {
		
		Set<String> employeeName = new HashSet<>();
        // creating a set
		employeeName.add("Sanjay");
		employeeName.add("Dinesh");
		employeeName.add("Harbajan");
		employeeName.add("Natarajan");
		employeeName.add("Bumra");
        // adding elements to set

        System.out.println(employeeName);
        // displaying the set

        System.out.println("Item removed: " + employeeName.remove("Dinesh"));
        // removing an element

        for (String name : employeeName) {
            System.out.println(name);
        }
        // iterating elements in set

	}

}
