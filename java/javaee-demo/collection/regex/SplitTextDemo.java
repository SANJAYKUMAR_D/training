/*
Regex:
======
2. split any random text using any pattern you desire.
   
Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Split any random text using any pattern you desire.
2.Entity :
    SplitTextDemo 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Create a class with main method.
    2)Initialize the input String.
    3)split the directory based on the sub string in split() method
    4)Print the splited path
5.Pseudocode :

public class SplitTextDemo {

	public static void main(String[] args) {
		
		String directory = "G:/PHOTOS/candit/Snapseed/img45.png";

        // spliting the directory based on String in split method
        for (String path : directory.split("/")) {
            System.out.println(path);
        }

	}

}
*/

package regex;

public class SplitTextDemo {

	public static void main(String[] args) {
		
		String directory = "G:/PHOTOS/candit/Snapseed/img45.png";

        // spliting the directory based on String in split method
        for (String path : directory.split("/")) {
            System.out.println(path);
        }

	}

}
