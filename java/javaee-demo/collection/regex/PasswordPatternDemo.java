/*
Regex:
======
1. create a pattern for password which contains
   8 to 15 characters in length
   Must have at least one uppercase letter
   Must have at least one lower case letter
   Must have at least one digit
   
Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Create a pattern for password which contains
    - 8 to 15 characters in length
    - Must have at least one uppercase letter
    - Must have at least one lower case letter
    - Must have at least one digit
2.Entity :
    PasswordPatternDemo 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Create a class LinkedHashSetDemo with main method.
    2)Get the input from user using scanner class.
    3)Check the input has any null or space or any special characters
        3.1)If, Throw exception that input should not contain null or any special 
            characters.
    4)Create a pattern that
        4.1)check whether the password length lies around 8 to 15 characters.
        4.2)check the password has at least one upper case character.
        4.2)check the password has at least one lower case character.
        4.3)check the password has at least one number(digit).
    5)Check whether the input matches the pattern.
        5.1)If the input matches print "Valid Password"
        5.2)orelse print "Invalid Password"
5.Pseudocode :

public class PasswordPatternDemo {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
        System.out.println("Enter Password: ");
        String password = scanner.nextLine();

        try {
            if (password.equals("") || password.contains(" ")) {
                throw new Exception("Input should not contain null or space");
            }
        }
        catch (Exception e) {
            System.out.println(e);
        }
        if (Pattern.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]{8,15}$")
                .matcher(password).find()) {
            System.out.println("Valid Password");
        }
        else {
            System.out.println("Invalid Password");
        }

	}

}

*/

package regex;

import java.util.Scanner;
import java.util.regex.Pattern;

public class PasswordPatternDemo {

	public static void main(String[] args) {
		
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
        System.out.println("Enter Password: ");
        String password = scanner.nextLine();

        try {
            if (password.equals("") || password.contains(" ")) {
                throw new Exception("Input should not contain null or space");
            }
        }
        catch (Exception e) {
            System.out.println(e);
        }
        if (Pattern.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]{8,15}$")
                .matcher(password).find()) {
            System.out.println("Valid Password");
        }
        else {
            System.out.println("Invalid Password");
        }

	}

}
