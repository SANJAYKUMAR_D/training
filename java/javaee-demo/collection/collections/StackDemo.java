/*
Collections:
============
5.add and remove the elements in stack

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Add and remove the elements in stack
2.Entity :
    StackDemo 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Create a stack and Add elements to the stack.
    2)Remove a elements from the stack.
5.Pseudocode :

public class StackDemo {

	public static void main(String[] args) {
		
		Stack<String> student = new Stack<>();
		ADD elements to the stack
        System.out.println(student);

        System.out.println("Removed student: " + student.pop());

        System.out.println(student);

	}

}

*/

package collections;

import java.util.Stack;

public class StackDemo {

	public static void main(String[] args) {
		
		Stack<String> student = new Stack<>();
		student.push("Sanjay");
		student.push("Arun");
		student.push("Naveen");
		student.push("Surya");
		student.push("Jeeva");
        System.out.println(student);

        System.out.println("Removed student: " + student.pop());

        System.out.println(student);

	}

}
