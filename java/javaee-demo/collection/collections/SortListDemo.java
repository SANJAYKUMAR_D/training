/*
Collections:
============
3.code for sorting vetor list in descending order.

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Code for sorting vetor list in descending order.
2.Entity :
    SortListDemo 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Create a vector list and Add elements to the list
    2)Sort the list in the reverse order
    3)Display the sorted list
5.Pseudocode :

public class SortListDemo {

	public static void main(String[] args) {
		
		Vector<String> avengers = new Vector<>();
		ADD elements to the vector

        System.out.println(avengers);

        avengers.sort(Collections.reverseOrder());

        System.out.println(avengers);

	}

}

*/

package collections;

import java.util.Collections;
import java.util.Vector;

public class SortListDemo {

	public static void main(String[] args) {
		
		Vector<String> avengers = new Vector<>();
		avengers.add("Iron man");
		avengers.add("Captain america");
		avengers.add("Thor");
        avengers.add("Hulk");
        avengers.add("Spider man");

        System.out.println(avengers);

        avengers.sort(Collections.reverseOrder());

        System.out.println(avengers);

	}

}
