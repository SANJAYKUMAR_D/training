/*
Collections:
============
1.create an array list with 7 elements, and create an empty linked list add all elements 
of the array list to linked list ,traverse the elements and display the result
    
Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Create an array list with 7 elements, and create an empty linked list add all elements 
of the array list to linked list ,traverse the elements and display the result
2.Entity :
    ArrayLinkedListDemo 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Create a array list and add 7 elements to it.
    2)Create a empty linked list and add all the elements in array list to linked list.
    3)For each elements in linked list
        3.1)Display the elements.
5.Pseudocode :

public class ArrayLinkedListDemo {

	public static void main(String[] args) {
		
		ArrayList<String> brands = new ArrayList<>();
		brands.add("Puma");
		brands.add("Nike");
		brands.add("Addidas");
		brands.add("Allen");
		brands.add("Ramraj");
		brands.add("Titan");
		brands.add("MRF");

        LinkedList<String> linkedList = new LinkedList<>();
        linkedList.addAll(brands);

        for (String brand : linkedList) {
            System.out.println(brand);
        }

	}

}

*/

package collections;

import java.util.ArrayList;
import java.util.LinkedList;

public class ArrayLinkedListDemo {

	public static void main(String[] args) {
		
		ArrayList<String> brands = new ArrayList<>();
		brands.add("Puma");
		brands.add("Nike");
		brands.add("Addidas");
		brands.add("Allen");
		brands.add("Ramraj");
		brands.add("Titan");
		brands.add("MRF");

        LinkedList<String> linkedList = new LinkedList<>();
        linkedList.addAll(brands);

        for (String brand : linkedList) {
            System.out.println(brand);
        }

	}

}
