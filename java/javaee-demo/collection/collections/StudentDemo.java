/*
Collections:
============
6.LIST CONTAINS 10 STUDENT NAMES
krishnan, abishek, arun, vignesh, kiruthiga, murugan, adhithya, balaji, vicky, priya and 
display only names starting with 'A'.

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    LIST CONTAINS 10 STUDENT NAMES
krishnan, abishek, arun, vignesh, kiruthiga, murugan, adhithya, balaji, vicky, priya and 
display only names starting with 'A'.
2.Entity :
    StudentDemo 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Create a list and add student names in that list.
    2)For each student in students list
        2.1)Check if the student name starts with 'A'
            2.1.1)Display the name of the student.
        2.2)orelse leave the name.
5.Pseudocode :

public class StudentDemo {

	public static void main(String[] args) {
		
		List<String> students = new ArrayList<>();
        ADD names to the list

        for (String student : students) {
            if (student.startsWith("A")) {
                System.out.println(student);
            }
        }

	}

}

*/

package collections;

import java.util.ArrayList;
import java.util.List;

public class StudentDemo {

	public static void main(String[] args) {
		
		List<String> students = new ArrayList<>();
        students.add("Krishnan");
        students.add("Abishek");
        students.add("Arun");
        students.add("Vignesh");
        students.add("Kiruthiga");
        students.add("Murugan");
        students.add("Adhithya");
        students.add("Balaji");
        students.add("Vicky");
        students.add("Priya");

        for (String student : students) {
            if (student.startsWith("A")) {
                System.out.println(student);
            }
        }

	}

}
