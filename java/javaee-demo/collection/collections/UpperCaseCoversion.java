/*
Collections:
============
7.  8 districts are shown below
    Madurai, Coimbatore, Theni, Chennai, Karur, Salem, Erode, Trichy to be converted to UPPERCASE. 

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    8 districts are shown below
    Madurai, Coimbatore, Theni, Chennai, Karur, Salem, Erode, Trichy to be converted to UPPERCASE. 
2.Entity :
    UpperCaseCoversion 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Create a list and add given elements.
    2)Convert all the district name into upper case format.
5.Pseudocode :

public class UpperCaseCoversion {

	public static void main(String[] args) {
		
		List<String> districts = new ArrayList<>();
        ADD elements to the list.
        System.out.println(districts);
        districts.replaceAll(string -> string.toUpperCase());
        System.out.println(districts);

	}

}

*/

package collections;

import java.util.ArrayList;
import java.util.List;

public class UpperCaseCoversion {

	public static void main(String[] args) {
		
		List<String> districts = new ArrayList<>();
        districts.add("Madurai");
        districts.add("Coimbatore");
        districts.add("Theni");
        districts.add("Chennai");
        districts.add("Karur");
        districts.add("Salem");
        districts.add("Erode");
        districts.add("Trichy");
        System.out.println(districts);

        districts.replaceAll(string -> string.toUpperCase());

        System.out.println(districts);

	}

}
