/*
Collections:
============
4.use addFirst(),addLast(),removeFirst(),removeLast(),peekFirst(),peekLast(),pollFirst(),poolLast() 
methods to store and retrieve elements in ArrayDequeue.

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Use addFirst(),addLast(),removeFirst(),removeLast(),peekFirst(),peekLast(),pollFirst(),poolLast() 
methods to store and retrieve elements in ArrayDequeue.
2.Entity :
    ArrayDequeDemo 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Create a iplTeam double ended queue and add iplTeam names to it.
    2)Add a name at first.
    3)Add a name at last.
    4)Remove the first name at in queue.
    5)Remove the last name at in queue.
    6)Get the peek first iplTeam.
    7)Get the peek last iplTeam.
    8)Remove the first iplTeam name in laptops dequeue using poll.
    9)Remove the last iplTeam name in laptops dequeue using poll.
5.Pseudocode :

public class ArrayDequeDemo {

	public static void main(String[] args) {
		
		Deque<String> iplTeam = new ArrayDeque<>();
		iplTeam.add("CHENNAI SUPER KINGS");
		iplTeam.add("MUMBAI INDIANS");
		iplTeam.addFirst("DELHI CAPITALS");
        //adding element at first
		iplTeam.add("RAJASTHAN ROYALS");
		iplTeam.add("ROYAL CHALENGERS BANGALORE");
		iplTeam.addLast("KOLKATA KNIGHT RIDERS");
        //adding element at last
		iplTeam.add("KINGS XI PUNJAB");

        System.out.println(iplTeam + "\n");

        iplTeam.removeFirst();
        iplTeam.removeLast();
        //removing the first and last element in dequeue

        System.out.println(iplTeam + "\n");

        System.out.println("Peek first element: " + iplTeam.peekFirst() + "\n");
        //getting element present at first
        System.out.println("Peek last element: " + iplTeam.peekLast() + "\n");
        //getting element present at last

        System.out.println("poll first: " + iplTeam.pollFirst() + "\n");
        System.out.println("poll last: " + iplTeam.pollLast() + "\n");
        //removing first and last element
        System.out.println(iplTeam);

	}

}

*/

package collections;

import java.util.ArrayDeque;
import java.util.Deque;

public class ArrayDequeDemo {

	public static void main(String[] args) {
		
		Deque<String> iplTeam = new ArrayDeque<>();
		iplTeam.add("CHENNAI SUPER KINGS");
		iplTeam.add("MUMBAI INDIANS");
		iplTeam.addFirst("DELHI CAPITALS");
        //adding element at first
		iplTeam.add("RAJASTHAN ROYALS");
		iplTeam.add("ROYAL CHALENGERS BANGALORE");
		iplTeam.addLast("KOLKATA KNIGHT RIDERS");
        //adding element at last
		iplTeam.add("KINGS XI PUNJAB");

        System.out.println(iplTeam + "\n");

        iplTeam.removeFirst();
        iplTeam.removeLast();
        //removing the first and last element in dequeue

        System.out.println(iplTeam + "\n");

        System.out.println("Peek first element: " + iplTeam.peekFirst() + "\n");
        //getting element present at first
        System.out.println("Peek last element: " + iplTeam.peekLast() + "\n");
        //getting element present at last

        System.out.println("poll first: " + iplTeam.pollFirst() + "\n");
        System.out.println("poll last: " + iplTeam.pollLast() + "\n");
        //removing first and last element
        System.out.println(iplTeam);

	}

}
