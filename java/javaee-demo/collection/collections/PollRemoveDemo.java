package collections;

/*
Collections:
============
2.what is the difference between poll() and remove() method of queue interface?

Answer:
=======

 - we can simply use remove method for the specified or known size of queue.
 - poll() method is useful when the size of the queue is not judgeable.
 - when different thread accessing same queue, makes hard to judge.

 - If trying to pick element from empty queue using poll method returns 'null'
 - If remove() method is used it will throw an NoSuchElementException.

*/