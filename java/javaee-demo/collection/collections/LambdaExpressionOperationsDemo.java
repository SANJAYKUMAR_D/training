/*
Collections:
============
8.Addition,Substraction,Multiplication and Division concepts are achieved using 
Lambda expression and functional interface.

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Addition, Substraction, Multiplication and Division concepts are achieved using 
Lambda expression and functional interface.
2.Entity :
    LambdaExpressionOperationsDemo 
3.Function Declaration :
    public static void main(String[] args)
    public T calculate(Character symbol, T number1, T number2)
4.Jobs to be done :
    1)Print the Added two numbers
    2)Print the Subtracted two numbers
    3)Print the Multiplied two numbers
    4)Print the Divided two numbers
5.Pseudocode :

public class LambdaExpressionOperationsDemo {

	public static void main(String[] args) {
		
		Calculation<Integer> operation = (operator, number1, number2) -> {
            switch (operator) {
                case '*': 
                    return number1 * number2;
                case '/':
                    return number1 / number2;
                case '+':
                    return number1 + number2;
                case '-':
                    return number1 - number2;
                default:
                    return 0;
            }
        };

        System.out.println("Addition: " + operation.calculate('+', 20, 25));
        System.out.println("Subtraction: " + operation.calculate('-', 15, 5));
        System.out.println("Multiplication: " + operation.calculate('*', 25, 4));
        System.out.println("Division: " + operation.calculate('/', 50, 5));
        System.out.println("Unknown operator: " + operation.calculate('#', 40, 4));

	}

}

*/

package collections;

interface Calculation<T> {
    public T calculate(Character symbol, T number1, T number2);
}

public class LambdaExpressionOperationsDemo {

	public static void main(String[] args) {
		
		Calculation<Integer> operation = (operator, number1, number2) -> {
            switch (operator) {
                case '*': 
                    return number1 * number2;
                case '/':
                    return number1 / number2;
                case '+':
                    return number1 + number2;
                case '-':
                    return number1 - number2;
                default:
                    return 0;
            }
        };

        System.out.println("Addition: " + operation.calculate('+', 20, 25));
        System.out.println("Subtraction: " + operation.calculate('-', 15, 5));
        System.out.println("Multiplication: " + operation.calculate('*', 25, 4));
        System.out.println("Division: " + operation.calculate('/', 50, 5));
        System.out.println("Unknown operator: " + operation.calculate('#', 40, 4));

	}

}
