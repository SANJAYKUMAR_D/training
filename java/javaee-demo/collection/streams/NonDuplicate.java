/*
Streams:
========
7. Consider a following code snippet:
            List<Integer> randomNumbers = Array.asList({1, 6, 10, 1, 25, 78, 10, 25})
       - Get the non-duplicate values from the above list using java.util.Stream API
       
Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Get the non-duplicate values from the above list using java.util.Stream API.
2.Entity :
    NonDuplicate 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Create a list with random numbers.
    2)Filter the numbers by no repeations.
    3)Display the filtered elements.
5.Pseudocode :

public class NonDuplicate {

	public static void main(String[] args) {
		
		List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 1, 25, 78, 10, 25);
        randomNumbers.stream()
            .filter(number -> Collections.frequency(randomNumbers, number) == 1)
            .forEach(System.out::println); 

	}

}

*/

package streams;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class NonDuplicate {

	public static void main(String[] args) {
		
		List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 1, 25, 78, 10, 25);
        randomNumbers.stream()
            .filter(number -> Collections.frequency(randomNumbers, number) == 1)
            .forEach(System.out::println); 

	}

}
