/*
Streams:
========
10.Iterate the roster list in Persons class and and print the person without using forLoop/Stream 
      
Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Iterate the roster list in Persons class and and print the person without using 
forLoop/Stream 
2.Entity :
    PrintPersonIterate 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)To returns a list, Invoke the createRoster method using the class Person.
    2)Store the returned list to a new list persons.
    3)Create an Iterator object.
    4)Display all the person details by iterator object.
5.Pseudocode :

public class PrintPersonIterate {

	public static void main(String[] args) {
		
		List<Person> persons = Person.createRoster();
        Iterator<Person> iterator = persons.iterator();

        while (iterator.hasNext()) {
            iterator.next().printPerson();
        }

	}

}
*/

package streams;

import java.util.Iterator;
import java.util.List;

public class PrintPersonIterate {

	public static void main(String[] args) {
		
		List<Person> persons = Person.createRoster();
        Iterator<Person> iterator = persons.iterator();

        while (iterator.hasNext()) {
            iterator.next().printPerson();
        }

	}

}
