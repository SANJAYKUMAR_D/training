/*
Streams:
========
14. Consider the following code snippet:
            List<Person> newRoster = new ArrayList<>();
            newRoster.add(
                new Person(
                "John",
                IsoChronology.INSTANCE.date(1980, 6, 20),
                Person.Sex.MALE,
                "john@example.com"));
            newRoster.add(
                new Person(
                "Jade",
                IsoChronology.INSTANCE.date(1990, 7, 15),
                Person.Sex.FEMALE, "jade@example.com"));
            newRoster.add(
                new Person(
                "Donald",
                IsoChronology.INSTANCE.date(1991, 8, 13),
                Person.Sex.MALE, "donald@example.com"));
            newRoster.add(
                new Person(
                "Bob",
                IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com"));
       - Remove the only person who are in the newRoster from the roster list.
       - Remove the following person from the roster List:
            new Person(
                "Bob",
                IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com"));
      
Work Breakdown Structure(WBS):
==============================
1.Requirement :
    - Remove the only person who are in the newRoster from the roster list.
    - Remove the following person from the roster List:
            new Person(
                "Bob",
                IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com"));
2.Entity :
    RemovePerson 
3.Function Declaration :
    public static void main(String[] args)
    public static int contains(List<Person> list, Person o)
4.Jobs to be done :
    1)To returns a list, Invoke the createRoster method using the class Person.
    2)Store the returned list to a new list persons.
    3)Create a new list and store some Person objects
    4)Check if the person present in list
        4.1)If present remove the person from the list
    5)Check the particular Person present in the list
        5.1)If present remove the person from the list
5.Pseudocode :

public class RemovePerson {

	public static int contains(List<Person> list, Person o) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).birthday.equals(o.birthday) &&
                    list.get(i).emailAddress.equals(o.emailAddress) &&
                    list.get(i).gender.equals(o.gender) &&
                    list.get(i).name.equals(o.name)) {
                return i;
            }
        }
        return -1;
    }
	
	public static void main(String[] args) {
		
		List<Person> roster = Person.createRoster();

        List<Person> newRoster = new ArrayList<>();
        ADD values to the list

        roster.forEach(Person::printPerson);

        Person.createRoster().forEach(person -> {
            int index = contains(newRoster, person);
            if (index >= 0) roster.remove(index);
        });
        System.out.println();

        roster.forEach(Person::printPerson);

        Person aPerson = new Person(
              "Bob",
              IsoChronology.INSTANCE.date(2000, 9, 12),
              Person.Sex.MALE, "bob@example.com");

        int index = contains(roster, aPerson);
        if (index >= 0) {
            roster.remove(index);
            System.out.println(roster.get(index).name +
                    " removed from list");
        } else System.out.println("The person not found in list");

	}

}

*/

package streams;

import java.time.chrono.IsoChronology;
import java.util.ArrayList;
import java.util.List;

public class RemovePerson {

	public static int contains(List<Person> list, Person o) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).birthday.equals(o.birthday) &&
                    list.get(i).emailAddress.equals(o.emailAddress) &&
                    list.get(i).gender.equals(o.gender) &&
                    list.get(i).name.equals(o.name)) {
                return i;
            }
        }
        return -1;
    }
	
	public static void main(String[] args) {
		
		List<Person> roster = Person.createRoster();

        List<Person> newRoster = new ArrayList<>();
        newRoster.add(
            new Person(
            "John",
            IsoChronology.INSTANCE.date(1980, 6, 20),
            Person.Sex.MALE,
            "john@example.com"));
        newRoster.add(
            new Person(
            "Jade",
            IsoChronology.INSTANCE.date(1990, 7, 15),
            Person.Sex.FEMALE, "jade@example.com"));
        newRoster.add(
            new Person(
            "Donald",
            IsoChronology.INSTANCE.date(1991, 8, 13),
            Person.Sex.MALE, "donald@example.com"));
        newRoster.add(
            new Person(
            "Bob",
            IsoChronology.INSTANCE.date(2000, 9, 12),
            Person.Sex.MALE, "bob@example.com"));

        roster.forEach(Person::printPerson);

        Person.createRoster().forEach(person -> {
            int index = contains(newRoster, person);
            if (index >= 0) roster.remove(index);
        });
        System.out.println();

        roster.forEach(Person::printPerson);

        Person aPerson = new Person(
              "Bob",
              IsoChronology.INSTANCE.date(2000, 9, 12),
              Person.Sex.MALE, "bob@example.com");

        int index = contains(roster, aPerson);
        if (index >= 0) {
            roster.remove(index);
            System.out.println(roster.get(index).name +
                    " removed from list");
        } else System.out.println("The person not found in list");

	}

}
