/*
Streams:
========
5. Write a program to collect the minimal person with name and email address from the Person class using java.util.Stream<T> API as List
       
Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Program to collect the minimal person with name and email address from the Person class using java.util.Stream<T> API as List
2.Entity :
    MinimalPersonList 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)To returns a list, Invoke the createRoster method using the class Person.
    2)Store the returned list to a new list persons.
    3)Collecting the emails of persons list using map method and convert to list
    4)Check each person's email matches with the list
        4.1)if it present
            4.1.1)Display the name of person with email address.
5.Pseudocode :

public class MinimalPersonDemo {

	public static void main(String[] args) {
		
		List<Person> persons = Person.createRoster();

        List<String> emails = persons.stream()
            .map(Person::getEmailAddress)
            .collect(Collectors.toList());
        //collecting the emails from persons list using pipelined
        //methods of stream API

        for (int emailIndex = 0; emailIndex < emails.size(); emailIndex++) {
            for (int personIndex = 0; personIndex < persons.size(); personIndex++) {
                if (persons.get(personIndex).emailAddress == emails.get(emailIndex)) {
                    System.out.println("Person Name: " +
                        persons.get(personIndex).name + ", mailId: " +
                        emails.get(emailIndex));
                }
            }
        }

	}

}

*/

package streams;

import java.util.List;
import java.util.stream.Collectors;

public class MinimalPersonList {

	public static void main(String[] args) {
		
		List<Person> persons = Person.createRoster();

        List<String> emails = persons.stream()
            .map(Person::getEmailAddress)
            .collect(Collectors.toList());
        //collecting the emails from persons list using pipelined
        //methods of stream API

        for (int emailIndex = 0; emailIndex < emails.size(); emailIndex++) {
            for (int personIndex = 0; personIndex < persons.size(); personIndex++) {
                if (persons.get(personIndex).emailAddress == emails.get(emailIndex)) {
                    System.out.println("Person Name: " +
                        persons.get(personIndex).name + ", mailId: " +
                        emails.get(emailIndex));
                }
            }
        }

	}

}
