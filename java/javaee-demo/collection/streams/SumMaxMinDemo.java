/*
Streams:
========
4.  Consider a following code snippet:
            List<Integer> randomNumbers = Array.asList({1, 6, 10, 25, 78})
        - Find the sum of all the numbers in the list using java.util.Stream API
        - Find the maximum of all the numbers in the list using java.util.Stream API
        - Find the minimum of all the numbers in the list using java.util.Stream API
       
Work Breakdown Structure(WBS):
==============================
1.Requirement :
    - Find the sum of all the numbers in the list using java.util.Stream API
    - Find the maximum of all the numbers in the list using java.util.Stream API
    - Find the minimum of all the numbers in the list using java.util.Stream API
2.Entity :
    SumMaxMinDemo 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Create a list with random numbers.
    2)Get stream of objects and Collect stream of objects.
    3)Sum all the collected integers using summingInt of Comparator.
    4)Get minimum number from collected numbers using getMin method.
    5)Get maximum number from collected numbers using getMax method.
5.Pseudocode :

public class SumMaxMinDemo {

	public static void main(String[] args) {
		
		List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 25, 78);

        int sum = randomNumbers.stream()
                .collect(Collectors.summingInt(Integer::intValue));
        //summingInt method helps to sum all the integer values

        int maxList = randomNumbers.stream()
                .collect(Collectors.summarizingInt(Integer::intValue)).getMax();
        //summarizingInt method accepts integer values

        int minList = randomNumbers.stream()
                .collect(Collectors.summarizingInt(Integer::intValue)).getMin();
        //getMin method returns minimum element from collected integers

        System.out.println("Sum: " + sum);
        System.out.println("Maximum in list: " + maxList);
        System.out.println("Minimum in list: " + minList);

	}

}
*/

package streams;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SumMaxMinDemo {

	public static void main(String[] args) {
		
		List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 25, 78);

        int sum = randomNumbers.stream()
                .collect(Collectors.summingInt(Integer::intValue));
        //summingInt method helps to sum all the integer values

        int maxList = randomNumbers.stream()
                .collect(Collectors.summarizingInt(Integer::intValue)).getMax();
        //summarizingInt method accepts integer values

        int minList = randomNumbers.stream()
                .collect(Collectors.summarizingInt(Integer::intValue)).getMin();
        //getMin method returns minimum element from collected integers

        System.out.println("Sum: " + sum);
        System.out.println("Maximum in list: " + maxList);
        System.out.println("Minimum in list: " + minList);

	}

}
