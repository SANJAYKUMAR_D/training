/*
Streams:
========
3. Write a program to filter the Person, who are male and
        - find the first person from the filtered persons
        - find the last person from the filtered persons
        - find random person from the filtered persons
      
Work Breakdown Structure(WBS):
==============================
1.Requirement :
    program to filter the Person, who are male and
    - find the first person from the filtered persons
    - find the last person from the filtered persons
    - find random person from the filtered persons
2.Entity :
    FilterPersonGender 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)To returns a list, Invoke the createRoster method using the class Person.
    2)Store the returned list to a new list persons.
    3)Create a Stream of persons objects
    4)Filter each person by person's gender is male
    5)Store the filtered persons to the list
    6)Get first person, last person,and random person from the filtered persons.
5.Pseudocode :

public class FilterPersonGender {

	public static void main(String[] args) {
		
		List<Person> persons = Person.createRoster();

        List<Person> filteredPersons = persons.stream()
                .filter(person -> person.gender == Person.Sex.MALE)
                .collect(Collectors.toList());
        //Using Stream API filter get the persons who are male.

        filteredPersons.get(0).printPerson();
        //Getting first person in the list

        filteredPersons.get(filteredPersons.size() - 1).printPerson();
        //Getting last person in the list

        Random random = new Random();
        filteredPersons.get(random.nextInt(filteredPersons.size())).printPerson();

	}

}

*/

package streams;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class FilterPersonGender {

	public static void main(String[] args) {
		
		List<Person> persons = Person.createRoster();

        List<Person> filteredPersons = persons.stream()
                .filter(person -> person.gender == Person.Sex.MALE)
                .collect(Collectors.toList());
        //Using Stream API filter get the persons who are male.

        filteredPersons.get(0).printPerson();
        //Getting first person in the list

        filteredPersons.get(filteredPersons.size() - 1).printPerson();
        //Getting last person in the list

        Random random = new Random();
        filteredPersons.get(random.nextInt(filteredPersons.size())).printPerson();

	}

}
