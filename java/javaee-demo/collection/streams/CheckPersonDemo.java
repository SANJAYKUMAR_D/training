/*
Streams:
========
13. Consider the following Person:
            new Person(
                "Bob",
                IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com"));
       - Check if the above person is in the roster list obtained from Person class.
       
Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Check if the above person is in the roster list obtained from Person class.
2.Entity :
    CheckPersonDemo 
3.Function Declaration :
    public static void main(String[] args)
    public static boolean contains(List<Person> persons, Person o)
4.Jobs to be done :
    1)To returns a list, Invoke the createRoster method using the class Person.
    2)Store the returned list to a list roster
    3)Store a object of Person to a variable of that type
    4)Check the object stored on a variable is present in a list
        4.1)If its present display as true
        4.2)Otherwise display as false
5.Pseudocode :

public class CheckPersonDemo {

	public static boolean contains(List<Person> persons, Person o) {
        for (Person person : persons) {
            if (person.name.equals(o.name) &&
                    person.gender.equals(o.gender) &&
                    person.emailAddress.equals(o.emailAddress) &&
                    person.birthday.equals(o.birthday)) {
                return true;
            }
        }
        return false;
    }
	
	public static void main(String[] args) {
		
		List<Person> roster = Person.createRoster();
        Person isPerson = new Person(
                             "Bob",
                             IsoChronology.INSTANCE.date(2000, 9, 12),
                             Person.Sex.MALE, "bob@example.com");

        System.out.println("Check person is in the roster list: " + contains(roster, isPerson));

	}

}

*/

package streams;

import java.time.chrono.IsoChronology;
import java.util.List;

public class CheckPersonDemo {

	public static boolean contains(List<Person> persons, Person o) {
        for (Person person : persons) {
            if (person.name.equals(o.name) &&
                    person.gender.equals(o.gender) &&
                    person.emailAddress.equals(o.emailAddress) &&
                    person.birthday.equals(o.birthday)) {
                return true;
            }
        }
        return false;
    }
	
	public static void main(String[] args) {
		
		List<Person> roster = Person.createRoster();
        Person isPerson = new Person(
                             "Bob",
                             IsoChronology.INSTANCE.date(2000, 9, 12),
                             Person.Sex.MALE, "bob@example.com");

        System.out.println("Check person is in the roster list: " + contains(roster, isPerson));

	}

}
