/*
Streams:
========
9) Sort the roster list based on the person's age in descending order using comparator
      
Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Sort the roster list based on the person's age in descending order using comparator
2.Entity :
    SortUsingComparator 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)To returns a list, Invoke the createRoster method using the class Person.
    2)Store the returned list to a new list persons.
    3)Sort the list based on the person age in descending order.
    4)Display the sorted person's details.
5.Pseudocode :

public class SortUsingComparator {

	public static void main(String[] args) {
		
		List<Person> persons = Person.createRoster();
        persons.sort(Comparator.comparing(Person::getAge).reversed());
        
        for (Person person : persons) {
            person.printPerson();
        }

	}

}
*/

package streams;

import java.util.Comparator;
import java.util.List;

public class SortUsingComparator {

	public static void main(String[] args) {
		
		List<Person> persons = Person.createRoster();
        persons.sort(Comparator.comparing(Person::getAge).reversed());
        
        for (Person person : persons) {
            person.printPerson();
        }

	}

}
