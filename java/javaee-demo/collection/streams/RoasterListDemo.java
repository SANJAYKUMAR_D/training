/*
Streams:
========
12. Consider the following code snippet:
            List<Person> newRoster = new ArrayList<>();
            newRoster.add(
                new Person(
                "John",
                IsoChronology.INSTANCE.date(1980, 6, 20),
                Person.Sex.MALE,
                "john@example.com"));
            newRoster.add(
                new Person(
                "Jade",
                IsoChronology.INSTANCE.date(1990, 7, 15),
                Person.Sex.FEMALE, "jade@example.com"));
            newRoster.add(
                new Person(
                "Donald",
                IsoChronology.INSTANCE.date(1991, 8, 13),
                Person.Sex.MALE, "donald@example.com"));
            newRoster.add(
                new Person(
                "Bob",
                IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com"));
        - Create the roster from the Person class and add each person in the newRoster to the existing 
          list and print the new roster List.
        - Print the number of persons in roster List after the above addition.
        - Remove the all the person in the roster list
       
Work Breakdown Structure(WBS):
==============================
1.Requirement :
    - Create the roster from the Person class and add each person in the newRoster to the existing list 
      and print the new roster List.
    - Print the number of persons in roster List after the above addition.
    - Remove the all the person in the roster list
2.Entity :
    RoasterListDemo 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)To returns a list, Invoke the createRoster method using the class Person.
    2)Store the returned list to a list.
    3)Creating a newRoster list and add new Person objects to it.
    4)Add the objects of newRoster list to the list.
    5)Display the details of each person
    6)Display the size of the list
    7)Clear all the elements in list.
5.Pseudocode :

public class RoasterListDemo {

	public static void main(String[] args) {
		
		List<Person> roster = Person.createRoster();
        List<Person> newRoster = new ArrayList<>();
        newRoster.add(
            new Person(
            "John",
            IsoChronology.INSTANCE.date(1980, 6, 20),
            Person.Sex.MALE,
            "john@example.com"));
        newRoster.add(
            new Person(
            "Jade",
            IsoChronology.INSTANCE.date(1990, 7, 15),
            Person.Sex.FEMALE, "jade@example.com"));
        newRoster.add(
            new Person(
            "Donald",
            IsoChronology.INSTANCE.date(1991, 8, 13),
            Person.Sex.MALE, "donald@example.com"));
        newRoster.add(
            new Person(
            "Bob",
            IsoChronology.INSTANCE.date(2000, 9, 12),
            Person.Sex.MALE, "bob@example.com"));

        newRoster.forEach(roster::add);
        
        newRoster.forEach(Person::printPerson);

        System.out.println("Number of persons in roster list: " + roster.size());

        roster.clear();

        System.out.println("Number of persons in roster list: " + roster.size());

	}

}
*/

package streams;

import java.time.chrono.IsoChronology;
import java.util.ArrayList;
import java.util.List;

public class RoasterListDemo {

	public static void main(String[] args) {
		
		List<Person> roster = Person.createRoster();
        List<Person> newRoster = new ArrayList<>();
        newRoster.add(
            new Person(
            "John",
            IsoChronology.INSTANCE.date(1980, 6, 20),
            Person.Sex.MALE,
            "john@example.com"));
        newRoster.add(
            new Person(
            "Jade",
            IsoChronology.INSTANCE.date(1990, 7, 15),
            Person.Sex.FEMALE, "jade@example.com"));
        newRoster.add(
            new Person(
            "Donald",
            IsoChronology.INSTANCE.date(1991, 8, 13),
            Person.Sex.MALE, "donald@example.com"));
        newRoster.add(
            new Person(
            "Bob",
            IsoChronology.INSTANCE.date(2000, 9, 12),
            Person.Sex.MALE, "bob@example.com"));

        newRoster.forEach(roster::add);
        
        newRoster.forEach(Person::printPerson);

        System.out.println("Number of persons in roster list: " + roster.size());

        roster.clear();

        System.out.println("Number of persons in roster list: " + roster.size());

	}

}
