/*
Streams:
========
11. Sort the roster list based on the person's age in descending order using
java.util.Stream
      
Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Sort the roster list based on the person's age in descending order using
java.util.Stream
2.Entity :
    SortUsingStream 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)To returns a list, Invoke the createRoster method using the class Person.
    2)Store the returned list to a new list persons.
    3)Sort the list based on the person age in descending order using Stream class
    4)Display the sorted person's details.
5.Pseudocode :

public class SortUsingStream {

	public static void main(String[] args) {
		
		List<Person> persons = Person.createRoster();
        persons.stream().sorted(Person::compareByAge)
            .forEach(Person::printPerson);

	}

}

*/

package streams;

import java.util.List;

public class SortUsingStream {

	public static void main(String[] args) {
		
		List<Person> persons = Person.createRoster();
        persons.stream().sorted(Person::compareByAge)
            .forEach(Person::printPerson);

	}

}
