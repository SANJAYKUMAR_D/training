/*
Streams:
========
2. Write a program to print minimal person with name and email address from the Person class using java.util.Stream<T>#map API
      
Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Program to print minimal person with name and email address from the Person class using java.util.Stream<T>#map API
2.Entity :
    MinimalPersonMap 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)To returns a list, Invoke the createRoster method using the class Person.
    2)Store the returned list to a new list persons.
    3)Create a Stream and Collect the stream of objects.
    4)Map the name and email of collected persons and store it to a Map variable.
    5)Display all the mapped data.
5.Pseudocode :

public class MinimalPersonMap {

	public static void main(String[] args) {
		
		List<Person> persons = Person.createRoster();

        Map<String, String> personMap = persons.stream()
                .collect(Collectors.toMap(Person::getEmailAddress, Person::getName));
        //stream method returns stream of objects
        //collect() method collects the stream of data(object)
        //Using streams Collector to map all the persons email and their name

        personMap.entrySet().forEach(System.out::println);

	}

}
*/

package streams;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MinimalPersonMap {

	public static void main(String[] args) {
		
		List<Person> persons = Person.createRoster();

        Map<String, String> personMap = persons.stream()
                .collect(Collectors.toMap(Person::getEmailAddress, Person::getName));
        //stream method returns stream of objects
        //collect() method collects the stream of data(object)
        //Using streams Collector to map all the persons email and their name

        personMap.entrySet().forEach(System.out::println);

	}

}
