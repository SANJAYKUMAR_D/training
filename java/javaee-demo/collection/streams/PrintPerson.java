/*
Streams:
========
8. Print all the persons in the roster using java.util.Stream<T>#forEach
      
Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Print all the persons in the roster using java.util.Stream<T>#forEach
2.Entity :
    PrintPerson 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)To returns a list, Invoke the createRoster method using the class Person.
    2)Store the returned list to a new list persons.
    3)For each person in persons list
         3.1)Display the person detail using printPerson method.
5.Pseudocode :

public class PrintPerson {

	public static void main(String[] args) {
		
		List<Person> persons = Person.createRoster();
        persons.forEach(Person::printPerson);

	}

}
*/

package streams;

import java.util.List;

public class PrintPerson {

	public static void main(String[] args) {
		
		List<Person> persons = Person.createRoster();
        persons.forEach(Person::printPerson);

	}

}
