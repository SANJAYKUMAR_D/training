/*
Streams:
========
1. Write a program to filter the Person, who are male and age greater than 21
       
Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Program to filter the Person, who are male and age greater than 21.
2.Entity :
    FilterAgeAndGender 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)To returns a list, Invoke the createRoster method using the class Person.
    2)Store the returned list to the variable
    3)Filter the person by if
        3.1)The person gender is male and The person age is greater than 21.
    4)Display the filtered persons using forEach method of Streams.
5.Pseudocode :

public class FilterAgeAndGender {

	public static void main(String[] args) {
		
		List<Person> persons = Person.createRoster();

        List<Person> filteredPersons = persons.stream()
            .filter(person -> person.gender == Person.Sex.MALE)
            .filter(person -> person.getAge() > 21)
            .collect(Collectors.toList());
        //filtering their their gender and age

        filteredPersons.forEach(Person::printPerson);

	}

}
*/

package streams;

import java.util.List;
import java.util.stream.Collectors;

public class FilterAgeAndGender {

	public static void main(String[] args) {
		
		List<Person> persons = Person.createRoster();

        List<Person> filteredPersons = persons.stream()
            .filter(person -> person.gender == Person.Sex.MALE)
            .filter(person -> person.getAge() > 21)
            .collect(Collectors.toList());
        //filtering their their gender and age

        filteredPersons.forEach(Person::printPerson);

	}

}
