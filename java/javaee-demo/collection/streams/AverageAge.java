/*
Streams:
========
6. Write a program to find the average age of all the Person in the person List

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Program to find the average age of all the Person in the person List
2.Entity :
    AverageAge 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Invoke the createRoster method using the Person class, the method returns a list
    2)Store the returned list to a new list persons
    3)Get the age of persons in persons list using mapToDouble to
get age of each person
    4)average() method used to find the average.
5.Pseudocode :

public class AverageAge {

	public static void main(String[] args) {
		
		//Stream method used to pipeline various methods
        List<Person> persons = Person.createRoster();
        double age = persons.stream()
        		               .mapToDouble(Person::getAge)
        		               .average() //average() method calculates the average for sequential elements
        		               .getAsDouble(); //getAsDouble() method converts and return Optional double to double       
        System.out.println("Average age: " + age);

	}

}
*/

package streams;

import java.util.List;

public class AverageAge {

	public static void main(String[] args) {
		
		//Stream method used to pipeline various methods
        List<Person> persons = Person.createRoster();
        double age = persons.stream()
        		               .mapToDouble(Person::getAge)
        		               .average() //average() method calculates the average for sequential elements
        		               .getAsDouble(); //getAsDouble() method converts and return Optional double to double       
        System.out.println("Average age: " + age);

	}

}
