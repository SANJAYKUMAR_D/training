/*
Date and Time:
==============
2. Find the time difference using timer class ?
   
Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Find the time difference using timer class
2.Entity :
    TimeDifferenceDemo 
3.Function Declaration :
    public static void main(String[] args)
    public void start()
    public void end()
4.Jobs to be done :
    1)Invoke start() method in a class.
    2)Get the end time using end() method.
    3)Display the delayed time by difference of 2 times.
5.Pseudocode :

public class TimeDifferenceDemo {

	private long startTime;
    private long endTime;

    public void start() {
        startTime = System.currentTimeMillis();
        System.out.println("Timer started...");
    }

    public void end() {
        endTime = System.currentTimeMillis();
        System.out.println((endTime - startTime)/1000 + " secs");
        System.out.println("Timer stopped");
    }
    
	public static void main(String[] args) throws InterruptedException {
		
		TimeDifferenceDemo timer = new TimeDifferenceDemo();

	    timer.start();
        Thread.sleep(5000);
        timer.end();

	}

}
*/

public class TimeDifferenceDemo {

	private long startTime;
    private long endTime;

    public void start() {
        startTime = System.currentTimeMillis();
        System.out.println("Timer started..");
    }

    public void end() {
        endTime = System.currentTimeMillis();
        System.out.println((endTime - startTime)/1000 + " secs");
        System.out.println("Timer stopped");
    }
    
	public static void main(String[] args) throws InterruptedException {
		
		TimeDifferenceDemo timer = new TimeDifferenceDemo();

	    timer.start();
        Thread.sleep(5000);
        timer.end();

	}

}