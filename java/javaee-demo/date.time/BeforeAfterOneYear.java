/*
Date and Time:
==============
10.Write a Java program to get a date before and after 1 year compares to the current date. 

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Program to get a date before and after 1 year compares to the current date.
2.Entity :
    BeforeAfterOneYear 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Invoke Calendar class getInstance method and store in calendar.
    2)Invoke Calendar class getTime method and store date in date variable.
    3)Using add method add one year to Calender
        3.1)Get the nextYear using Calender class getTime method.
    4)Using add method subract one year to Calender
        4.1)Get the previousYear using Calender class getTime method.
    5)Display the current date, nextYear and previousYear.
5.Pseudocode :

public class BeforeAfterOneYear {

	public static void main(String[] args) {
		
        Calendar calendar = Calendar.getInstance();

        Date currentDate = calendar.getTime();

	    // next year
	    calendar.add(Calendar.YEAR, 1); 
	    Date dateAfterOneYear = calendar.getTime();

	    // previous year
	    calendar.add(Calendar.YEAR, -2); 
	    Date dateBeforeOneYear = calendar.getTime();
	    System.out.println("Current Date : " + currentDate);
	    System.out.println("Date before 1 year : " + dateBeforeOneYear);
	    System.out.println("Date after 1 year  : " + dateAfterOneYear);

	}

}
*/

import java.util.Calendar;
import java.util.Date;

public class BeforeAfterOneYear {

	public static void main(String[] args) {
		
        Calendar calendar = Calendar.getInstance();

        Date currentDate = calendar.getTime();

	    // next year
	    calendar.add(Calendar.YEAR, 1); 
	    Date dateAfterOneYear = calendar.getTime();

	    // previous year
	    calendar.add(Calendar.YEAR, -2); 
	    Date dateBeforeOneYear = calendar.getTime();
	    System.out.println("Current Date : " + currentDate);
	    System.out.println("Date before 1 year : " + dateBeforeOneYear);
	    System.out.println("Date after 1 year  : " + dateAfterOneYear);

	}

}
