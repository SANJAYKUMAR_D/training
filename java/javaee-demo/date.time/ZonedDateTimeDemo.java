/*
Date and Time:
==============
12. write a Java program to convert ZonedDateTime to Date.
   
Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Program to convert ZonedDateTime to Date.
2.Entity :
    ZonedDateTimeDemo 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Get current time using now() method of ZonedDateTime.
    2)Get date from instant object of Instant class.
    3)Display the date value.
5.Pseudocode :
public class ZonedDateTimeDemo {

	public static void main(String[] args) {
		
		ZonedDateTime zdt = ZonedDateTime.now();
        //Convert ZonedDateTime to Instant instance
        Instant instant = zdt.toInstant();
        Date date = Date.from(instant);
        //Display Date value

	}

}
*/

import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.Date;

public class ZonedDateTimeDemo {

	public static void main(String[] args) {
		
		ZonedDateTime zdt = ZonedDateTime.now();

        //Convert ZonedDateTime to Instant instance
        Instant instant = zdt.toInstant();

        Date date = Date.from(instant);

        //Display Date value
        System.out.println(date);

	}

}