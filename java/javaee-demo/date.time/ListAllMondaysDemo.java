/*
Date and Time:
==============
11. Write a Java program for a given month of the current year, lists all of the Mondays in that month.

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Program for a given month of the current year, lists all of the Mondays in that month.
2.Entity :
    ListAllMondaysDemo 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Get the month of current year from user.
    2)Get the first monday in the month using TemporalAdjusters and store it in
the object date of LocalDate.
    3)Get the all dates of month.
    4)checking date of month is matches with a given month
        4.1)Display the date if lies in monday.
5.Pseudocode :

public class ListAllMondaysDemo {

	public static void main(String[] args) {
		
		System.out.println("Enter the month:");
        @SuppressWarnings("resource")
        Scanner scanner = new Scanner(System.in);
        String monthInput = scanner.nextLine().toUpperCase();
        Month month = Month.valueOf(monthInput);

        LocalDate date = Year.now().atMonth(month).atDay(1)
                .with(TemporalAdjusters.firstInMonth(DayOfWeek.MONDAY));
        Month dateOfMonth = date.getMonth();

        while (month == dateOfMonth) {
            System.out.println(date);
            date = date.with(TemporalAdjusters.next(DayOfWeek.MONDAY));
            dateOfMonth = date.getMonth();
        }

	}

}
*/

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.time.temporal.TemporalAdjusters;
import java.util.Scanner;

public class ListAllMondaysDemo {

	public static void main(String[] args) {
		
		System.out.println("Enter the month:");
        @SuppressWarnings("resource")
        Scanner scanner = new Scanner(System.in);
        String monthInput = scanner.nextLine().toUpperCase();
        Month month = Month.valueOf(monthInput);

        LocalDate date = Year.now().atMonth(month).atDay(1)
                .with(TemporalAdjusters.firstInMonth(DayOfWeek.MONDAY));
        Month dateOfMonth = date.getMonth();

        while (month == dateOfMonth) {
            System.out.println(date);
            date = date.with(TemporalAdjusters.next(DayOfWeek.MONDAY));
            dateOfMonth = date.getMonth();
        }

	}

}
