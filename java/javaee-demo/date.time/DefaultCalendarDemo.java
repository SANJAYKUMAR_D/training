/*
Date and Time:
==============
7. Write a Java program to get and display information
(year, month, day, hour, minute) of a default calendar 

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Program to get and display information
(year, month, day, hour, minute) of a default calendar.
2.Entity :
    DefaultCalendarDemo 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Invoke Calendar class with getInstance method()
    2)Display current year, month, day, hour, minute by using get() method.
5.Pseudocode :

public class DefaultCalendarDemo {

	public static void main(String[] args) {
		
		Calendar calendar = Calendar.getInstance();

        //Display information of current year, month, date, hours, minutes.
        System.out.println(
            "Year: " + calendar.get(Calendar.YEAR) + "\n" +
            "Month: " + calendar.get(Calendar.MONTH) + "\n" +
            "Date: " + calendar.get(Calendar.DATE) + "\n" +
            "Hours: " + calendar.get(Calendar.HOUR) + "\n" +
            "Minutes: " + calendar.get(Calendar.MINUTE));

	}

}
*/

import java.util.Calendar;

public class DefaultCalendarDemo {

	public static void main(String[] args) {
		
		Calendar calendar = Calendar.getInstance();

        //Display information of current year, month, date, hours, minutes, seconds.s
        System.out.println(
            "Year: " + calendar.get(Calendar.YEAR) + "\n" +
            "Month: " + calendar.get(Calendar.MONTH) + "\n" +
            "Date: " + calendar.get(Calendar.DATE) + "\n" +
            "Hours: " + calendar.get(Calendar.HOUR) + "\n" +
            "Minutes: " + calendar.get(Calendar.MINUTE));

	}

}
