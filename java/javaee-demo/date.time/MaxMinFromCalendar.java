/*
Date and Time:
==============
8. Write a Java program to get the maximum and minimum value
   of the year, month, week, date from the current date of a default calendar.
Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Program to get the maximum and minimum value of the year, month, week, date 
from the current date of a default calendar.
2.Entity :
    MaxMinFromCalendar 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Invoke Calendar class.
    2)Display the minimum year, month, week of month and date using getActualMinimum method.
    3)display the maximum year, month, week of month and date using getActualMaximum method.
5.Pseudocode :

public class MaxMinFromCalendar {

	public static void main(String[] args) {
		
		Calendar calendar = Calendar.getInstance();

        int minYear = calendar.getActualMinimum(Calendar.YEAR);
        int minMonth = calendar.getActualMinimum(Calendar.MONTH);
        int minWeek = calendar.getActualMinimum(Calendar.WEEK_OF_MONTH);
        int minDate = calendar.getActualMinimum(Calendar.DATE);
        
        Display minimum year, month, week, and date.

        int maxYear = calendar.getActualMaximum(Calendar.YEAR);
        int maxMonth = calendar.getActualMaximum(Calendar.MONTH);
        int maxWeek = calendar.getActualMaximum(Calendar.WEEK_OF_YEAR);
        int maxDate = calendar.getActualMaximum(Calendar.DATE);

        Display maximum year, month, week, and date.

	}

}
*/

import java.util.Calendar;

public class MaxMinFromCalendar {

	public static void main(String[] args) {
		
		Calendar calendar = Calendar.getInstance();

        System.out.println("Minimum values:");

        int minYear = calendar.getActualMinimum(Calendar.YEAR);
        int minMonth = calendar.getActualMinimum(Calendar.MONTH);
        int minWeek = calendar.getActualMinimum(Calendar.WEEK_OF_MONTH);
        int minDate = calendar.getActualMinimum(Calendar.DATE);
        
        System.out.println("Year: " + minYear);
        System.out.println("Month: " + minMonth);
        System.out.println("Week: " + minWeek);
        System.out.println("Date: " + minDate);
 
        System.out.println("\n" + "Maximum values:");

        int maxYear = calendar.getActualMaximum(Calendar.YEAR);
        int maxMonth = calendar.getActualMaximum(Calendar.MONTH);
        int maxWeek = calendar.getActualMaximum(Calendar.WEEK_OF_YEAR);
        int maxDate = calendar.getActualMaximum(Calendar.DATE);

        System.out.println("Year: " + maxYear);
        System.out.println("Month: " + maxMonth);
        System.out.println("Week: " + maxWeek);
        System.out.println("Date: " + maxDate);

	}

}
