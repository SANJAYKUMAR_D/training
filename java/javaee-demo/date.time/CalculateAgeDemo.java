/*
Date and Time:
==============
4. Write a Java program to calculate your age  

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Program to calculate your age.
2.Entity :
    CalculateAgeDemo 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Invoke the LocalDate class and pass date of birth in of LocalDate class method.
    2)Invoke the LocalDate class and get current time using now method.
    3)Invoke Period class and pass dateOfBirth, present date.
    4)Get the age using Period object with getYears(), getMonths() and getDays() methods
5.Pseudocode :

public class CalculateAgeDemo {

	public static void main(String[] args) {
		
		LocalDate dob = LocalDate.parse("2000-11-08");

        LocalDate now = LocalDate.now();
        //difference between present date and date of birth
        Period ageDifference = Period.between(dob, now);

        System.out.format("%d years, %d months and %d days old."
                , ageDifference.getYears()
                , ageDifference.getMonths()
                , ageDifference.getDays());

	}

}

*/

import java.time.LocalDate;
import java.time.Period;

public class CalculateAgeDemo {

	public static void main(String[] args) {
		
		LocalDate dob = LocalDate.parse("2000-11-08");

        LocalDate now = LocalDate.now();
        //difference between present date and date of birth
        Period ageDifference = Period.between(dob, now);

        System.out.format("%d years, %d months and %d days old."
                , ageDifference.getYears()
                , ageDifference.getMonths()
                , ageDifference.getDays());

	}

}
