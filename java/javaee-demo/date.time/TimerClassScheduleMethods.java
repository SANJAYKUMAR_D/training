/*
Date and Time:
==============
3. write a Java program to demonstrate schedule method calls of Timer class ?
   
Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Program to demonstrate schedule method calls of Timer class.
2.Entity :
    TimerClassScheduleMethods 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Invoke TimerTask class.
    2)Create a task to complete by timer.
    3)Invoke Timer class.
    4)Schedule the task with delay using schedule() method in Timer class.
5.Pseudocode :

public class TimerClassScheduleMethods {

	public static Timer timer;
	
	public static void main(String[] args) {
		
		TimerTask firstTask = new TimerTask() {

            int count = 1;

            @Override
            public void run() {
                test();
            }

            private void test() {
                if (count == 4) {
                    timer.cancel();
                }
                System.out.println(count++);
            }

        };

        Timer timer = new Timer();
        //schedule(task, delay, period)
        timer.schedule(firstTask, 5000, 1000);

	}

}
*/

import java.util.Timer;
import java.util.TimerTask;

public class TimerClassScheduleMethods {

	public static Timer timer;
	
	public static void main(String[] args) {
		
		TimerTask firstTask = new TimerTask() {

            int count = 1;

            @Override
            public void run() {
                test();
            }

            private void test() {
                if (count == 4) {
                    timer.cancel();
                }
                System.out.println(count++);
            }

        };

        Timer timer = new Timer();
        //schedule(task, delay, period)
        timer.schedule(firstTask, 5000, 1000);

	}

}
