/*
Date and Time:
==============
1. How do you convert a Calendar to Date and vice-versa with example?

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Convert a Calendar to Date and vice-versa with example
2.Entity :
    CalendarToDateDemo 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Invoke Calendar class
    2)Display by converting calendar to date.
    3)Display by converting date to calendar.
5.Pseudocode :

public class CalendarToDateDemo {

	public static void main(String[] args) throws InterruptedException  {
		
		//convert calendar to date 
		Calendar calendar = new GregorianCalendar();
        Date date = calendar.getTime();
        System.out.println(date);

        Thread.sleep(3000);

        //convert date to calendar
        Date newDate = new Date(System.currentTimeMillis());
        Calendar newCalendar = Calendar.getInstance();
        newCalendar.setTime(newDate);
        System.out.println(newCalendar.getTime());

	}

}
*/

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class CalendarToDateDemo {

	public static void main(String[] args) throws InterruptedException  {
		
		//convert calendar to date 
		Calendar calendar = new GregorianCalendar();
        Date date = calendar.getTime();
        System.out.println(date);

        Thread.sleep(3000);

        //convert date to calendar
        Date newDate = new Date(System.currentTimeMillis());
        Calendar newCalendar = Calendar.getInstance();
        newCalendar.setTime(newDate);
        System.out.println(newCalendar.getTime());

	}

}
