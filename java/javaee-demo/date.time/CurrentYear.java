/*
Date and Time:
==============
5. Write a Java program to get the information of current/given year
   5.1)check the given year is leap year or not? 
   5.2)Find the Length of the give year?

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Program to get the information of current/given year
        1)check the given year is leap year or not? 
        2)Find the Length of the give year?
2.Entity :
    CurrentYear 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Invoke Year class.
    2)Display current year.
    3)check the current year is leap year or not
        3.1)if Display the current year is leap year.
        3.2)otherwise Display the current year is not a leap year.
    4)Display the length of the year.
5.Pseudocode :

public class CurrentYear {

	public static void main(String[] args) {
		
		Year year = Year.now();
        System.out.println("Current year: " + year);

        if (year.isLeap()) {
            System.out.println(year + " is a leap year");
        } else System.out.println(year + " is not a leap year");

        System.out.format("Length of the year :%d days", year.length());

	}

}
*/

import java.time.Year;

public class CurrentYear {

	public static void main(String[] args) {
		
		Year year = Year.now();
        System.out.println("Current year: " + year);

        if (year.isLeap()) {
            System.out.println(year + " is a leap year");
        } else System.out.println(year + " is not a leap year");

        System.out.format("Length of the year :%d days", year.length());

	}

}
