/*
Date and Time:
==============
6. Write a Java program to get the dates 10 days before and after today.
   6.1)using local date 
   6.2)using calendar
   
Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Program to get the dates 10 days before and after today.
        1)using local date 
        2)using calendar
2.Entity :
    TenDaysBeforeAndAfter 
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Get the local date using now() method.
    2)Display the local date after and before ten days using plusDays() method.
    3)Invoke Calendar class.
    4)Display ten days after and before in calendar using add() method in Calendar.DAY_OFMONTH.
5.Pseudocode :

public class TenDaysBeforeAndAfter {

	public static void main(String[] args) {
		
        LocalDate today = LocalDate.now();
        System.out.println("Current Date: " + today);
        //printing the date before and after 10 days
        System.out.println("10 days before: " + today.plusDays(-10));
        System.out.println("10 days after: " + today.plusDays(10) + "\n");

        Calendar calendar = Calendar.getInstance();
        Calendar newCalendar = Calendar.getInstance();
        //Printing the present Date
        System.out.println("Current Date: " + calendar.getTime());
        //Printing 10 days
        calendar.add(Calendar.DAY_OF_MONTH, -10);
        System.out.println("10 days before: " + calendar.getTime());
        //Printing 10 days
        newCalendar.add(Calendar.DAY_OF_MONTH, 10);
        System.out.println("10 days after: " + newCalendar.getTime());

	}

}
*/

import java.time.LocalDate;
import java.util.Calendar;

public class TenDaysBeforeAndAfter {

	public static void main(String[] args) {
		
		System.out.println("Local Date:");

        LocalDate today = LocalDate.now();
        System.out.println("Current Date: " + today);

        //printing the date before and after 10 days
        System.out.println("10 days before: " + today.plusDays(-10));
        System.out.println("10 days after: " + today.plusDays(10) + "\n");

        System.out.println("Calendar:");

        Calendar calendar = Calendar.getInstance();
        Calendar newCalendar = Calendar.getInstance();

        //Printing the present Date
        System.out.println("Current Date: " + calendar.getTime());

        //Printing 10 days
        calendar.add(Calendar.DAY_OF_MONTH, -10);
        System.out.println("10 days before: " + calendar.getTime());

        //Printing 10 days
        newCalendar.add(Calendar.DAY_OF_MONTH, 10);
        System.out.println("10 days after: " + newCalendar.getTime());

	}

}
