/*
ARRAYS:
=======
1.Create a java array using reflect.array class and add elements
using the appropriate method.

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Create a java array using reflect.array class and add elements
using the appropriate method.
2.Entity :
    ReflectArrayDemo 
3.Function Declaration :
    public static void main(String[] args) 
4.Jobs to be done :
    1)Create a character array using newInstance method with type and size specified.
    2)Add elements to array at the specified index using setChar() method.
    3)Display the array elements.
5.Pseudocode :

public class ReflectArrayDemo {

	public static void main(String[] args) {
		
        char[] array = (char[]) Array.newInstance(char.class, 6);
        ADD elements into the array
        System.out.println(new String(array));

	}

}
*/

package arrays;

import java.lang.reflect.Array;

public class ReflectArrayDemo {

	public static void main(String[] args) {
		
        char[] array = (char[]) Array.newInstance(char.class, 6);

        //Add elements into the array
        Array.setChar(array, 0, 'G');
        Array.setChar(array, 1, 'O');
        Array.setChar(array, 2, 'O');
        Array.setChar(array, 3, 'G');
        Array.setChar(array, 4, 'L');
        Array.setChar(array, 5, 'E');

        System.out.println(new String(array));

	}

}
