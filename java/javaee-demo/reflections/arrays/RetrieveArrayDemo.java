/*
ARRAYS:
=======
2.Retrieve the added elements using the appropriate method.

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Retrieve the added elements using the appropriate method.
using the appropriate method.
2.Entity :
    RetrieveArrayDemo 
3.Function Declaration :
    public static void main(String[] args) 
4.Jobs to be done :
    1)Create a integer array using newInstance() method.
    2)Add elements to array at the specified index using setInt() method.
    3)Display the elements from array using getInt method.
5.Pseudocode :

public class RetrieveArrayDemo {

	public static void main(String[] args) {
		
		int[] array = (int[]) Array.newInstance(int.class, 4);
        int range = 4;
        ADD integer array values
        int index = -1;
        //getting integers from array
        while( ++index < range) {
            System.out.println(Array.getInt(array, index));
        }

	}

}
*/

package arrays;

import java.lang.reflect.Array;

public class RetrieveArrayDemo {

	public static void main(String[] args) {
		
		int[] array = (int[]) Array.newInstance(int.class, 4);

        int range = 4;
        //ADD integer array values
        Array.setInt(array, 0, range);
        Array.setInt(array, 1, 22);
        Array.setInt(array, 2, 33);
        Array.setInt(array, 3, 44);

        int index = -1;
        //getting integers from array
        while( ++index < range) {
            System.out.println(Array.getInt(array, index));
        }

	}

}
