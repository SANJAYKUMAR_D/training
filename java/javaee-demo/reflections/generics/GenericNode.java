package generics;

/*
GENERICS:
=========
1. Consider this class:
    class Node<T> implements Comparable<T> {
        public int compareTo(T obj) {  ...  }
        
    }

Will the following code compile? If not, why?

Answer:
=======

    No,
    Because the greater than (>) operator applied only to primitive numeric types.

*/