package generics;

/*
GENERICS:
=========
2.If the compiler erases all type parameters at compile time,
why should you use generics?

Answer:
=======

    - Generics enable types (classes and interfaces) to be parameters
when defining classes, interfaces and methods.

    - It can be customized, and are type safe and easier to read.
    
    - By using generics, programmers can implement generic algorithms
that work on collections of different types.

*/