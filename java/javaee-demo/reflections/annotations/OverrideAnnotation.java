/*
ANNOTATIONS:
============
1)USE OVERRIDE ANNOTATION and complete the program,
SHOW THE OUTPUT OF THIS PROGRAM: 

class Base 
{ 
     public void display() 
     { 
         
     } 
} 
class Derived extends Base 
{ 
     public void display(int x) 
     { 
         
     } 
  
     public static void main(String args[]) 
     { 
         Derived obj = new Derived(); 
         obj.display(); 
     } 
}

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    USE OVERRIDE ANNOTATION and complete the program
2.Entity :
    OverrideAnnotation 
3.Function Declaration :
    public static void main(String[] args)
    public void display() 
4.Jobs to be done :
    1)Create a base class with a method display.
    2)Create a derived class with a method display.
    3)Use Override annotation to denote the method overriding base class method.
5.Pseudocode :
class Base {

    public void display() {
        System.out.println("Base class");
    }

}

public class OverrideAnnotation extends Base {

    @Override
    public void display() {
        System.out.println("Derived class");
    }

    public static void main(String args[]) {
    	OverrideAnnotation obj = new OverrideAnnotation();
        obj.display();
    }

}
*/

package annotations;

class Base {

    public void display() {
        System.out.println("Base class");
    }

}

public class OverrideAnnotation extends Base {

    @Override
    public void display() {
        System.out.println("Derived class");
    }

    public static void main(String args[]) {
    	OverrideAnnotation obj = new OverrideAnnotation();
        obj.display();
    }

}

