/*
ANNOTATIONS:
============
2)complete:
i)Deprecate the display method
ii)how to suppress the deprecate message

class DeprecatedTest {
    //method to be deprecated
    public void Display()
    {
        System.out.println("Deprecatedtest display()");
    }
}

public class Test
{
    public static void main(String args[]) {
        DeprecatedTest d1 = new DeprecatedTest();
        d1.Display();
    }
}

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    i)Deprecate the display method
    ii)how to suppress the deprecate message
2.Entity :
    DeprecateDemo 
3.Function Declaration :
    public static void main(String[] args)
    public void display() 
4.Jobs to be done :
    1)Create a class DeprecatedTest with a method display.
    2)Use Deprecate annotation to the method display.
    3)Use SuppressWarnings annotation to the main method.
5.Pseudocode :

class DeprecatedTest {
    @Deprecated
    public void display() {
        System.out.println("Deprecatedtest display()");
    }
}

public class DeprecateDemo {

	@SuppressWarnings({"checked", "deprecation"})
    public static void main(String args[]) {
        DeprecatedTest d1 = new DeprecatedTest();
        d1.display();
    }

}

*/

package annotations;

class DeprecatedTest {
    @Deprecated
    public void display() {
        System.out.println("Deprecatedtest display())");
    }
}

public class DeprecateDemo {

    public static void main(String args[]) {
        DeprecatedTest d1 = new DeprecatedTest();
        d1.display();
    }

}
