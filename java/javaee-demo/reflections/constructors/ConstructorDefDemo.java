package constructors;

/*
CONSTRUCTORS:
=============
3) Why does contructors does not returns values?

Answer:
=======

    Constructor doesn't return a value is because it's not called directly by your 
code, it's called by the memory allocation and object initialization code in the runtime.

    The whole purpose of constructor is to initialize the current state of the object by 
setting the initial values.

*/