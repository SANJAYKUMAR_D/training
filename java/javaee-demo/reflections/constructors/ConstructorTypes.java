package constructors;

/*
CONSTRUCTORS:
=============
2) Explain the types of constructors with an example.

Answer:
=======

There are 3 types of constructors
    1)no-arg constructor
    2)Parameterized constructor
    3)Default constructor

no-arg constructor:
===================
    The no-arg constructor that does not accept any arguments.

Parameterized constructor:
==========================
    It accepts a specific number of parameters.

Default constructor:
====================
    The constructor that is automatically created by the Java compiler, 
if it is not explicitly defined.

*/
