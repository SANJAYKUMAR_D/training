/*
CONSTRUCTORS:
=============
1) Create a private constructor with main function

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Create a private constructor with main function
2.Entity :
    PrivateConstructorDemo 
3.Function Declaration :
    public static void main(String[] args) 
4.Jobs to be done :
    1)Create a class with private constructor and a static method.
    2)Invoke the static method instanceMethod
        2.1)The static method can create object of class.
    3)Constructor method get executed.
5.Pseudocode :

public class PrivateConstructorDemo {

	//create private constructor
    private PrivateConstructorDemo() {
        System.out.println("Private constructor successfully tested.");
    }

    //create a public static method
    public static void instanceMethod() {
        //create an instance of class
		PrivateConstructorDemo construct = new PrivateConstructorDemo();
    }
    
    //create a main method
	public static void main(String[] args) {
		
		//call the instanceMethod()
		PrivateConstructorDemo.instanceMethod();

	}

}
*/

package constructors;

public class PrivateConstructorDemo {

	//create private constructor
    private PrivateConstructorDemo() {
        System.out.println("Private constructor successfully tested.");
    }

    //create a public static method
    public static void instanceMethod() {
        //create an instance of class
    	@SuppressWarnings("unused")
		PrivateConstructorDemo construct = new PrivateConstructorDemo();
    }
    
	public static void main(String[] args) {
		
		//call the instanceMethod()
		PrivateConstructorDemo.instanceMethod();

	}

}
