/*
Logical Operators & Escape Sequence:
====================================
1.)Write a Java program to calculate the revenue from a sale based on the unit price and quantity of a product input by the user.
   The discount rate is 10% for the quantity purchased between 100 and 120 units, and 15% for the 
   quantity purchased greater than 120 units. If the quantity purchased is less than 100 units, the discount rate is 0%.
See the example output as shown below:
    Enter unit price: 25
    Enter quantity: 110
    The revenue from sale: 2475.0$
    After discount: 275.0$(10.0%)

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Program to calculate the revenue from a sale based on the unit price and quantity of a product input by the user.
The discount rate is 10% for the quantity purchased between 100 and 120 units, and 15% for the quantity purchased greater 
than 120 units. If the quantity purchased is less than 100 units, the discount rate is 0%.
2.Entity :
    RevenueSaleDemo
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Get the input from user.
    2)Check the quantity
        2.1)If the quantity more than 120, 15% discount should be given
        2.2)If the quantity lies in between 120 and 100, 10% discount should be given
        2.3)orelse no discount should be given.
    3)Calculate the revenue, based on discount applied.
5.Pseudocode:

public class RevenueSaleDemo {

	public static void main(String[] args) {
		
		@SuppressWarnings("resource")
        Scanner scanner = new Scanner(System.in);

        // getting the input from user

        // total sales amount without discount
        double total = unitPrice * productQuantity;
        double discountAmount = 0;
        int discount = 0;

        // discount price according to the quantity
        if (productQuantity > 120) {
            discount = 15;
            discountAmount = total / discount;
        } else if (productQuantity >= 100) {
            discount = 10;
            discountAmount = total / discount;
        }

        // revenue after discount
        double revenue = total - discountAmount;
        System.out.format("The revenue from sale: %.1f$", revenue);
        System.out.format("\n" +
                "After discount: %.1f$(%d%%)", discountAmount, discount);

	}

}

*/

package logicalOperatorsEscapeSequence;

import java.util.Scanner;

public class RevenueSaleDemo {

	public static void main(String[] args) {
		
		@SuppressWarnings("resource")
        Scanner scanner = new Scanner(System.in);

        // getting the input from user
        System.out.println("Enter unit price: ");
        int unitPrice = scanner.nextInt();
        System.out.println("Enter quantity: ");
        int productQuantity = scanner.nextInt();

        // total sales amount without discount
        double total = unitPrice * productQuantity;
        double discountAmount = 0;
        int discount = 0;

        // discount price according to the quantity
        if (productQuantity > 120) {
            discount = 15;
            discountAmount = total / discount;
        } else if (productQuantity >= 100) {
            discount = 10;
            discountAmount = total / discount;
        }

        // revenue after discount
        double revenue = total - discountAmount;
        System.out.format("The revenue from sale: %.1f$", revenue);
        System.out.format("\n" +
                "After discount: %.1f$(%d%%)", discountAmount, discount);

	}

}
