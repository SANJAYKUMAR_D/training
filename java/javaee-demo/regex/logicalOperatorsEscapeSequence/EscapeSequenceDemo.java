/*
Logical Operators & Escape Sequence:
====================================
2.)write a program to display the following using escape sequence in java
    A.My favorite book is "Twilight" by Stephanie Meyer
    B.She walks in beauty, like the night, 
      Of cloudless climes and starry skies 
      And all that's best of dark and bright 
      Meet in her aspect and her eyes…
    C."Escaping characters", © 2019 Java

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Program to display the following using escape sequence in java
        A.My favorite book is "Twilight" by Stephanie Meyer
        B.She walks in beauty, like the night, 
          Of cloudless climes and starry skies 
          And all that's best of dark and bright 
          Meet in her aspect and her eyes…
        C."Escaping characters", © 2019 Java
2.Entity :
    EscapeSequenceDemo
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Create a class with main method.
    2)To use escape sequence to escape the double quotes in string.
    3)To use escape sequence to break to new line // \r\n to indicate an end of line
    4)To use unicode to display symbols. //unicode u00A9
5.Pseudocode :

public class EscapeSequenceDemo {

	public static void main(String[] args) {
		
		System.out.println("My favorite book is \"Twilight\" by Stephanie Meyer");
        System.out.println("\n" + "She walks in beauty, like the night, \r\n" +
                "Of cloudless climes and starry skies \r\n" +
                "And all that's best of dark and bright \r\n" +
                "Meet in her aspect and her eyes�");
        // \r\n to indicate an end of line
        System.out.println("\n" + "\"Escaping characters\", \u00A9 2019 Java");
        //unicode u00A9

	}

}

*/

package logicalOperatorsEscapeSequence;

public class EscapeSequenceDemo {

	public static void main(String[] args) {
		
		System.out.println("My favorite book is \"Twilight\" by Stephanie Meyer");
        System.out.println("\n" + "She walks in beauty, like the night, \r\n" +
                "Of cloudless climes and starry skies \r\n" +
                "And all that's best of dark and bright \r\n" +
                "Meet in her aspect and her eyes�");
        // \r\n to indicate an end of line
        System.out.println("\n" + "\"Escaping characters\", \u00A9 2019 Java");
        //unicode u00A9

	}

}
