/*
Java String Regex Methods:
==========================
1.) write a program for Java String Regex Methods?

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    program for Java String Regex Methods
2.Entity :
    StringRegexMethods
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Create a class with main method.
    2)Check the name matches that contains some character.
        2.1)print TRUE or FALSE whether the string match or not.
    3)Replace all the particular String to other String.
        3.1)print the changed sentence.
    4)Replace only the first word on repeated words.
        4.1)print the changed sentence.
    5)Split the sentence based on regex.
        5.1)print the splited strings.
    
5.Pseudocode :

public class StringRegexMethods {

	public static void main(String[] args) {
		
		// matches(regex)
        String name = "Krishna";
        // .* means either nothing or anything present
        boolean match = name.matches("(.*)sh(.*)");
        System.out.println("Checking the name 'Krishna' has 'sh' inside: " + match);

        // replaceAll(regex, replacement)
        String sentence = "CSK had a beautiful innings";
        // replaces all the regex with the replacement string
        System.out.println(sentence.replaceAll("CSK", "RCB"));

        // replaceFirst(regex, replacement)
        // replaces first regex with the replacement string
        System.out.println(sentence.replaceFirst("CSK", "SRH"));

        // split(regex)
        String directory = "G:/PHOTOS/candit/Snapseed";
        System.out.println("Detailed path");
        // splits the string to strings whenever there is an regex
        for (String path : directory.split("/")) {
            System.out.println(path);
        }
        // split(regex, limit)
        System.out.println("\nSpecified path");
        // splits the string to strings based on the limit too
        //if limit is 2 output will 2 strings
        for (String path : directory.split("/", 3)) {
            System.out.println(path);
        }

	}

}

*/

package regexMethods;

public class StringRegexMethods {

	public static void main(String[] args) {
		
		// matches(regex)
        String name = "Krishna";
        // .* means either nothing or anything present
        boolean match = name.matches("(.*)sh(.*)");
        System.out.println("Checking the name 'Krishna' has 'sh' inside: " + match);

        // replaceAll(regex, replacement)
        String sentence = "CSK had a beautiful innings";
        // replaces all the regex with the replacement string
        System.out.println(sentence.replaceAll("CSK", "RCB"));

        // replaceFirst(regex, replacement)
        // replaces first regex with the replacement string
        System.out.println(sentence.replaceFirst("CSK", "SRH"));

        // split(regex)
        String directory = "G:/PHOTOS/candit/Snapseed";
        System.out.println("Detailed path");
        // splits the string to strings whenever there is an regex
        for (String path : directory.split("/")) {
            System.out.println(path);
        }
        // split(regex, limit)
        System.out.println("\nSpecified path");
        // splits the string to strings based on the limit too
        //if limit is 2 output will 2 strings
        for (String path : directory.split("/", 3)) {
            System.out.println(path);
        }

	}

}
