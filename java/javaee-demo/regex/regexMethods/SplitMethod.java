/*
Java String Regex Methods:
==========================
2.) For the following code use the split method() and print in sentence
    String website = "https-www-google-com";

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Use the split method() and print in sentence
String website = "https-www-google-com";
2.Entity :
    SplitMethod
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Create a class with main method.
    2)Initialize the input String.
    3)split the directory based on the sub string in split() method
    4)Print the splited path
5.Pseudocode :

public class SplitMethod {

	public static void main(String[] args) {
		
		// url as source
        String directory = "G:/PHOTOS/candit/Snapseed/img45.png";

        // spliting the directory based on String in split method
        for (String path : directory.split("/")) {
            System.out.println(path);
        }

	}

}
*/

package regexMethods;

public class SplitMethod {

	public static void main(String[] args) {
		
		// url as source
        String directory = "G:/PHOTOS/candit/Snapseed/img45.png";

        // spliting the directory based on String in split method
        for (String path : directory.split("/")) {
            System.out.println(path);
        }

	}

}
