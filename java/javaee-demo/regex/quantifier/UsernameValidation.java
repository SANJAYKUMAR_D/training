/*
Quantifiers:
============
3.)create a username for login website which contains
   8-12 characters in length
   Must have at least one uppercase letter
   Must have at least one lower case letter
   Must have at least one digit


Work Breakdown Structure(WBS):
==============================
1.Requirement :
    create a username for login website which contains
    - 8-12 characters in length
    - Must have at least one uppercase letter
    - Must have at least one lower case letter
    - Must have at least one digit
2.Entity :
    UsernameValidation
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Get the input from user using Scanner class.
    2)Create a pattern that
        2.1)length lies between 8-12 characters
        2.2)Must have at least one upper case letter
        2.3)Must have at least one lower case letter
        2.4)Must have at least one digit
    3)Create a matcher that tries to match input and pattern
    4)Check if the matcher matches the input
        4.1) Display the given userName is a valid or not.
5.Pseudocode :

public class UsernameValidation {

	public static void main(String[] args) {
		
		System.out.print("Enter the username: ");
        @SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
        String username = scanner.nextLine();

        //creating a pattern to validate user input
        Pattern pattern = Pattern
                .compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).{8,12}$");

        //checking matcher matches the input
        Matcher matcher = pattern.matcher(username);
        if (matcher.find()) {
            System.out.println("Valid username.");
        } else System.out.println("Invalid username.");

	}

}
*/

package quantifier;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UsernameValidation {

	public static void main(String[] args) {
		
		System.out.print("Enter the username: ");
        @SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
        String username = scanner.nextLine();

        //creating a pattern to validate user input
        Pattern pattern = Pattern
                .compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).{8,12}$");

        //checking matcher matches the input
        Matcher matcher = pattern.matcher(username);
        if (matcher.find()) {
            System.out.println("Valid username.");
        } else System.out.println("Invalid username.");

	}

}
