/*
Quantifiers:
============
2.)write a program for email-validation?

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Program for email-validation.
2.Entity :
    EmailValidation
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Get the input from user using Scanner Class.
    2)Create a pattern that should have to validate an email.
    3)Create a matcher that tries to match input and pattern
    4)If the given email matches
        4.1)Display the input as a Valid email orelse Invalid.
5.Pseudocode :

public class EmailValidation {

	public static void main(String[] args) {
		
		//getting the input from user
        @SuppressWarnings("resource")
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter your email address: ");
        String mailId = scanner.nextLine();

        //creating a pattern to validate user input
        Pattern pattern = Pattern
                .compile("^[a-zA-Z\\d._]{6,}+@[a-z.]+\\.[a-z]{2,}$");

        //checking matcher matches the input
        Matcher matcher = pattern.matcher(mailId);

        //if matches
        if (matcher.find()) {
            System.out.format("'%s'" + " is valid.", matcher.group());
        } else {
            System.out.println("Invalid email.");
        }
		
	}

}

*/

package quantifier;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidation {

	public static void main(String[] args) {
		
		//getting the input from user
        @SuppressWarnings("resource")
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter your email address: ");
        String mailId = scanner.nextLine();

        //creating a pattern to validate user input
        Pattern pattern = Pattern
                .compile("^[a-z\\d._]{6,}+@[a-z.]+\\.[a-z]{2,}$");

        //checking matcher matches the input
        Matcher matcher = pattern.matcher(mailId);

        //if matches
        if (matcher.find()) {
            System.out.format("'%s'" + " is valid.", matcher.group());
        } else {
            System.out.println("Invalid email.");
        }
		
	}

}
