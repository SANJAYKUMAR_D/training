/*
Quantifiers:
============
1.)write a program for java regex quantifier?

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Program for java regex quantifier.
2.Entity :
    RegexQuantifier
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    1)Initialize the input string.
    2)Create a pattern with some conditions
    3)Create a matcher that tries to match input and pattern.
    4)Check weather the String is present
        4.1)If present display the position in the sentence.
5.Pseudocode :

public class RegexQuantifier {

	public static void main(String[] args) {
		
		String thoughts = "Education is the most powerfull weapon " +
                "which you can use to change the world.";

        //creating a pattern
        Pattern pattern = Pattern.compile("the");

        //checking the pattern conditions matches the string
        Matcher matcher = pattern.matcher(thoughts);

        //find the occurrance of the pattern
        while (matcher.find()) {
            System.out.println("'" + matcher.group() + "'" + " found at " +
                    matcher.start());
        }

	}

}

*/

package quantifier;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexQuantifier {

	public static void main(String[] args) {
		
		String thoughts = "Education is the most powerfull weapon " +
                "which you can use to change the world.";

        //creating a pattern
        Pattern pattern = Pattern.compile("the");

        //checking the pattern conditions matches the string
        Matcher matcher = pattern.matcher(thoughts);

        //find the occurrance of the pattern
        while (matcher.find()) {
            System.out.println("'" + matcher.group() + "'" + " found at " +
                    matcher.start());
        }

	}

}
