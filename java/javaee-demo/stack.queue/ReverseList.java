/*
Stack exercises:
================================
* Reverse List Using Stack with minimum 7 elements in list.

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Reverse List Using Stack with minimum 7 elements in list
2.Entity :
    ReverseList
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    i)Create a class ReverseList with main method.
    ii)Create a list students using generic type and names are pushed into list
    iii)List elements pushed into stack.
    iv)The element get reversed.
    v)Print the ReversedList.
5.Pseudocode :

public class ReverseList {

	public static void main(String[] args) {
		
		List<String> students = new ArrayList<>();
        Stack<String> stack = new Stack<>();

        students.add("Sanjay");
        students.add("Santheesh");
        students.add("Harry");
        students.add("Watson");
        students.add("Galgadot");
        students.add("Rose");
        students.add("Robert Drowny");
        students.add("Kalam");
        students.add("Smithy");
        students.add("Billgates");
        
        System.out.print(students); // printing the list before reversing.

        while (students.size() > 0) {
            stack.push(students.remove(0)); 
        }

        System.out.println();

        // stack follows "First In Last Out"
        while (stack.size() > 0) {
        	students.add(stack.pop()); // picking the last element and putting in list.
        }

        System.out.print(students); // printing the reversed list


	}

}

*/

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class ReverseList {

	public static void main(String[] args) {
		
		List<String> students = new ArrayList<>();
        Stack<String> stack = new Stack<>();

        students.add("Sanjay");
        students.add("Santheesh");
        students.add("Harry");
        students.add("Watson");
        students.add("Galgadot");
        students.add("Rose");
        students.add("Robert Drowny");
        students.add("Kalam");
        students.add("Smithy");
        students.add("Billgates");
        
        System.out.print(students); // printing the list before reversing.

        while (students.size() > 0) {
            stack.push(students.remove(0)); 
        }

        System.out.println();

        // stack follows "First In Last Out"
        while (stack.size() > 0) {
        	students.add(stack.pop()); // picking the last element and putting in list.
        }

        System.out.print(students); // printing the reversed list


	}

}
