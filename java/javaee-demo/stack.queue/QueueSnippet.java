/*
Queue exercises:
================================
* Consider a following code snippet
        Queue bike = new PriorityQueue();    
        bike.poll();
        System.out.println(bike.peek());    

  what will be output and complete the code.
  
Work Breakdown Structure(WBS):
==============================
1.Requirement :
    what will be output and complete the code
2.Entity :
    QueueSnippet
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    i)Create a class QueueSnippet with main method.
    ii)Create a queue and add a elements to that queue.
    iii)Printing a poll and peek element in a queue.
5.Pseudocode :

public class QueueSnippet {

	public static void main(String[] args) {
		
		Queue<String> bike = new PriorityQueue<>();
		bike.add("Pulsar150");
		bike.add("RX100");
		bike.add("Duke200");
		bike.add("Yamaha");
        System.out.println(bike.poll());
        System.out.println(bike.peek());

	}

}
*/

import java.util.PriorityQueue;
import java.util.Queue;

public class QueueSnippet {

	public static void main(String[] args) {
		
		Queue<String> bike = new PriorityQueue<>();
		bike.add("Pulsar150");
		bike.add("RX100");
		bike.add("Duke200");
		bike.add("Yamaha");
        System.out.println(bike.poll());
        System.out.println(bike.peek());

	}

}
