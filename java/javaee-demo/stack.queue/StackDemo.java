/*
Stack exercises:
================================
* Create a stack using generic type and implement
  -> Push atleast 5 elements
  -> Pop the peek element
  -> search a element in stack and print index value
  -> print the size of stack
  -> print the elements using Stream

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Create a stack using generic type and implement
    Push atleast 5 elements
    Pop the peek element
    search a element in stack and print index value
    print the size of stack
    print the elements using Stream
2.Entity :
    StackDemo
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    i)Create a class StackDemo with main method.
    ii)Create a stack students using generic type and names pushed into stack
    iii)Pop the peek name
    iv)Search a name in stack and print index value
    v)Print the size of stack
    vi)Print the elements using Stream
5.Pseudocode :

public class StackDemo {

	public static void main(String[] args) {
		
		Stack<String> student = new Stack<>();
		student.push("Sanjay");
		student.push("Arun");
		student.push("Naveen");
		student.push("Surya");
		student.push("Jeeva");
	    
		//Pop the peek element.
	    System.out.println(student.pop());
	    
        //search a element in stack and print index value
	    System.out.println(student.search("Sanjay"));
	    
	    //print the size of stack.
        System.out.println(student.size());
        
	    //Print the elements using Stream API's forEach
	    Stream<String> stream = student.stream();
	    stream.forEach(System.out::print);

	}

}

*/

import java.util.Stack;
import java.util.stream.Stream;

public class StackDemo {

	public static void main(String[] args) {
		
		Stack<String> student = new Stack<>();
		student.push("Sanjay");
		student.push("Arun");
		student.push("Naveen");
		student.push("Surya");
		student.push("Jeeva");
	    
		//Pop the peek element.
	    System.out.println(student.pop());
	    
        //search a element in stack and print index value
	    System.out.println(student.search("Sanjay"));
	    
	    //print the size of stack.
        System.out.println(student.size());
        
	    //Print the elements using Stream API's forEach
	    Stream<String> stream = student.stream();
	    stream.forEach(System.out::print);

	}

}
