/*
Queue exercises:
================================
Create a queue using generic type and in both implementation Priority Queue,Linked list and complete following  
  -> add atleast 5 elements
  -> remove the front element
  -> search a element in stack using contains key word and print boolean value value
  -> print the size of stack
  -> print the elements using Stream 

Work Breakdown Structure(WBS):
==============================
1.Requirement :
    Create a queue using generic type and in both implementation Priority Queue,Linked list and complete following  
    add atleast 5 elements
    remove the front element
    search a element in stack using contains key word and print boolean value value
    print the size of stack
    print the elements using Stream 
2.Entity :
    QueueDemo
3.Function Declaration :
    public static void main(String[] args)
4.Jobs to be done :
    i)Create a class QueueDemo with main method.
    ii)Create a queue using generic type and valuess are pushed into queueValues
    iii)Removing the peek element from the queue.
    iv)Printing all elements using forEach in stream and adding elements to the queue
    v)Getting the size of the queue
    vi)Printing all elements using forEach in stream
5.Pseudocode :

public class QueueDemo {

	public static void main(String[] args) {
		
        Queue<Integer> queueValues = new LinkedList<>();

        // adding elements to queue
        queueValues.add(0);
        queueValues.add(1);
        queueValues.add(2);
        queueValues.add(3);
        queueValues.add(4);

        // remove the front element
        queueValues.remove();

        // checking whether queue has the element or not
        System.out.println(queueValues.contains(0));

        // printing the size of the queue
        System.out.println(queueValues.size());

        // printing all elements using forEach in stream
        Stream<Integer> stream = queueValues.stream();
        stream.forEach(System.out::println);

        Queue<Integer> priorityQueue = new PriorityQueue<>();

        // adding elements to the queue
        priorityQueue.add(4001);
        priorityQueue.add(4004);
        priorityQueue.add(4012);
        priorityQueue.add(4017);
        priorityQueue.offer(4020);
        priorityQueue.offer(4021);

        // removing the peek element from the queue
        priorityQueue.poll();

        //checking whether queue has the element or not
        System.out.println(priorityQueue.contains(2020));

        // getting the size of the queue
        System.out.println(priorityQueue.size());

        // printing all elements using forEach in stream
        Stream<Integer> priorityQueueStream = priorityQueue.stream();
        priorityQueueStream.forEach(System.out::println);

	}

}
*/


import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.stream.Stream;

public class QueueDemo {

	public static void main(String[] args) {
		
		System.out.println("LinkedList implementation:");
        Queue<Integer> queueValues = new LinkedList<>();

        // adding elements to queue
        queueValues.add(0);
        queueValues.add(1);
        queueValues.add(2);
        queueValues.add(3);
        queueValues.add(4);

        // remove the front element
        queueValues.remove();

        // checking whether queue has the element or not
        System.out.println(queueValues.contains(0));

        // printing the size of the queue
        System.out.println(queueValues.size());

        // printing all elements using forEach in stream
        System.out.println("Iterating elements using stream:");
        Stream<Integer> stream = queueValues.stream();
        stream.forEach(System.out::println);

        System.out.println("PriorityQueue implementation:");
        Queue<Integer> priorityQueue = new PriorityQueue<>();

        // adding elements to the queue
        priorityQueue.add(4001);
        priorityQueue.add(4004);
        priorityQueue.add(4012);
        priorityQueue.add(4017);
        priorityQueue.offer(4020);
        priorityQueue.offer(4021);

        // removing the peek element from the queue
        priorityQueue.poll();

        //checking whether queue has the element or not
        System.out.println(priorityQueue.contains(2020));

        // getting the size of the queue
        System.out.println(priorityQueue.size());

        // printing all elements using forEach in stream
        System.out.println("Iterating elements using stream:");
        Stream<Integer> priorityQueueStream = priorityQueue.stream();
        priorityQueueStream.forEach(System.out::println);

	}

}
